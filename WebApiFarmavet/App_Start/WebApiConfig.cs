﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;

namespace WebApiFarmavet
{
    public static class WebApiConfig
    {
        public static readonly string AppDateFormat = "dd/MM/yyyy";

        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web
            // Configure Web API para usar solo la autenticación de token de portador.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Rutas de API web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "CheckLogin",
                routeTemplate: "api/login/{action}",
                defaults: new { controller = "User" }
            );
        }
    }
}
