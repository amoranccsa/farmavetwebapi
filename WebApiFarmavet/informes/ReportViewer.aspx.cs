﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApiFarmavet.Controllers;
using WebApiFarmavet.Services;

namespace WebApiFarmavet.informes
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        //private static object miInforme;
        private static List<Tuple<string, object>> staticListaParam;
        //private static ViewInfo ultimaVista;

        /// <summary>
        /// El código comentado de esta función está para hacer pruebas, y como "documentación" de algunas cosas interesantes que se podrían hacer para arreglar la navegación.
        /// Actualmente sólo se puede navegar un nivel hacia abajo, y las páginas se muestran seguidas unas de otras.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SAPbobsCOM.Company oCompany;
                string reportPath = "";
                string serverName = ConfigurationManager.AppSettings.Get("Server");
                string DBName = ConfigurationManager.AppSettings.Get("DB");
                string user = ConfigurationManager.AppSettings.Get("DBUserID");
                string password = SAPDesglose.Decrypt(ConfigurationManager.AppSettings.Get("DBpwd"));
                List<Tuple<string, object>> listaParam = new List<Tuple<string, object>>();
                bool leyendoSubreport = false;

                string token = "";
                HttpStatusCode statusCode = HttpStatusCode.Forbidden;

                //Seguridad usando Token
                if (Request.HttpMethod == "GET")
                {
                    token = Request.Headers.Get("Token");
                    statusCode = CRMControllersCommonFunctions.CheckToken(token, out oCompany);
                    //statusCode = HttpStatusCode.OK;
                }
                else if (Request.HttpMethod == "POST")
                {
                    if (UglySecurity())
                        statusCode = HttpStatusCode.OK;
                    else
                        statusCode = HttpStatusCode.Forbidden;

                    leyendoSubreport = true;
                }
                

                if (statusCode == HttpStatusCode.OK)
                {
                    //Mostramos CR, ocultamos error FORBIDDEN
                    form1.Visible = true;
                    form2.Visible = false;

                    //Obtenemos parámetros de la URL
                    listaParam = GetRequestParams();

                    if (listaParam.Exists(x => x.Item1 == "doc"))
                    {
                        reportPath = GetReportPath(listaParam.FirstOrDefault(x => x.Item1 == "doc").Item2.ToString());
                        listaParam.Remove(listaParam.Find(x => x.Item1 == "doc"));  //Borramos el parámetro del tipo de documento porque no lo va a usar el CR
                    }

                    /*CrystalReportViewer1.Drill += CrystalReportViewer_DrillDown;
                    CrystalReportViewer1.DrillDownSubreport += CrystalReportViewer_DrillDownSubreport;
                    CrystalReportViewer1.Navigate += CrystalReportViewer1_Navigate;

                    ultimaVista = CrystalReportViewer1.ViewInfo;*/

                    //Invocamos al CR
                    if (!leyendoSubreport)
                    {
                        Session["ReportDocument"] = GetReport(reportPath, serverName, DBName, user, password, listaParam);
                        
                        //CrystalReportViewer1.ReportSource = GetReport(reportPath, serverName, DBName, user, password, listaParam);
                        staticListaParam = listaParam;

                        //CrystalReportViewer1.OnDrillDownSubreport(miInforme, );
                    }
                    else
                    {
                        //var lol = ((NonHTTPCachedReportSource)CrystalReportViewer1.ReportSource);
                        /*for (int i = 0; i < staticListaParam.Count; i++)
                            miInforme.SetParameterValue(staticListaParam[i].Item1, staticListaParam[i].Item2);*/

                        /*ParameterFields parameterFields = new ParameterFields();

                        for (int i = 0; i < staticListaParam.Count; i++)
                        {
                            ParameterDiscreteValue parameterDiscreteValue = new ParameterDiscreteValue();
                            parameterDiscreteValue.Value = staticListaParam[i].Item2;

                            ParameterValues currentParameterValues = new ParameterValues();
                            currentParameterValues.Add(parameterDiscreteValue);

                            ParameterField parameterField = new ParameterField();
                            parameterField.Name = staticListaParam[i].Item1;
                            parameterField.CurrentValues = currentParameterValues;

                            parameterFields.Add(parameterField);
                        }

                        CrystalReportViewer1.ParameterFieldInfo = new ParameterFields(parameterFields);
                        var ad = CrystalReportViewer1.ID;*/
                    }

                    //CrystalReportViewer1.ReportSource = miInforme; //Guardando el informe como static no se pierden los parámetros, pero no se puede navegar más allá de un enlace :S
                    //if (!CrystalReportViewer1.ViewInfo.IsSubreportView) //Sólo cargamos el rpt desde el informe principal, y la primera vez desde el subinforme (esta variable aun vale false)
                                                                        //No sé si se puede cargar la segunda página del subinforme con los datos de los parámetros.
                    CrystalReportViewer1.ReportSource = Session["ReportDocument"]; //Guardando el informe como static no se pierden los parámetros, pero no se puede navegar más allá de un enlace :S

                }
                else
                {
                    //Ocultamos el CR, mostramos error FORBIDDEN
                    form1.Visible = false;
                    form2.Visible = true;
                }
            } catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION ReportViewer: " + ex, ErrorHandler.ErrorVerboseLevel.None);
            }
        }


        /*private void CrystalReportViewer_DrillDownSubreport(object source, DrillSubreportEventArgs e)
        {
            var ad = CrystalReportViewer1.ViewInfo;
            
        }

        private void CrystalReportViewer_DrillDown(object source, DrillEventArgs e)
        {
            var ad = CrystalReportViewer1.ViewInfo;
        }

        private void CrystalReportViewer1_Navigate(object source, NavigateEventArgs e)
        {
            var ad = CrystalReportViewer1.ViewInfo;
        }*/




        //Usar este proceso para testeo del visor
        /*protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string serverName = ConfigurationManager.AppSettings.Get("Server");
                string DBName = ConfigurationManager.AppSettings.Get("DB");
                string user = ConfigurationManager.AppSettings.Get("DBUserID");
                string password = SAPDesglose.Decrypt(ConfigurationManager.AppSettings.Get("DBpwd"));
                List<Tuple<string, object>> listaParam = new List<Tuple<string, object>>();

                
                listaParam.Add(new Tuple<string, object>("desde", "01/01/2019"));
                listaParam.Add(new Tuple<string, object>("hasta", "31/12/2019"));
                listaParam.Add(new Tuple<string, object>("id_emp_com", 2));
                    
                CrystalReportViewer1.ReportSource = GetReport(HttpContext.Current.Server.MapPath("~") + "informes/plantillas/app_INFORME_VENTAS.rpt", "192.168.1.184", "Farmavet_CCSA_PRUEBAS",
                "sa", "SAPB1Admin", listaParam);
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION ReportViewer: " + ex, ErrorHandler.ErrorVerboseLevel.None);
            }
        }*/

        private string GetReportPath(string docType)
        {
            string reportFileName = "";
            ErrorHandler.WriteInLog("* Leyendo informe tipo " + docType, ErrorHandler.ErrorVerboseLevel.Trace);
            switch (docType)
            {
                case "informeVentas":
                    reportFileName = "app_INFORME_VENTAS.rpt";
                    return HttpContext.Current.Server.MapPath("~") + "informes/plantillas/" + reportFileName;
                default:
                    return "";
            }
        }

        private List<Tuple<string, object>> GetRequestParams ()
        {
            List<Tuple<string, object>> paramsList = new List<Tuple<string, object>>();
            string[] keyList = Request.QueryString.AllKeys;
            object tempValue;

            for (int i = 0; i < keyList.Length; i++)
            {
                tempValue = Request.QueryString.Get(keyList[i]);
                if (tempValue is string)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch((string)tempValue, @"^\d*\d/\d*\d/\d{2,4}$"))
                        tempValue = DateTime.ParseExact((string)tempValue, WebApiConfig.AppDateFormat, System.Globalization.CultureInfo.InvariantCulture);
                }
                paramsList.Add(new Tuple<string, object>(keyList[i], tempValue));

                ErrorHandler.WriteInLog(string.Format("* Parámetro de informe: ({0}, {1})", keyList[i], tempValue), ErrorHandler.ErrorVerboseLevel.Trace);
            }

            return paramsList;
        }

        private ReportDocument GetReport(string rptNameFullPath, string serverName, string DBName, string user, string password, List<Tuple<string, object>> listaParam)
        {
            ReportDocument crystalReport = new ReportDocument();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();

            crystalReport.Load(rptNameFullPath);

            crystalReport.SetDatabaseLogon(user, password, serverName, DBName);
            //crystalReport.SetDatabaseLogon("sa", "SAPB1Admin");

            crConnectionInfo.DatabaseName = DBName;
            crConnectionInfo.ServerName = serverName;
            crConnectionInfo.UserID = user;
            crConnectionInfo.Password = password;

            foreach (CrystalDecisions.CrystalReports.Engine.Table crTabla in crystalReport.Database.Tables)
            {
                crTabla.LogOnInfo.ConnectionInfo = crConnectionInfo;
                crTabla.ApplyLogOnInfo(crTabla.LogOnInfo);
            }

            //POSIBLES SUBREPORTS
            //crystalReport.Subreports[0].SetDatabaseLogon(user, password, serverName, DBName);

            //crystalReport.Refresh();
            crystalReport.VerifyDatabase();
            for (int i = 0; i < listaParam.Count; i++)
            {
                crystalReport.SetParameterValue(listaParam[i].Item1, listaParam[i].Item2);
            }
            //crystalReport.Close();

            return crystalReport;
        }




        private bool UglySecurity ()
        {
            return Request.Headers.Get("Origin").Contains(Request.Headers.Get("Host")) && (Request.Params.Count > 63);
        }
    }
}