﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="WebApiFarmavet.informes.ReportViewer" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title></title>
        <script lang="javaScript" type="text/javascript" src="/aspnet_client/system_web/4_0_30319/crystalreportViewers13/js/crviewer/crv.js"></script>
    </head>
    <body style="height: 799px">
        <form id="form1" runat="server">
            <div>
                <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" Height="50px"
                        PrintMode="ActiveX" ToolPanelView="None" ToolPanelWidth="200px" Width="350px" EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" ReuseParameterValuesOnRefresh="True" SeparatePages="False"/>
            </div>
        </form>
        <form id="form2" runat="server">
            <div>
                <h1 id="texto1">Acceso no autorizado</h1>
                <p id="texto2">No se ha logeado correctamente en SAP B1. Por favor, introduzca sus datos de usuario en la APP e inténtelo de nuevo.</p>
            </div>
        </form>
    </body>
</html>
