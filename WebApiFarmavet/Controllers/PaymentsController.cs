﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class PaymentsController : ApiController
    {
        #region PENDING PAYMENT list ***********************************************************************************
        [Route("api/payments/pendingList")]
        public HttpResponseMessage GetPendingPaymentList([FromUri] int salesPerson, string cardCode, string paymentMethod)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMPendingPaymentModel> pendingPaymentList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("GetPendingPaymentList: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                pendingPaymentList = SAPPayments.GetPendingPaymentList(oCompany, salesPerson, cardCode, paymentMethod);
                message = Request.CreateResponse(HttpStatusCode.OK, pendingPaymentList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *****************************************************************************************************

        #region ADDING PAYMENT *****************************************************************************************
        private HttpResponseMessage AddPayment(CRMPaymentModel payment, bool asDraft)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* AddPayment", ErrorHandler.ErrorVerboseLevel.Trace);
                _error = SAPPayments.AddPayment(oCompany, payment, asDraft);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/payments/add")]
        public HttpResponseMessage PostAddPayment([FromBody] CRMPaymentModel payment)
        {
            return AddPayment(payment, false);
        }

        [Route("api/payments/addDraft")]
        public HttpResponseMessage PostAddPaymentDraft([FromBody] CRMPaymentModel payment)
        {
            return AddPayment(payment, true);
        }
        #endregion *****************************************************************************************************
    }
}
