﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    public class PictureController : ApiController
    {
        [Route("api/pictures/picture")]
        public HttpResponseMessage GetPicture([FromUri] string pictureName)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMPictureModel picture = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get picture " + pictureName+ ")", ErrorHandler.ErrorVerboseLevel.Trace);
                picture = SAPPictures.GetPicture(oCompany, pictureName, -1);
                message = Request.CreateResponse(HttpStatusCode.OK, picture,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/pictures/thumbnail")]
        public HttpResponseMessage GetPicture([FromUri] string pictureName, int size)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMPictureModel picture = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get picture " + pictureName + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                picture = SAPPictures.GetPicture(oCompany, pictureName, size);
                message = Request.CreateResponse(HttpStatusCode.OK, picture,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
    }
}
