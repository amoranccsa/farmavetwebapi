﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Services;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class DebugController : ApiController
    {
        [Route("api/debug/message")]
        [HttpPost]
        public HttpResponseMessage WriteDebugMsgFromAPP([FromBody] string message)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage httpMessage;
            
            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("*** DEBUG message from APP: " + message, ErrorHandler.ErrorVerboseLevel.Debug);
                httpMessage = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
                httpMessage = Request.CreateResponse(statusCode);

            return httpMessage;
        }
    }
}
