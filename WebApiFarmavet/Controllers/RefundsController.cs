﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class RefundsController : ApiController
    {
        #region GET Refunds List *****************************************************************************************
        [Route("api/refunds/list")]
        public HttpResponseMessage GetRefundsList([FromUri] int salesPerson)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMRefundModel> refundsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetRefundsList. Sales person: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                refundsList = SAPRefunds.GetRefundsList(oCompany, salesPerson, null, null, null, null, false);
                message = Request.CreateResponse(HttpStatusCode.OK, refundsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/refunds/list")]
        public HttpResponseMessage GetRefundsList([FromUri] int salesPerson, string refundId, string customer, string dateFrom, string dateTo)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMRefundModel> refundsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetRefundsList. Salesperson={0}, refund={1}, customer={2}, from {3} to {4}",
                                                        salesPerson, refundId, customer, dateFrom, dateTo), ErrorHandler.ErrorVerboseLevel.Trace);
                refundsList = SAPRefunds.GetRefundsList(oCompany, salesPerson, refundId, customer, dateFrom, dateTo, false);
                message = Request.CreateResponse(HttpStatusCode.OK, refundsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }



        [Route("api/refunds/draftList")]
        public HttpResponseMessage GetRefundsDraftList([FromUri] int salesPerson)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMRefundModel> refundsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetRefundsDraftList. Sales person: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                refundsList = SAPRefunds.GetRefundsList(oCompany, salesPerson, null, null, null, null, true);
                message = Request.CreateResponse(HttpStatusCode.OK, refundsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/refunds/draftList")]
        public HttpResponseMessage GetRefundsDraftList([FromUri] int salesPerson, string refundId, string customer, string dateFrom, string dateTo)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMRefundModel> refundsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetRefundsDraftList. Salesperson={0}, refund={1}, customer={2}, from {3} to {4}",
                                                        salesPerson, refundId, customer, dateFrom, dateTo), ErrorHandler.ErrorVerboseLevel.Trace);
                refundsList = SAPRefunds.GetRefundsList(oCompany, salesPerson, refundId, customer, dateFrom, dateTo, true);
                message = Request.CreateResponse(HttpStatusCode.OK, refundsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }




        [Route("api/refunds/list")]
        public HttpResponseMessage GetRefundsPagedList([FromUri] int salesPerson, bool getDocuments, bool getDrafts, int page)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMRefundModel> refundsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetRefundsPagedList. Sales person: (" + salesPerson + "). Page: " + page, ErrorHandler.ErrorVerboseLevel.Trace);
                refundsList = SAPRefunds.GetRefundsPagedList(oCompany, salesPerson, null, null, null, null, getDocuments, getDrafts, page);
                message = Request.CreateResponse(HttpStatusCode.OK, refundsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/refunds/list")]
        public HttpResponseMessage GetRefundsPagedList([FromUri] int salesPerson, string refundId, string customer, string dateFrom, string dateTo,
                                                        bool getDocuments, bool getDrafts, int page)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMRefundModel> refundsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetRefundsPagedList. Salesperson={0}, refund={1}, customer={2}, from {3} to {4}, page: {5}",
                                                        salesPerson, refundId, customer, dateFrom, dateTo, page), ErrorHandler.ErrorVerboseLevel.Trace);
                refundsList = SAPRefunds.GetRefundsPagedList(oCompany, salesPerson, refundId, customer, dateFrom, dateTo, getDocuments, getDrafts, page);
                message = Request.CreateResponse(HttpStatusCode.OK, refundsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }





        [Route("api/refunds/orderItemsReturnedBatch")]
        public HttpResponseMessage GetOrderItemsReturnedBatchList([FromUri] string orderEntry)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMOrderRefundItemWithBatch> refundsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetOrderItemsReturnedList. Order DocEntry: (" + orderEntry + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                refundsList = SAPRefunds.GetOrderRefundsWithBatchs(oCompany, orderEntry);
                message = Request.CreateResponse(HttpStatusCode.OK, refundsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *******************************************************************************************************

        #region ADD Refund ***********************************************************************************************
        [Route("api/refunds/add")]
        public HttpResponseMessage PostAddRefund([FromBody] CRMRefundDetailModel refund)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* AddRefund", ErrorHandler.ErrorVerboseLevel.Trace);
                _error = SAPRefunds.AddRefund(oCompany, refund, false);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *******************************************************************************************************

        #region DRAFTS ***************************************************************************************************
        //****************************************************************************************************************

        #region ADD Draft Refund ***********************************************************************************************
        [Route("api/refunds/addDraft")]
        public HttpResponseMessage PostAddDraftRefund([FromBody] CRMRefundDetailModel refund)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* AddRefundDraft.", ErrorHandler.ErrorVerboseLevel.Trace);
                //_error = SAPRefundsDrafts.AddRefundDraft(oCompany, refund);
                _error = SAPRefunds.AddRefund(oCompany, refund, true);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *******************************************************************************************************

        #endregion *******************************************************************************************************





        #region OTHER DATA ***********************************************************************************************
        /*[Route("api/refunds/reasons")]
        public HttpResponseMessage GetRefundsReasonList()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBaseModel> rList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetRefundsReasonList.", ErrorHandler.ErrorVerboseLevel.Trace);
                rList = SAPRefundsData.GetRefundReasonList(oCompany);
                message = Request.CreateResponse(HttpStatusCode.OK, rList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }*/
        #endregion *******************************************************************************************************
    }
}
