﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class BPPaymentInfoController : ApiController
    {
        [Route("api/account/paymentInfo")]
        public HttpResponseMessage Get([FromUri] string idAccount)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMBPPaymentModel _payment;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get payment info for BPartner: " + idAccount, ErrorHandler.ErrorVerboseLevel.Trace);
                _payment = SAPBPPaymentInfo.GetBPPaymentInfo(oCompany, idAccount);
                if (_payment != null)
                    message = Request.CreateResponse(HttpStatusCode.OK, _payment,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/account/paymentUpdate")]
        public HttpResponseMessage PutUpdatePaymentInfo([FromBody] CRMBPPaymentModel paymentInfo)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Updating payment info:", ErrorHandler.ErrorVerboseLevel.Trace);
                if (paymentInfo != null)
                {
                    ErrorHandler.WriteInLog("   for BPartner: " + paymentInfo.Id, ErrorHandler.ErrorVerboseLevel.Trace);
                    _error = SAPBPPaymentInfo.UpdateBPPaymentInfo(oCompany, paymentInfo);
                    if (_error != null)
                        message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                        System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                    else
                        message = Request.CreateResponse(HttpStatusCode.NotFound);
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.BadRequest, "No data info received",
                                        System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                }
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/payment/methods")]
        public HttpResponseMessage GetMethods()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBPPaymentMethodModel> _methodList;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get payment methods. No CardCode", ErrorHandler.ErrorVerboseLevel.Trace);
                _methodList = SAPBPPaymentInfo.GetPaymentMethods(oCompany, null);
                if (_methodList != null)
                    message = Request.CreateResponse(HttpStatusCode.OK, _methodList,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/payment/methods")]
        public HttpResponseMessage GetMethods(string cardCode)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBPPaymentMethodModel> _methodList;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get payment methods. CardCode = " + cardCode, ErrorHandler.ErrorVerboseLevel.Trace);
                _methodList = SAPBPPaymentInfo.GetPaymentMethods(oCompany, cardCode);
                if (_methodList != null)
                    message = Request.CreateResponse(HttpStatusCode.OK, _methodList,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/payment/conditions")]
        public HttpResponseMessage GetConditions()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBPPaymentConditionModel> _pConditionsList;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get payment conditions.", ErrorHandler.ErrorVerboseLevel.Trace);
                _pConditionsList = SAPBPPaymentInfo.GetPaymentConditions(oCompany);
                if (_pConditionsList != null)
                    message = Request.CreateResponse(HttpStatusCode.OK, _pConditionsList,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/payment/bankCodes")]
        public HttpResponseMessage GetBankCodes()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBankCodeModel> _bankCodesList;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get bank codes list.", ErrorHandler.ErrorVerboseLevel.Trace);
                _bankCodesList = SAPBankData.GetBankCodesList(oCompany);
                if (_bankCodesList != null)
                    message = Request.CreateResponse(HttpStatusCode.OK, _bankCodesList,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        #region Business Partner payment misc info ******************************************************************
        [Route("api/account/creditInfo")]
        public HttpResponseMessage GetCreditInfo([FromUri] string cardCode)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMBPCreditInfo creditInfo;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("Get credit info for IC: " + cardCode, ErrorHandler.ErrorVerboseLevel.Trace);
                if (!string.IsNullOrWhiteSpace(cardCode))
                {
                    creditInfo = SAPPayments.GetCreditInfo(oCompany, cardCode);

                    message = Request.CreateResponse(HttpStatusCode.OK, creditInfo,
                                            System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;

        }
        #endregion **************************************************************************************************
    }
}
