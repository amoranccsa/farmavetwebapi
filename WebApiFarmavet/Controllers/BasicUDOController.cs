﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class BasicUDOController : ApiController
    {
        [Route("api/principios/list")]
        public HttpResponseMessage GetPrincipiosList()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBaseModel> PrincipiosList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetPrincipiosList", ErrorHandler.ErrorVerboseLevel.Trace);
                PrincipiosList = SAPBDGenerics.GetIDCustomAllListFromTable(oCompany, "@DOI_OPINAC", "U_Codi", "U_Desc", "");
                message = Request.CreateResponse(HttpStatusCode.OK, PrincipiosList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }



        [Route("api/species/list")]
        public HttpResponseMessage GetSpeciesList()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBaseModel> SpeciesList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetSpeciesList", ErrorHandler.ErrorVerboseLevel.Trace);
                SpeciesList = SAPBDGenerics.GetIDCustomAllListFromTable(oCompany, "@DOI_OESPANI", "U_Codi", "U_Desc", "");
                message = Request.CreateResponse(HttpStatusCode.OK, SpeciesList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
    }
}
