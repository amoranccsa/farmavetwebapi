﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class InvoicesController : ApiController
    {
        [Route("api/invoices/list")]
        public HttpResponseMessage GetList([FromUri] int salesPerson, string cardCode, string docNum, string cardName, string paymentMethod, string dateFrom, string dateTo)
        {
            //SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMInvoiceModel> invoicesList = null;


            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out SAPbobsCOM.Company oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get Invoices List: Sales person (" + salesPerson + "), CardCode (" + cardCode + "), Dates: " + dateFrom +" - " + dateTo,
                                        ErrorHandler.ErrorVerboseLevel.Trace);
                invoicesList = SAPInvoices.GetInvoicesList(oCompany, salesPerson, cardCode, docNum, cardName, paymentMethod, dateFrom, dateTo);
                message = Request.CreateResponse(HttpStatusCode.OK, invoicesList,
                                     System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #region Get Invoices individual info ******************************************************************************
        [Route("api/invoices/sumDocTotal")]
        public HttpResponseMessage GetSumDocTotal([FromUri] int salesPerson, string cardCode, string docNum, string cardName, string paymentMethod, string dateFrom, string dateTo)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            double sumDocTotal = 0;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("*Get Invoices SumDocTotal. Sales person: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                sumDocTotal = SAPInvoices.GetSumDocTotal(oCompany, salesPerson, cardCode, docNum, cardName, paymentMethod, dateFrom, dateTo);
                message = Request.CreateResponse(HttpStatusCode.OK, sumDocTotal,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion*******************************************************************************************************
    }
}
