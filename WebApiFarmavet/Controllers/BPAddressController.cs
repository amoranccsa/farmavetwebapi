﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    public class BPAddressController : ApiController
    {
        #region GET DATA *******************************************************************************************

        [Route("api/account/AddressList")]
        public HttpResponseMessage GetOrdersList([FromUri] string cardCode, string addressType)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBPAddressModel> addressList = null;
            SAPBPAddress.AddressType _addressType;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetAddressList: (" + cardCode + ")", ErrorHandler.ErrorVerboseLevel.Trace);

                //Checking valid values
                if (string.IsNullOrWhiteSpace(addressType))
                    _addressType = SAPBPAddress.AddressType.Both;
                else
                {
                    switch (addressType)
                    {
                        case "B":
                            _addressType = SAPBPAddress.AddressType.Bill;
                            break;
                        case "S":
                            _addressType = SAPBPAddress.AddressType.Shipping;
                            break;
                        default:
                            _addressType = SAPBPAddress.AddressType.Both;
                            statusCode = HttpStatusCode.BadRequest;
                            break;
                    }
                }

                if (statusCode == HttpStatusCode.OK)
                {
                    addressList = SAPBPAddress.GetAddressList(oCompany, cardCode, _addressType);
                    message = Request.CreateResponse(HttpStatusCode.OK, addressList,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                }
                else
                {
                    message = Request.CreateErrorResponse(statusCode, string.Format("The addressType '{0}' is not valid.", addressType));
                }
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion *************************************************************************************************

        #region ADD DATA *******************************************************************************************
        [Route("api/account/addAddressList")]
        public HttpResponseMessage PostAddresses([FromBody] CRMBPAddressListModel listModel)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Add addresses: (" + listModel.List.Count + ") for CardCode: " + listModel.Id, ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("Address type: " + listModel.List[0].Type, ErrorHandler.ErrorVerboseLevel.Trace);
                _error = SAPBPAddress.AddAddressList(oCompany, listModel);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *************************************************************************************************

        #region UPDATE DATA ****************************************************************************************

        [Route("api/account/updateAddressList")]
        public HttpResponseMessage PutUpdateAddressInfo([FromBody] CRMBPAddressListModel addressListInfo)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Updating payment info:", ErrorHandler.ErrorVerboseLevel.Trace);
                if (addressListInfo != null)
                {
                    ErrorHandler.WriteInLog("   for BPartner: " + addressListInfo.Id, ErrorHandler.ErrorVerboseLevel.Trace);
                    _error = SAPBPAddress.UpdateAddressList(oCompany, addressListInfo);
                    if (_error != null)
                        message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                        System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                    else
                        message = Request.CreateResponse(HttpStatusCode.NotFound);
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.BadRequest, "No data info received",
                                        System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                }
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion *************************************************************************************************

        
        
        #region retrieving info ************************************************************************************
        [Route("api/account/defaultAddress")]
        public HttpResponseMessage GetDefaultAddress([FromUri] string cardCode, string addressType)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string defaultAddress = null;
            SAPBPAddress.AddressType _addressType;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetAddressList: (" + cardCode + ")", ErrorHandler.ErrorVerboseLevel.Trace);

                //Checking valid values
                if (string.IsNullOrWhiteSpace(addressType))
                    _addressType = SAPBPAddress.AddressType.Both;
                else
                {
                    switch (addressType)
                    {
                        case "B":
                            _addressType = SAPBPAddress.AddressType.Bill;
                            break;
                        case "S":
                            _addressType = SAPBPAddress.AddressType.Shipping;
                            break;
                        default:
                            _addressType = SAPBPAddress.AddressType.Both;
                            statusCode = HttpStatusCode.BadRequest;
                            break;
                    }
                }

                if (statusCode == HttpStatusCode.OK)
                {
                    if (_addressType == SAPBPAddress.AddressType.Bill)
                        defaultAddress = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRD", "BillToDef", string.Format("\"CardCode\" = '{0}'", cardCode));
                    else
                        defaultAddress = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRD", "ShipToDef", string.Format("\"CardCode\" = '{0}'", cardCode));

                    message = Request.CreateResponse(HttpStatusCode.OK, defaultAddress,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                }
                else
                {
                    message = Request.CreateErrorResponse(statusCode, string.Format("The addressType '{0}' is not valid.", addressType));
                }
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *************************************************************************************************
    }
}
