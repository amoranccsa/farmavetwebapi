﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class CreditNotesController : ApiController
    {
        #region GET CreditNotes List *****************************************************************************************
        [Route("api/creditNotes/list")]
        public HttpResponseMessage GetCreditNotesList([FromUri] int salesPerson)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMCreditNoteModel> creditNotesList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetCreditNotesList. Sales person: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                creditNotesList = SAPCreditNotes.GetCreditNotesList(oCompany, salesPerson, null, null, null, null, false);
                message = Request.CreateResponse(HttpStatusCode.OK, creditNotesList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/creditNotes/list")]
        public HttpResponseMessage GetCreditNotesList([FromUri] int salesPerson, string creditNoteId, string customer, string dateFrom, string dateTo)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMCreditNoteModel> creditNotesList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetCreditNotesList. Salesperson={0}, creditNote={1}, customer={2}, from {3} to {4}",
                                                        salesPerson, creditNoteId, customer, dateFrom, dateTo), ErrorHandler.ErrorVerboseLevel.Trace);
                creditNotesList = SAPCreditNotes.GetCreditNotesList(oCompany, salesPerson, creditNoteId, customer, dateFrom, dateTo, false);
                message = Request.CreateResponse(HttpStatusCode.OK, creditNotesList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }



        [Route("api/creditNotes/draftList")]
        public HttpResponseMessage GetCreditNotesDraftList([FromUri] int salesPerson)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMCreditNoteModel> creditNotesList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetCreditNotesDraftList. Sales person: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                creditNotesList = SAPCreditNotes.GetCreditNotesList(oCompany, salesPerson, null, null, null, null, true);
                message = Request.CreateResponse(HttpStatusCode.OK, creditNotesList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/creditNotes/draftList")]
        public HttpResponseMessage GetCreditNotesDraftList([FromUri] int salesPerson, string creditNoteId, string customer, string dateFrom, string dateTo)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMCreditNoteModel> creditNotesList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetCreditNotesDraftList. Salesperson={0}, CreditNote={1}, customer={2}, from {3} to {4}",
                                                        salesPerson, creditNoteId, customer, dateFrom, dateTo), ErrorHandler.ErrorVerboseLevel.Trace);
                creditNotesList = SAPCreditNotes.GetCreditNotesList(oCompany, salesPerson, creditNoteId, customer, dateFrom, dateTo, true);
                message = Request.CreateResponse(HttpStatusCode.OK, creditNotesList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }




        [Route("api/creditNotes/list")]
        public HttpResponseMessage GetCreditNotesPagedList([FromUri] int salesPerson, bool getDocuments, bool getDrafts, int page)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMCreditNoteModel> creditNotesList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetCreditNotesPagedList. Sales person: (" + salesPerson + "). Page: " + page, ErrorHandler.ErrorVerboseLevel.Trace);
                creditNotesList = SAPCreditNotes.GetCreditNotesPagedList(oCompany, salesPerson, null, null, null, null, getDocuments, getDrafts, page);
                message = Request.CreateResponse(HttpStatusCode.OK, creditNotesList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/creditNotes/list")]
        public HttpResponseMessage GetCreditNotesPagedList([FromUri] int salesPerson, string creditNoteId, string customer, string dateFrom, string dateTo,
                                                           bool getDocuments, bool getDrafts, int page)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMCreditNoteModel> creditNotesList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetCreditNotesPagedList. Salesperson={0}, creditNote={1}, customer={2}, from {3} to {4}, page: {5}",
                                                        salesPerson, creditNoteId, customer, dateFrom, dateTo, page), ErrorHandler.ErrorVerboseLevel.Trace);
                creditNotesList = SAPCreditNotes.GetCreditNotesPagedList(oCompany, salesPerson, creditNoteId, customer, dateFrom, dateTo, getDocuments, getDrafts, page);
                message = Request.CreateResponse(HttpStatusCode.OK, creditNotesList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        
        #endregion *******************************************************************************************************

    }
}
