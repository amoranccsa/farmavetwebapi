﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class ApprovalRequestsController : ApiController
    {
        [Route("api/approvalrequest/list")]
        public HttpResponseMessage GetList([FromUri] int salesPerson)
        {
            //SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMApprovalRequestModel> arList = null;


            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out SAPbobsCOM.Company oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetApprovalRequestsList: salesPerson (" + salesPerson + ")",
                                        ErrorHandler.ErrorVerboseLevel.Trace);
                arList = SAPApprovalRequest.GetApprovalRequestList(oCompany, salesPerson);
                message = Request.CreateResponse(HttpStatusCode.OK, arList,
                                     System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }



        [Route("api/approvalrequest/approved")]
        public HttpResponseMessage GetApprovedList([FromUri] int salesPerson)
        {
            //SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMApprovalRequestModel> arList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out SAPbobsCOM.Company oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetApprovedRequestsList: salesPerson (" + salesPerson + ")",
                                        ErrorHandler.ErrorVerboseLevel.Trace);
                arList = SAPApprovalRequest.GetApprovedRequestList(oCompany, salesPerson);
                message = Request.CreateResponse(HttpStatusCode.OK, arList,
                                     System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
    }
}
