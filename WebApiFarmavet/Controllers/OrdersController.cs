﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class OrdersController : ApiController
    {
        #region GET ORDER *************************************************************************************************************
        [Route("api/orders/openList")]
        public HttpResponseMessage GetOpenOrdersList([FromUri] int salesPerson)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMOrderModel> ordersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("*GetOrdersList. Sales person: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                ordersList = SAPOrdersGet.GetOpenOrdersList(oCompany, false, salesPerson, null, null);
                message = Request.CreateResponse(HttpStatusCode.OK, ordersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/orders/openList")]
        public HttpResponseMessage GetOpenOrdersList([FromUri] int salesPerson, string order, string customer)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMOrderModel> ordersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("*GetOrdersList: SalesPerson={0}, order={1}, customer={2}",
                                        salesPerson, order, customer), ErrorHandler.ErrorVerboseLevel.Trace);
                ordersList = SAPOrdersGet.GetOpenOrdersList(oCompany, false, salesPerson, order, customer);
                message = Request.CreateResponse(HttpStatusCode.OK, ordersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }



        [Route("api/orders/completeList")]
        public HttpResponseMessage GetCompleteOrdersList([FromUri] int salesPerson)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMOrderModel> ordersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("*GetOrdersList. Sales person: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                ordersList = SAPOrdersGet.GetCompleteOrdersList(oCompany, false, salesPerson, null, null, null, null);
                message = Request.CreateResponse(HttpStatusCode.OK, ordersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/orders/completeList")]
        public HttpResponseMessage GetCompleteOrdersList([FromUri] int salesPerson, string order, string customer, string dateFrom, string dateTo)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMOrderModel> ordersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("*GetOrdersList: SalesPerson={0}, order={1}, customer={2}, from {3} to {4}",
                                        salesPerson, order, customer, dateFrom, dateTo), ErrorHandler.ErrorVerboseLevel.Trace);
                ordersList = SAPOrdersGet.GetCompleteOrdersList(oCompany, false, salesPerson, order, customer, dateFrom, dateTo);
                message = Request.CreateResponse(HttpStatusCode.OK, ordersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/orders/pagedList")]
        public HttpResponseMessage GetCompleteOrdersPagedList([FromUri] int salesPerson, bool getDocuments, bool getDrafts, string order, string customer, string dateFrom, string dateTo, int page)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMOrderModel> ordersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("*GetOrdersList: SalesPerson={0}, order={1}, customer={2}, from {3} to {4}, docs({5}) - drafts({6}), page: {7}",
                                        salesPerson, order, customer, dateFrom, dateTo, getDocuments, getDrafts, page), ErrorHandler.ErrorVerboseLevel.Trace);
                ordersList = SAPOrdersGet.GetCompletePagedOrdersList(oCompany, getDrafts, getDocuments, salesPerson, order, customer, dateFrom, dateTo, page);
                message = Request.CreateResponse(HttpStatusCode.OK, ordersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }





        [Route("api/orders/details")]
        public HttpResponseMessage GetOrderDetails([FromUri] string order)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMOrderDetailModel orderDetails = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("*GetOrdersDetails: (" + order + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                orderDetails = SAPOrdersGet.GetOrderDetails(oCompany, order, false);
                message = Request.CreateResponse(HttpStatusCode.OK, orderDetails,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *******************************************************************************************************************


        #region ADD ORDER ************************************************************************************************************

        private HttpResponseMessage AddOrder(CRMOrderDetailModel order, bool asDraft)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* AddOrder: (" + order + ") as draft: (" + asDraft + ") slpCode(" + order.salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                _error = SAPOrdersAddUpdate.AddOrder(oCompany, order, asDraft);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/orders/add")]
        public HttpResponseMessage PostAddOrder([FromBody] CRMOrderDetailModel order)
        {
            return AddOrder(order, false);
        }

        [Route("api/orders/addDraft")]
        public HttpResponseMessage PostAddOrderDraft([FromBody] CRMOrderDetailModel order)
        {
            return AddOrder(order, true);
        }

        #endregion ********************************************************************************************************************

        #region UPDATE ORDER **********************************************************************************************************
        [Route("api/orders/update")]
        public HttpResponseMessage PutUpdateOrder([FromBody] CRMOrderDetailModel order)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Update order: (" + order.Id + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                _error = SAPOrdersAddUpdate.UpdateOrder(oCompany, order);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion *******************************************************************************************************************

        #region DELETE ORDER *********************************************************************************************************
        [Route("api/orders/delete")]
        public HttpResponseMessage DeleteOrder([FromUri] string order)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Delete Order: (" + order + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                _error = SAPOrdersAddUpdate.DeleteOrder(oCompany, order, false);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *******************************************************************************************************************

        #region other orders list ***************************************************************************************
        [Route("api/orders/withDeliveries")]
        public HttpResponseMessage GetOrdersWithDeliveriesList([FromUri] int salesPerson, string order, string customer, string dateFrom, string dateTo)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMOrderModel> ordersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetOrdersList with deliveries. Salesperson={0}, order={1}, customer={2}, from {3} to {4}",
                                                        salesPerson, order, customer, dateFrom, dateTo), ErrorHandler.ErrorVerboseLevel.Trace);
                ordersList = SAPOrdersGet.GetOrdersWithDeliveriesList(oCompany, salesPerson, order, customer, dateFrom, dateTo);
                message = Request.CreateResponse(HttpStatusCode.OK, ordersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/orders/withDeliveries")]
        public HttpResponseMessage GetOrdersWithDeliveriesList([FromUri] int salesPerson, string order, string customer, string dateFrom, string dateTo, int page)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMOrderModel> ordersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetOrdersList with deliveries (paged). Salesperson={0}, order={1}, customer={2}, from {3} to {4}, page {5}",
                                                        salesPerson, order, customer, dateFrom, dateTo, page), ErrorHandler.ErrorVerboseLevel.Trace);
                ordersList = SAPOrdersGet.GetOrdersWithDeliveriesList(oCompany, salesPerson, order, customer, dateFrom, dateTo, page);
                message = Request.CreateResponse(HttpStatusCode.OK, ordersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion ******************************************************************************************************





        #region GET DRAFT *************************************************************************************************************
        [Route("api/orders/draftList")]
        public HttpResponseMessage GetOrdersDraftList([FromUri] int salesPerson)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMOrderModel> ordersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("*GetOrdersList. Sales person: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                ordersList = SAPOrdersGet.GetOpenOrdersList(oCompany, true, salesPerson, null, null);
                message = Request.CreateResponse(HttpStatusCode.OK, ordersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/orders/draftList")]
        public HttpResponseMessage GetOrdersDraftList([FromUri] int salesPerson, string order, string customer)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMOrderModel> ordersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("*GetOrdersList: SalesPerson={0}, order={1}, customer={2}",
                                        salesPerson, order, customer), ErrorHandler.ErrorVerboseLevel.Trace);
                ordersList = SAPOrdersGet.GetOpenOrdersList(oCompany, true, salesPerson, order, customer);
                message = Request.CreateResponse(HttpStatusCode.OK, ordersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/orders/draftDetails")]
        public HttpResponseMessage GetOrderDraftDetails([FromUri] string order)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMOrderDetailModel orderDetails = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("*GetOrdersDetails: (" + order + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                orderDetails = SAPOrdersGet.GetOrderDetails(oCompany, order, true);
                message = Request.CreateResponse(HttpStatusCode.OK, orderDetails,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *******************************************************************************************************************

        #region DELETE ORDER DRAFT ***************************************************************************************************
        [Route("api/orders/deleteDraft")]
        public HttpResponseMessage DeleteOrderDraft([FromUri] string order)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Delete Order: (" + order + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                _error = SAPOrdersAddUpdate.DeleteOrder(oCompany, order, true);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *******************************************************************************************************************
    }
}
