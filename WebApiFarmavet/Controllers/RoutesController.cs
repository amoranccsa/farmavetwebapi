﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class RoutesController : ApiController
    {
        #region GET routes List *****************************************************************************************
        [Route("api/routes/list")]
        public HttpResponseMessage GetRoutesList()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBaseModel> refundsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetRoutesList.", ErrorHandler.ErrorVerboseLevel.Trace);
                refundsList = SAPRoutes.GetRoutesList(oCompany);
                message = Request.CreateResponse(HttpStatusCode.OK, refundsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *****************************************************************************************************
    }
}
