﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class CRMBusinessPartnersController : ApiController
    {

        #region CUSTOMERS list sumarized ********************************************************************************
        [Route("api/customers/summarized")]
        public HttpResponseMessage GetSummarizedCustomersList([FromUri] int salesPerson)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMSummaryAccountModel> customersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetCustomersSummarizedList: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                customersList = SAPBusinessPartners.GetSummarizedCustomersList(oCompany, salesPerson);
                message = Request.CreateResponse(HttpStatusCode.OK, customersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/customers/summarized")]
        public HttpResponseMessage GetSummarizedCustomersList([FromUri] int salesPerson,string cardCode, string cardName, string contact, string route)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMSummaryAccountModel> customersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetCustomersSummarizedList: (" + salesPerson + "): " +
                                        "CardCode = " + cardCode + ", " +
                                        "CardName = " + cardName + ", " +
                                        "Contact = " + contact + ", " +
                                        "Route = " + route, ErrorHandler.ErrorVerboseLevel.Trace);
                customersList = SAPBusinessPartners.GetSummarizedCustomersList(oCompany, salesPerson, cardCode, cardName, contact, route);
                message = Request.CreateResponse(HttpStatusCode.OK, customersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion ******************************************************************************************************

        
        #region CUSTOMERS individual data *******************************************************************************

        [Route("api/account/customer")]
        public HttpResponseMessage GetCustomer([FromUri] string idAccount)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMAccountModel _customer;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("Get customer data: " + idAccount, ErrorHandler.ErrorVerboseLevel.Trace);
                _customer = SAPBusinessPartners.GetCustomer(oCompany, idAccount);
                if (_customer != null)
                    message = Request.CreateResponse(HttpStatusCode.OK, _customer,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion ******************************************************************************************************



        #region LEADS list sumarized ************************************************************************************
        [Route("api/leads/summarized")]
        public HttpResponseMessage GetSummarizedLeadsList([FromUri] int salesPerson)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMSummaryAccountModel> leadsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("GetLeadsSummarizedList: (" + salesPerson + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                //leadsList = SAPBusinessPartners.GetSummarizedLeadsList(oCompany, salesPerson);
                leadsList = SAPBusinessPartners.GetSummarizedLeadsList(oCompany, salesPerson, null, null, null, null);  //Redirigimos a los filtros para añadir filtros específicos de Harineras
                message = Request.CreateResponse(HttpStatusCode.OK, leadsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/leads/summarized")]
        public HttpResponseMessage GetSummarizedLeadsList([FromUri] int salesPerson, string cardCode, string cardName, string contact, string route)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMSummaryAccountModel> leadsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("GetLeadsSummarizedList: (" + salesPerson + "): " +
                                        "CardCode = " + cardCode + ", " +
                                        "CardName = " + cardName + ", " +
                                        "Contact = " + contact + ", " +
                                        "Route = " + route, ErrorHandler.ErrorVerboseLevel.Trace);
                leadsList = SAPBusinessPartners.GetSummarizedLeadsList(oCompany, salesPerson, cardCode, cardName, contact, route);
                message = Request.CreateResponse(HttpStatusCode.OK, leadsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion ******************************************************************************************************

        #region LEADS individual data ***********************************************************************************

        [Route("api/account/lead")]
        public HttpResponseMessage GetLead([FromUri] string idAccount)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMAccountModel _lead;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("Get lead data: " + idAccount, ErrorHandler.ErrorVerboseLevel.Trace);
                _lead = SAPBusinessPartners.GetLead(oCompany, idAccount);
                if (_lead != null)
                    message = Request.CreateResponse(HttpStatusCode.OK, _lead,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion ******************************************************************************************************



        #region CUSTOMERS paged list sumarized **************************************************************************
        [Route("api/accounts/summarized")]
        public HttpResponseMessage GetSummarizedBPPagedList([FromUri] int salesPerson, int page)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMSummaryAccountModel> customersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetCustomersSummarizedList: (" + salesPerson + "), page: " + page, ErrorHandler.ErrorVerboseLevel.Trace);
                customersList = SAPBusinessPartners.GetSumarizedBusinessPartnerPagedList(oCompany, true, true, true, salesPerson, null, null, null, null, null, page);
                message = Request.CreateResponse(HttpStatusCode.OK, customersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/accounts/summarized")]
        public HttpResponseMessage GetSummarizedBPPagedList([FromUri] int salesPerson, string cardCode, string cardName, string contact, string route, int page)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMSummaryAccountModel> customersList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetCustomersSummarizedList: (" + salesPerson + "): " +
                                        "CardCode = " + cardCode + ", " +
                                        "CardName = " + cardName + ", " +
                                        "Contact = " + contact + ", " +
                                        "Route = " + route + ", " +
                                        "Page = " + page, ErrorHandler.ErrorVerboseLevel.Trace);
                customersList = SAPBusinessPartners.GetSumarizedBusinessPartnerPagedList(oCompany, true, true, true, salesPerson, cardCode, cardName, contact, route, null, page);
                message = Request.CreateResponse(HttpStatusCode.OK, customersList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion ******************************************************************************************************



        #region ADD new Business Partner ********************************************************************************
        [Route("api/account/add")]
        public HttpResponseMessage PostAddBP([FromBody] CRMAccountModel account)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Add Customer: (" + account.Company + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                _error = SAPBusinessPartnersAddUpdate.AddBusinessPartner(oCompany, account);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion ******************************************************************************************************
        
        #region UPDATE Business Partner **********************************************************************************

        [Route("api/account/update")]
        public HttpResponseMessage PutUpdateBP([FromBody] CRMAccountModel account)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                _error = SAPBusinessPartnersAddUpdate.UpdateBusinessPartner(oCompany, account);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion *******************************************************************************************************

        #region DELETE Business Partner **********************************************************************************
        [Route("api/account/delete")]
        public HttpResponseMessage DeleteBP([FromUri] string idAccount)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                _error = SAPBusinessPartnersAddUpdate.DeleteBusinessPartner(oCompany, idAccount);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *******************************************************************************************************


    }
}