﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class ItemsController : ApiController
    {
        #region GET Item list ***************************************************************************************
        [Route("api/items/list")]
        public HttpResponseMessage GetItemsList()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMItemModel> itemsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetItemsList", ErrorHandler.ErrorVerboseLevel.Trace);
                itemsList = SAPItems.GetItemsList(oCompany, null, null, null, null, null, null, null, 0);
                message = Request.CreateResponse(HttpStatusCode.OK, itemsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/items/list")]
        public HttpResponseMessage GetItemsList([FromUri] string itemName, string specie, string principio1, string principio2, string principio3, string itemGroup, string itemCode, int itemPriceType)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMItemModel> itemsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetItemsList: itemName(" + itemName + "), specie(" + specie + 
                    "), principio1(" + principio1 + "), principio2(" + principio2 + "), principio3(" + principio3 +
                     "), itemGroup(" + itemGroup + "), itemCode(" + itemCode +
                     "), itemPriceType(" + itemPriceType.ToString() + ")",
                    ErrorHandler.ErrorVerboseLevel.Trace);
                itemsList = SAPItems.GetItemsList(oCompany, itemName, specie, principio1, principio2, principio3, itemGroup, itemCode, itemPriceType);
                message = Request.CreateResponse(HttpStatusCode.OK, itemsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion **************************************************************************************************


        #region GET Paged Item list *********************************************************************************
        [Route("api/items/list")]
        public HttpResponseMessage GetPagedItemsList([FromUri] int page, bool withSales)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMItemModel> itemsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetItemsList: page(" + page.ToString() + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                itemsList = SAPItems.GetPartialItemsList(oCompany, null, null, null, null, null, null, null, 0, page, null, withSales);
                message = Request.CreateResponse(HttpStatusCode.OK, itemsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/items/list")]
        public HttpResponseMessage GetPagedItemsList([FromUri] string itemName, string specie, string principio1, string principio2, string principio3, string itemGroup, string itemCode, int itemPriceType, int page, bool withSales)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMItemModel> itemsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetItemsList: itemName(" + itemName + "), specie(" + specie +
                     "), principio1(" + principio1 + "), principio2(" + principio2 + "), principio3(" + principio3 +
                     "), itemGroup(" + itemGroup + "), itemCode(" + itemCode +
                     "), itemPriceType(" + itemPriceType.ToString() + "), page(" + page.ToString() + "), with sales(" + withSales + ")",
                    ErrorHandler.ErrorVerboseLevel.Trace);
                itemsList = SAPItems.GetPartialItemsList(oCompany, itemName, specie, principio1, principio2, principio3, itemGroup, itemCode, itemPriceType, page, null, withSales);
                message = Request.CreateResponse(HttpStatusCode.OK, itemsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/items/list")]
        public HttpResponseMessage GetPagedItemsList([FromUri] string itemName, string specie, string principio1, string principio2, string principio3, string itemGroup, string itemCode, int itemPriceType, int page, string cardCode, bool withSales)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMItemModel> itemsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetItemsList: itemName(" + itemName + "), specie(" + specie +
                     "), principio1(" + principio1 + "), principio2(" + principio2 + "), principio3(" + principio3 +
                     "), itemGroup(" + itemGroup + "), itemCode(" + itemCode + "), cardCode(" + cardCode +
                     "), itemPriceType(" + itemPriceType.ToString() + "), page(" + page.ToString() + "), with sales (" + withSales + ")",
                    ErrorHandler.ErrorVerboseLevel.Trace);
                itemsList = SAPItems.GetPartialItemsList(oCompany, itemName, specie, principio1, principio2, principio3, itemGroup, itemCode, itemPriceType, page, cardCode, withSales);
                message = Request.CreateResponse(HttpStatusCode.OK, itemsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion **************************************************************************************************


        [Route("api/items/item")]
        public HttpResponseMessage GetItemDetails([FromUri] string itemCode)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMItemModel itemsData = null;
            string userCode;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetItemDetails: itemCode(" + itemCode + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                userCode = CRMControllersCommonFunctions.GetUserCodeFromConnection(Request);
                itemsData = SAPItems.GetItemDetails(oCompany, itemCode, System.Configuration.ConfigurationManager.AppSettings.Get("SellPriceList"), userCode);
                message = Request.CreateResponse(HttpStatusCode.OK, itemsData,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #region STOCK ***********************************************************************************************
        [Route("api/items/stockDetails")]
        public HttpResponseMessage GetItemStock([FromUri]string itemCode)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMStockModel> stockList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetStockDetails: ItemCode ({0})", itemCode), ErrorHandler.ErrorVerboseLevel.Trace);
                stockList = SAPItems.GetItemStock(oCompany, itemCode);
                message = Request.CreateResponse(HttpStatusCode.OK, stockList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion **************************************************************************************************

        #region ITEM GROUPS ****************************************************************************************
        [Route("api/itemgroups/list")]
        public HttpResponseMessage GetItemGroupsList()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBaseModel> ItemGroupsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetItemGroupsList", ErrorHandler.ErrorVerboseLevel.Trace);
                ItemGroupsList = SAPItems.GetItemGroupsList(oCompany);
                message = Request.CreateResponse(HttpStatusCode.OK, ItemGroupsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *************************************************************************************************

        #region ITEM DATA ******************************************************************************************
        [Route("api/items/getItemPrice")]
        public HttpResponseMessage GetItemPrice([FromUri] string cardCode, string itemCode, double quantity, string docDueDate)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            double itemsData;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetItemDetails: itemCode({0}), cardCode({1}), quantity={2}, date: {3}", itemCode, cardCode, quantity, docDueDate),
                                        ErrorHandler.ErrorVerboseLevel.Trace);
                itemsData = SAPItems.GetItemPrice(oCompany, cardCode, itemCode, quantity, docDueDate);
                ErrorHandler.WriteInLog(string.Format("* Item price = {0}", itemsData), ErrorHandler.ErrorVerboseLevel.Trace);
                message = Request.CreateResponse(HttpStatusCode.OK, itemsData,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        /*[Route("api/items/getItemPrice")]
        //public HttpResponseMessage GetItemPrice()
        public HttpResponseMessage GetItemPrice([FromUri] string cardCode, string itemCode, double quantity, string docDueDate)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            double itemsData = 5.0;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetItemDetails: itemCode(" +  ")",
                    ErrorHandler.ErrorVerboseLevel.Trace);

                ErrorHandler.WriteInLog("**********************************************************************************************", ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00001", "1", 1, new DateTime(2019, 05, 30).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00001", "1", 1, new DateTime(2019, 05, 13).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00001", "1", 1, new DateTime(2019, 05, 16).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00001", "1", 15, new DateTime(2019, 05, 14).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00001", "1", 34, new DateTime(2019, 05, 14).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);

                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00002", "1", 1, new DateTime(2019, 05, 30).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00002", "1", 1, new DateTime(2019, 05, 13).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00002", "1", 1, new DateTime(2019, 05, 16).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00002", "1", 15, new DateTime(2019, 05, 14).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00002", "1", 34, new DateTime(2019, 05, 14).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);

                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00002", "BAY00001", 1, new DateTime(2019, 05, 30).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00002", "BAY00001", 1, new DateTime(2019, 05, 13).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00002", "BAY00001", 1, new DateTime(2019, 05, 16).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00002", "BAY00001", 15, new DateTime(2019, 05, 14).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog("PRECIO: " + SAPItems.GetItemPrice(oCompany, "C00002", "BAY00001", 34, new DateTime(2019, 05, 14).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))), ErrorHandler.ErrorVerboseLevel.Trace);
               

                ErrorHandler.WriteInLog("**********************************************************************************************", ErrorHandler.ErrorVerboseLevel.Trace);


                message = Request.CreateResponse(HttpStatusCode.OK, itemsData,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }*/


        [Route("api/items/getItemDiscount")]
        public HttpResponseMessage GetItemDiscount([FromUri] string cardCode, string itemCode, double quantity, string docDueDate)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            int itemPriceList;
            double itemsData;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetItemDiscount: itemCode({0}), cardCode({1}), quantity={2}, date: {3}", itemCode, cardCode, quantity, docDueDate),
                                        ErrorHandler.ErrorVerboseLevel.Trace);

                if (string.IsNullOrWhiteSpace(cardCode)) {
                    if (!int.TryParse(SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRD", "ListNum", string.Format("\"CardCode\" = '{0}'", cardCode)), out itemPriceList))
                        itemPriceList = int.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("SellPriceList"));
                }
                else
                    itemPriceList = int.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("SellPriceList"));

                itemsData = SAPItems.GetItemDiscount(oCompany, itemPriceList, itemCode, cardCode, docDueDate, quantity);
                ErrorHandler.WriteInLog(string.Format("* Item price = {0}", itemsData), ErrorHandler.ErrorVerboseLevel.Trace);
                message = Request.CreateResponse(HttpStatusCode.OK, itemsData,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/items/getDogertyItemDiscount")]
        public HttpResponseMessage GetDogertyItemDiscount([FromUri] string cardCode, string itemCode, bool aplicaCam, double quantity, string docDueDate)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMDogertyItemDiscounts descuentos;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetDogertyItemDiscounts: aplica campaña ({4}), itemCode({0}), cardCode({1}), quantity={2}, date: {3}",
                                                      itemCode, cardCode, quantity, docDueDate, aplicaCam),
                                        ErrorHandler.ErrorVerboseLevel.Trace);

                descuentos = SAPItems.GetFarmavetItemDiscounts(oCompany, cardCode, itemCode, aplicaCam, quantity, docDueDate);

                message = Request.CreateResponse(HttpStatusCode.OK, descuentos,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }



        [Route("api/items/getDogertyCampanas")]
        public HttpResponseMessage GetDogertyCampanas([FromUri] string cardCode, string itemCode, string docDueDate)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBaseModel> campanas;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetDogertyCampañas: itemCode({0}), cardCode({1}), date: {2}", itemCode, cardCode, docDueDate),
                                        ErrorHandler.ErrorVerboseLevel.Trace);

                campanas = SAPItems.GetFarmavetItemCampanas(oCompany, cardCode, itemCode, docDueDate);
                /*campanas = new List<CRMBaseModel>();
                campanas.Add(new CRMBaseModel() { Id = "5", Name = "7" });*/
                //campanas.Add(new BaseModel() { Id = "10", Name = "14" });
                //campanas.Add(new BaseModel() { Id = "20", Name = "25" });

                message = Request.CreateResponse(HttpStatusCode.OK, campanas,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }



        [Route("api/items/getItemRE")]
        public HttpResponseMessage GetItemRE([FromUri] string cardCode, string itemCode, string docDueDate)
        {
            //AMM 20191030 SOPSAP-452 Recargo equivalencia

            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            int itemPriceList;
            double itemsData;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* GetItemRE: itemCode({0}), cardCode({1}), date: {2}", itemCode, cardCode, docDueDate),
                                        ErrorHandler.ErrorVerboseLevel.Trace);

                itemsData = SAPItems.GetItemRE(oCompany,  itemCode, cardCode, docDueDate);
                ErrorHandler.WriteInLog(string.Format("* Item % RE = {0}", itemsData), ErrorHandler.ErrorVerboseLevel.Trace);
                message = Request.CreateResponse(HttpStatusCode.OK, itemsData,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion *************************************************************************************************
    }
}
