﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class BatchsController : ApiController
    {
        #region GET DELIVERED BUT NOT RETURNED ITEMS BY BATCH *******************************************************
        [Route("api/batchs/deliveredNotReturned")]
        public HttpResponseMessage GetBatchsDnRList([FromUri] string itemCode, string cardCode)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBatchModel> batchsList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetBatchsList for items delivered but not returned. ItemCode: (" + itemCode+ "); CardCode: (" + cardCode + ")",
                                        ErrorHandler.ErrorVerboseLevel.Trace);
                batchsList = SAPBatchs.GetNotReturnedItemsByBatch(oCompany, itemCode, cardCode);
                message = Request.CreateResponse(HttpStatusCode.OK, batchsList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion **************************************************************************************************
    }
}
