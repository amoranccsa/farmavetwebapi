﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class DeliveriesController : ApiController
    {
        [Route("api/deliveries/itemsFromOrder")]
        public HttpResponseMessage GetDeliveredItemsFromOrder([FromUri] int salesPerson, string order, bool withBatchs)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMDeliveredItemModel> _deliveredItemList;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get delivered items from order: " + order, ErrorHandler.ErrorVerboseLevel.Trace);
                _deliveredItemList = SAPDeliveries.GetDeliveredItemsFromOrder(oCompany, salesPerson, order, withBatchs);
                if (_deliveredItemList != null)
                    message = Request.CreateResponse(HttpStatusCode.OK, _deliveredItemList,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/deliveries/itemInfo")]
        public HttpResponseMessage GetDeliveredItemInfo([FromUri] string cardCode, string itemCode)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMDeliveredItemInfoModel _deliveredItemInfo;

            if (string.IsNullOrWhiteSpace(itemCode))
            {
                message = Request.CreateResponse(HttpStatusCode.BadRequest, "ItemCode not valid.");
            }
            else
            {
                HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
                if (statusCode == HttpStatusCode.OK)
                {
                    ErrorHandler.WriteInLog("* Get delivered item info: " + itemCode, ErrorHandler.ErrorVerboseLevel.Trace);
                    _deliveredItemInfo = SAPDeliveries.GetDeliveredItemInfo(oCompany, cardCode, itemCode);
                    if (_deliveredItemInfo != null)
                        message = Request.CreateResponse(HttpStatusCode.OK, _deliveredItemInfo,
                                        System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                    else
                        message = Request.CreateResponse(HttpStatusCode.NotFound);
                }
                else
                {
                    message = Request.CreateResponse(statusCode);
                }
            }

            return message;
        }
    }
}
