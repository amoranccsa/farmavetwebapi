﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    public class ContactPersonController : ApiController
    {
        #region Contact person list data *********************************************************************************

        [Route("api/account/contactList")]
        public HttpResponseMessage Get([FromUri] string idAccount)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBPContactModel> _contactList;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get contact list for BPartner: " + idAccount, ErrorHandler.ErrorVerboseLevel.Trace);
                _contactList = SAPContactPerson.GetContactList(oCompany, idAccount);
                if (_contactList != null)
                    message = Request.CreateResponse(HttpStatusCode.OK, _contactList,
                                    System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/account/updateContactList")]
        public HttpResponseMessage Put([FromBody] CRMBPContactListModel contactList)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("Updating contact list for BPartner: " + contactList.Id, ErrorHandler.ErrorVerboseLevel.Trace);
                _error = SAPContactPerson.UpdateContactList(oCompany, contactList);
                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        #endregion ******************************************************************************************************
    }
}
