﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Services;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class ReportsController : ApiController
    {

        [Route("api/reports/ventas")]
        public HttpResponseMessage GetVentas([FromUri]int salesPerson, string dateFrom, string dateTo)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string fileName;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetInformeVentas: salesPerson (" + salesPerson + "), Fechas(" + dateFrom +"," + dateTo + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                try
                {

                    //AMM 20191126 SOPSAP-481 OJO! pequeños BUG con el formato de las fechas... usar el parametro formato fecha de la webapi para "no confundir"
                    //dateFrom = DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                    //                        System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"));
                     
                    //dateTo = DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                    //                        System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"));

                 
                    fileName = CrystalReportToPDF.InformeVentasToPDF(oCompany, salesPerson, dateFrom, dateTo);
                    ErrorHandler.WriteInLog("** Generado fichero: " + fileName, ErrorHandler.ErrorVerboseLevel.Trace);
                    //baseFilterData.Id = "1v3.pdf";
                    if (!string.IsNullOrWhiteSpace(fileName))
                        message = Request.CreateResponse(HttpStatusCode.OK, fileName,
                                            System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                    else
                        message = Request.CreateResponse(HttpStatusCode.NotFound);
                    
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION GetInformeVentas: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                    message = Request.CreateResponse(HttpStatusCode.NotFound, ex);
                }
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/reports/factura")]
        public HttpResponseMessage GetFactura([FromUri]string docEntry)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string fileName;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetInformeFacturas: docEntry (" + docEntry + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                try
                {
                    fileName = CrystalReportToPDF.InformeFacturaToPDF(oCompany, docEntry);
                    ErrorHandler.WriteInLog("** Generado fichero: " + fileName, ErrorHandler.ErrorVerboseLevel.Trace);
                    //baseFilterData.Id = "1v3.pdf";
                    if (!string.IsNullOrWhiteSpace(fileName))
                        message = Request.CreateResponse(HttpStatusCode.OK, fileName,
                                            System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                    else
                        message = Request.CreateResponse(HttpStatusCode.NotFound);

                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION GetInformeFactura: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                    message = Request.CreateResponse(HttpStatusCode.NotFound, ex);
                }
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/reports/devolucion")]
        public HttpResponseMessage GetDevolucion([FromUri]string docEntry, bool isDraft)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string fileName;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetInformeDevolucion: docEntry (" + docEntry + "), Borrador(" + isDraft + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                try
                {
                    fileName = CrystalReportToPDF.InformeDevolucionToPDF(oCompany, docEntry, isDraft);
                    ErrorHandler.WriteInLog("** Generado fichero: " + fileName, ErrorHandler.ErrorVerboseLevel.Trace);
                    //baseFilterData.Id = "1v3.pdf";
                    if (!string.IsNullOrWhiteSpace(fileName))
                        message = Request.CreateResponse(HttpStatusCode.OK, fileName,
                                            System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                    else
                        message = Request.CreateResponse(HttpStatusCode.NotFound);

                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION GetInformeDevolucion: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                    message = Request.CreateResponse(HttpStatusCode.NotFound, ex);
                }
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }



        [Route("api/reports/abono")]
        public HttpResponseMessage GetAbono([FromUri]string docEntry, bool isDraft)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string fileName;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetInformeAbono: docEntry (" + docEntry + "), Borrador(" + isDraft + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                try
                {
                    fileName = CrystalReportToPDF.InformeAbonoToPDF(oCompany, docEntry, isDraft);
                    ErrorHandler.WriteInLog("** Generado fichero: " + fileName, ErrorHandler.ErrorVerboseLevel.Trace);
                    //baseFilterData.Id = "1v3.pdf";
                    if (!string.IsNullOrWhiteSpace(fileName))
                        message = Request.CreateResponse(HttpStatusCode.OK, fileName,
                                            System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                    else
                        message = Request.CreateResponse(HttpStatusCode.NotFound);

                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION GetInformeAbono: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                    message = Request.CreateResponse(HttpStatusCode.NotFound, ex);
                }
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
    }
}
