﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class SalesController : ApiController
    {
        #region GET Sales List *****************************************************************************************
        [Route("api/sales/summarized")]
        public HttpResponseMessage GetSummarizedSalesList([FromUri] string itemCode)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMBaseModel> salesList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetSummarizedSalesList. item code (" + itemCode + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                salesList = SAPSalesData.GetSummarizedSalesList(oCompany, itemCode);
                message = Request.CreateResponse(HttpStatusCode.OK, salesList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/sales/list")]
        public HttpResponseMessage GetSalesList()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMSaleModel> salesList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetSalesList. ()", ErrorHandler.ErrorVerboseLevel.Trace);
                salesList = SAPSalesData.GetSalesList(oCompany, null);
                message = Request.CreateResponse(HttpStatusCode.OK, salesList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/sales/list")]
        public HttpResponseMessage GetSalesList([FromUri] string itemCode)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMSaleModel> salesList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetSummarizedSalesList. item code (" + itemCode + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                salesList = SAPSalesData.GetSalesList(oCompany, itemCode);
                message = Request.CreateResponse(HttpStatusCode.OK, salesList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *****************************************************************************************************


        #region GET Sale details ***************************************************************************************
        [Route("api/sales/details")]
        public HttpResponseMessage GetSaleDetails([FromUri] string saleCode)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMSaleModel saleData = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetSaleData. sale code (" + saleCode + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                saleData = SAPSalesData.GetSaleDetails(oCompany, saleCode);
                message = Request.CreateResponse(HttpStatusCode.OK, saleData,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *****************************************************************************************************



        #region GET other sale data ************************************************************************************
        [Route("api/sales/prices")]
        [HttpPut]
        public HttpResponseMessage SalePricesAndAvaiable([FromBody] CRMSaleModel saleData)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMItemModel> pricesData = null;
            string userCode;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetSalePricesAndAvaiable. sale code (" + saleData.Id + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                userCode = CRMControllersCommonFunctions.GetUserCodeFromConnection(Request);
                pricesData = SAPSalesData.GetPricesAndAvaiable(oCompany, saleData, userCode);
                message = Request.CreateResponse(HttpStatusCode.OK, pricesData,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *****************************************************************************************************
    }
}
