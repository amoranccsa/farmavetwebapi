﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFarmavet.Models;
using WebApiFarmavet.Services;
using WebApiFarmavet.Services.SAP;

namespace WebApiFarmavet.Controllers
{
    [Filters.TokenAuthentication]
    public class WarehousesController : ApiController
    {
        #region GET Warehouses distances and stock List ****************************************************************
        [Route("api/warehouses/distances")]
        public HttpResponseMessage GetWhsDistancesStockList([FromUri] string itemCode, string state)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<CRMWhsDistanceStockModel> whsDistancesStockList = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetWhsDistancesStockList. item code: (" + itemCode + "); state: (" + state + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                whsDistancesStockList = SAPWarehouses.GetWhsDistanceStock(oCompany, itemCode, state);
                message = Request.CreateResponse(HttpStatusCode.OK, whsDistancesStockList,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *****************************************************************************************************

        #region GET default Warehouse **********************************************************************************
        [Route("api/warehouses/default")]
        public HttpResponseMessage GetDefaultWarehouse([FromUri] string userCode)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            CRMBaseModel defaultWhs = null;

            HttpStatusCode statusCode = CRMControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* GetDefaultWarehouse. user code: (" + userCode + ")", ErrorHandler.ErrorVerboseLevel.Trace);
                defaultWhs = SAPWarehouses.GetDefaultWarehouse(oCompany, userCode);
                message = Request.CreateResponse(HttpStatusCode.OK, defaultWhs,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
        #endregion *****************************************************************************************************
    }
}
