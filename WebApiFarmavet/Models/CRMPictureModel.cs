﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMPictureModel : CRMBaseModel
    {
        public enum Format { Unknown, PNG, JPG, BMP };

        //ID = FileName
        public Format format { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string error { get; set; }
        public byte[] pictureData { get; set; }
    }
}