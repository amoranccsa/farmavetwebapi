﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMInvoiceModel : CRMBaseModel
    {
        //ID = DocEntry
        //Name = DocNum
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public double DocTotal { get; set; }
        public DateTime DocDate { get; set; }
        public DateTime DocDueDate { get; set; }
        public bool Open { get; set; }
        public string PayMethodId { get; set; }
        public string PayMethodName { get; set; }
    }
}