﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMBankCodeModel : CRMBaseModel
    {
        //ID = BankCode
        public string BankName { get; set; }
        public string GLAccount { get; set; }   //Cuenta de mayor
    }

    public class CRMBankAccountModel : CRMBankCodeModel
    {
        public string Branch { get; set; }
        public string ControlKey { get; set; }
        public string Account { get; set; }
        public string IBAN { get; set; }
    }

    public class CRMBankModel
    {
    }
}