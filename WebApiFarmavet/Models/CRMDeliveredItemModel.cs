﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    #region Derivative models ******************************************************************
    //Use this property when serializing a model that inherits from a list
    [JsonObject(MemberSerialization = MemberSerialization.Fields)]
    public class CRMDeliveredItemModel : List<CRMItemModel>
    {
        public string docEntry { get; set; }
        public string docNum { get; set; }
        public DateTime docDate { get; set; }
    }

    public class CRMDeliveredItemLineModel : CRMItemModel
    {
        public int quantity;
        public double lineTotal;

        public string batchNum;
        public int batchQty;
    }

    public class CRMDeliveredItemInfoModel : CRMBaseModel
    {
        //ID = ItemCode
        public string itemName { get; set; }
        public int quantity { get; set; }
        public int openQty { get; set; }
    }
    #endregion *********************************************************************************

    public class CRMDeliveryModel
    {
    }
}