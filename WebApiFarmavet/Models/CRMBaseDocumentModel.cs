﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMBaseDocumentModel : CRMBaseModel
    {
        public string docNum { get; set; }
        public string cardCode { get; set; }
        public string cardName { get; set; }
        public double docTotalBefDiscount { get; set; }
        public double discPrcnt { get; set; }
        public double docTotal { get; set; }
        public bool draft { get; set; }
        public bool open { get; set; }
    }
}