﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMApprovalRequestModel : CRMBaseDocumentModel
    {
        public enum ApprovalRequestStatusEnum { None, Pending, Approved, NotApproved, Generated, GeneratedByAuthorizer, Cancelled };

        public DateTime docDueDate { get; set; }
        public string appReqEntry { get; set; }
        public string comments { get; set; }
        public ApprovalRequestStatusEnum status { get; set; }
    }

    /*public enum ApprovalRequestStatus
    {
        [DescriptionAttribute("Pendiente")] arsPending = 0,
        [DescriptionAttribute("Aprovado")] arsApproved = 1,
        [DescriptionAttribute("Rechazado")] arsNotApproved = 2,
        [DescriptionAttribute("Generado")] arsGenerated = 3,
        [DescriptionAttribute("GeneradoPorPropietario")] arsParsGeneratedByAuthorizerending = 4,
        [DescriptionAttribute("Cancelado")] arsCancelled = 5
    }


    public class CRMApprovalRequestModel : CRMBaseModel
    {

        public int Code { get; set; }
        public DateTime CreationDate { get; set; }
        public int ObjectEntry { get; set; }
        public int ObjectType { get; set; }
        public int OwnerID { get; set; }
        public string OwnerCode { get; set; }
        public string Remarks { get; set; }
        public ApprovalRequestStatus Status { get; set; }
        public Boolean IsDraft { get; set; }

        public int UserID { get; set; }
        public string UserCode { get; set; }
        public string UserPass { get; set; }
        public ApprovalRequestStatus StatusRespond { get; set; }
        public string RemarksRespond { get; set; }
        public DateTime RequestDate { get; set; }

        public int StepCode { get; set; }
        public string StepName { get; set; }
        public string StepRemarks { get; set; }

        public int DocNum { get; set; }
        public string DocName { get; set; }
        public string DocCardCode { get; set; }
        public string DocCardName { get; set; }
        public double DocTotal { get; set; }
        public DateTime DocDate { get; set; }
    }*/
}