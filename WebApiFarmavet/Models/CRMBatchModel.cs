﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMBatchModel : CRMBaseModel
    {
        //ID = BatchNum
        public int quantity { get; set; }
        public DateTime expDate { get; set; }
        public string itemCode { get; set; }
    }

    public class CRMSelectedQtyBatchModel : CRMBatchModel
    {
        //ID = BatchNum
        public int selQty { get; set; }
    }
}