﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    #region Business partners payment info sub-models *******************************************************************

    public class CRMBPPaymentMethodModel : CRMBaseModel
    {
        //ID = PayMethCod
        public string Description { get; set; }
    }

    public class CRMBPPaymentConditionModel : CRMBaseModel
    {
        //ID = GroupNum
        public string PymntGroup { get; set; }
    }

    public class CRMBPPaymentModel : CRMBaseModel
    {
        public string CCCBankCode { get; set; }
        public string CCCBranch { get; set; }
        public string CCCControlKey { get; set; }
        public string CCCAccount { get; set; }
        public string Iban { get; set; }
        public string PaymentGroupId { get; set; }
        public string PaymentGroup { get; set; }
        public string FreeText { get; set; }
        public string PaymentWayId { get; set; }
        public string PaymentWay { get; set; }
    }

    public class CRMBPCreditInfo : CRMBaseModel
    {
        public double CreditLine { get; set; }
        public double CurrentBalance { get; set; }
    }



    #endregion **********************************************************************************************************


    #region Business partners info sub-models ***************************************************************************

    public class CRMBPContactModel : CRMBaseModel
    {
        //ID = OCPR.CntctCode
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string MPhone { get; set; }
        public string E_Mail { get; set; }

        public override string ToString()
        {
            string _contact;

            if (string.IsNullOrWhiteSpace(FirstName) && string.IsNullOrWhiteSpace(MiddleName) &&
                string.IsNullOrWhiteSpace(LastName))
                _contact = Id;
            else
            {
                _contact = FirstName;
                if (!string.IsNullOrWhiteSpace(MiddleName))
                    _contact += " " + MiddleName;
                if (!string.IsNullOrWhiteSpace(LastName))
                    _contact += " " + LastName;
            }

            return _contact;
        }
    }

    public class CRMBPContactListModel : CRMBaseModel
    {
        //ID = CardCode
        public string CntctPrsnId { get; set; }
        public List<CRMBPContactModel> ContactList { get; set; }
    }

    public class CRMBPAddressModel : CRMBaseModel
    {
        public enum AddressType { Bill, Shipping, Both }
        //ID = CRD1.Address
        public string Street { get; set; }
        public string Building { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public AddressType Type { get; set; }

        //Código de Farmavet ***************************
        public string LicTradNum { get; set; }
        //**********************************************

        [JsonIgnore]
        public string AddressString
        {
            get
            {
                return string.Format(
                    "{0}{1} {2} {3} {4}",
                    Street,
                    !string.IsNullOrWhiteSpace(Building) ? " " + Building + "," : string.Empty + ",",
                    !string.IsNullOrWhiteSpace(City) ? City + "," : string.Empty,
                    StateName,
                    ZipCode);
            }
        }

        [JsonIgnore]
        public string CityState
        {
            get { return City + ", " + StateName; }
        }

        [JsonIgnore]
        public string CityStatePostal
        {
            get { return CityState + " " + ZipCode; }
        }

        [JsonIgnore]
        public string StatePostal
        {
            get { return StateName + " " + ZipCode; }
        }
    }

    public class CRMBPAddressListModel : CRMBaseModel
    {
        public List<CRMBPAddressModel> List { get; set; }
    }

    #endregion **********************************************************************************************************


    #region Business partner account models *****************************************************************************
    public class CRMSummaryAccountModel : CRMBaseModel
    {
        public enum BpType { Customer, Lead, Supplier }
        public CRMSummaryAccountModel()
        {
            FirstName = LastName = Company = string.Empty;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public BpType Type { get; set; }

        
        public string DisplayContact
        {
            get { return FirstName + " " + LastName; }
        }
    }

    public class CRMAccountModel : CRMSummaryAccountModel
    {
        public CRMAccountModel()
        {
            FirstName = LastName = Company = Phone1 = Phone2 = JobTitle = Email = Nif = string.Empty;
            BillAddress = null;
            ShippingAddress = null;
            Industry = string.Empty;
            //OpportunityStage = OpportunityStages[0];
            //IsLead = true;
        }

        //ID = CardCode
        public string CntctPrsnId { get; set; }
        public string JobTitle { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string MPhone { get; set; }
        public string Email { get; set; }
        public string Nif { get; set; }

        public CRMBPAddressModel BillAddress { get; set; }
        public CRMBPAddressModel ShippingAddress { get; set; }

        //public double Latitude { get; set; }
        //public double Longitude { get; set; }

        public int SalesPerson { get; set; }

        public string Industry { get; set; }
        public double OpportunitySize { get; set; }
        public double OpportunityPercent { get; set; }
        public string ImageUrl { get; set; }

        //FARMAVET DATA ***********************
        public string RouteId { get; set; }
        public string RouteName { get; set; }
        //*************************************


        [JsonIgnore]
        public string DisplayName
        {
            get { return this.ToString(); }
        }


        public override string ToString()
        {
            return FirstName + " " + LastName;
        }

    }

    #endregion **********************************************************************************************************
}