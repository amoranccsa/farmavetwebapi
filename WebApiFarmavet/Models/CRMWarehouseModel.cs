﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMWhsDistanceStockModel : CRMBaseModel
    {
        //ID = WhsCode
        //Name = WhsName
        public string codState { get; set; }
        public string nameState { get; set; }
        public int distanceOrder { get; set; }
        public double stock { get; set; }
        public double avaiable { get; set; }
    }

    public class CRMWarehouseModel
    {
    }
}