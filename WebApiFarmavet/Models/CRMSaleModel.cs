﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMSaleModel : CRMBaseModel
    {
        public enum SaleTypeEnum { Definido, MasBajo };
        public enum MakerEnum { Farmavet, Laboratorio };

        //ID = Code
        //Name = Name
        public int saleType { get; set; }
        public int maker { get; set; }
        public bool repoProv { get; set; }

        public List<CRMBaseModel> saleOrigin { get; set; }
        public double saleOriginQty { get; set; }
        public List<CRMBaseModel> saleDestiny { get; set; }
        public double saleDestinyQty { get; set; }

        public List<CRMBaseModel> pricesList { get; set; }
    }
}