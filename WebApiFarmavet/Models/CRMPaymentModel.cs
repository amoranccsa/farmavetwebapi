﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMPendingPaymentModel : CRMBaseDocumentModel
    {
        //ID = DocEntry
        public double paidToDate { get; set; }
        public DateTime docDate { get; set; }
        public DateTime docDueDate { get; set; }

        public string payMethodId { get; set; }
        public string payMethodName { get; set; }
    }

    public class CRMPaymentModel : CRMBaseModel
    {
        public enum PaymentModeType { cash, check };

        public string cardCode { get; set; }
        public int slpCode { get; set; }
        public double docTotal { get; set; }
        public PaymentModeType paymentMode { get; set; }
        public string bankCode { get; set; }
        public string checkNumber { get; set; }
        public DateTime checkDueDate { get; set; }
        public List<string> invoicesID { get; set; }
        public string comments { get; set; }

        public string userName { get; set; }
    }
}