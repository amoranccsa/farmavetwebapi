﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMItemModel : CRMBaseModel
    {
        public string itemName { get; set; }
        public string description { get; set; }
        public double price { get; set; }
        public double vatPercent { get; set; }
        public double avaiable { get; set; }
        public int invUnitMsrId { get; set; }
        public string invUnitMsr { get; set; }
        public int salesUnitMsrId { get; set; }
        public string salesUnitMsr { get; set; }

        public int baseLine { get; set; }
        public string pictureFile { get; set; }

        //Código Farmavet *******************************
        public string groupId { get; set; }
        public string groupName { get; set; }
        public double customerDscnt { get; set; }
        public int nSales { get; set; }

        public string defaultPriceListId { get; set; }
        public List<CRMItemListPrice> pricesList { get; set; }

        [JsonIgnore]
        public double UoMMultiplier { get; set; }
        //***********************************************
    }

    public class CRMItemListPrice
    {
        public string priceList { get; set; }
        public string listName { get; set; }
        public double price { get; set; }
    }


    //Código FARMAVET ******************************
    public class CRMDogertyItemDiscounts
    {
        public double dtoCampana { get; set; }
        public double dto1 { get; set; }
        public double dto2 { get; set; }
        public double dto3 { get; set; }
        public double dtoExtra { get; set; }
        public double dto4 { get; set; }

        public double GetDiscountedTotal (double lineTotal)
        {
            double temp = lineTotal;

            if  (Math.Round(dtoCampana, 2) > 0)
            {
                lineTotal -= (lineTotal * dtoCampana) / 100;
                if (Math.Round(dtoExtra, 2) > 0)
                    lineTotal -= (lineTotal * dtoExtra) / 100;
                if (Math.Round(dto4, 2) > 0)
                    lineTotal -= (lineTotal * dto4) / 100;
            }
            else
            {
                if (Math.Round(dto1, 2) > 0)
                    lineTotal -= (lineTotal * dto4) / 100;
                if (Math.Round(dto2, 2) > 0)
                    lineTotal -= (lineTotal * dto2) / 100;
                if (Math.Round(dto3, 2) > 0)
                    lineTotal -= (lineTotal * dto3) / 100;
                if (Math.Round(dtoExtra, 2) > 0)
                    lineTotal -= (lineTotal * dtoExtra) / 100;
                if (Math.Round(dto4, 2) > 0)
                    lineTotal -= (lineTotal * dto4) / 100;
            }

            return Math.Round(temp, 2);
        }
    }
    //**********************************************
}