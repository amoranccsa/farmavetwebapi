﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMRefundLineModel : CRMBaseModel
    {
        //public string LineNum {va a ser el ID del BaseModel del que hereda}
        public string itemCode;
        public string description;
        public int quantity;
        public double openQuantity;
        public int invUomEntry;
        public string invUomCode;
        public int salesUomEntry;
        public string salesUomCode;
        public double unitPrice;
        public double vatPercent;
        public double lineTotal;
        public string text;
        public string baseRef;
        public int baseLine;

        public List<CRMSelectedQtyBatchModel> batchList;

        public double discount;
    }


    public class CRMRefundModel : CRMBaseDocumentModel
    {
        //ID = DocEntry
        public DateTime docDate { get; set; }
    }

    public class CRMRefundDetailModel : CRMRefundModel
    {
        public string comments;
        public string reasonId;
        public int slpCode;
        public List<CRMRefundLineModel> lines;

        public CRMRefundDetailModel() { }

        public CRMRefundDetailModel(bool defaultValues)
        {
            if (defaultValues)
            {
                docDate = DateTime.Now;
                lines = new List<CRMRefundLineModel>();
            }
        }
    }


    public class CRMOrderRefundItemWithBatch : CRMBaseModel
    {
        //ID = Order DocEntry
        public string deliveryEntry;
        public string returnEntry;
        public string itemCode;
        public int quantity;
        public string batchNum;
        public int batchQty;
    }
}