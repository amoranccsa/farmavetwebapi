﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMOrderLineModel : CRMBaseModel
    {
        //public string LineNum {va a ser el ID del BaseModel del que hereda}
        public string itemCode { get; set; }
        public string description { get; set; }
        public int quantity { get; set; }
        public double openQuantity { get; set; }
        public double servedQuantity { get; set; }
        public int invUomEntry { get; set; }
        public string invUomCode { get; set; }
        public int salesUomEntry { get; set; }
        public string salesUomCode { get; set; }
        public double unitPrice { get; set; }
        public double vatPercent { get; set; }
        public double lineTotal { get; set; }
        public string text { get; set; }

        //Código Farmavet ***************************
        public double discPrcnt { get; set; }
        public bool lineClosed { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string saleCode { get; set; }
        public string saleName { get; set; }
        public string saleComPro { get; set; }

        public CRMDogertyItemDiscounts dogertyDiscounts { get; set; }
        public string nombreCampana { get; set; }
        public bool aplicarCampana { get; set; }
        //*******************************************

    }

    public class CRMOrderModel : CRMBaseDocumentModel
    {
        public DateTime docDate { get; set; }
        public DateTime docDueDate { get; set; }
    }

    public enum OrderType { Normal, SinCargo }

    public class CRMOrderDetailModel : CRMOrderModel
    {
        public OrderType type { get; set; }
        public string comments { get; set; }
        public CRMBPAddressModel shipAddress { get; set; }
        public int salesPerson { get; set; }

        //Código Farmavet ***************************
        public int paymentTermsId { get; set; }
        public string paymentTermsName { get; set; }
        public string paymentMethodId { get; set; }
        public string paymentMethodName { get; set; }
        public string authComments { get; set; }
        //*******************************************

        public List<CRMOrderLineModel> lines { get; set; }
    }

    public class CRMTruckOrderModel : CRMOrderModel
    {
        public string truck { get; set; }
        public double weight { get; set; }
        public string shipAddress { get; set; }
    }
}