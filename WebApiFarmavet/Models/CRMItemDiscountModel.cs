﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMItemDiscountModel : CRMBaseModel
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string cardCode { get; set; }
        public int listNum { get; set; }
        public string listName { get; set; }
        public double finalDisc { get; set; }

        //Descuento genérico o por cliente
        public double dto { get; set; }
        public double price { get; set; }

        //Descuento por periodo
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public double dtoP { get; set; }
        public double priceP { get; set; }

        //Descuento por cantidad
        public double amount { get; set; }
        public double dtoPQ { get; set; }
        public double pricePQ { get; set; }
    }
}