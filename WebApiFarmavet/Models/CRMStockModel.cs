﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Models
{
    public class CRMStockModel : CRMBaseModel
    {
        //ID = itemCode
        //Name = itemName
        public string WhsCode { get; set; }
        public string WhsName { get; set; }
        public double Stock { get; set; }
        public double Reserved { get; set; }
        public int IUoMEntry { get; set; }
        public string IUoMName { get; set; }
    }
}