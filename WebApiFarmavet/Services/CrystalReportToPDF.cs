﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Services
{
    public static class CrystalReportToPDF
    {
        public static readonly string rutaInstalacion = HttpContext.Current.Server.MapPath("~");
        //private static readonly string rutaInstalacion = @"C:\CCSA\DemoWebApi\";
        //private static readonly string rutaInstalacion = @"C:\Users\Administrador\Documents\Visual Studio 2017\Projects\PruebaRendimiento02_web\PruebaRendimiento02_web\";
        private static readonly string carpetaInformes = rutaInstalacion + @"informes\informes\";
        //private static readonly string rpt_informeVentas = @"./informes/plantillasinformes/InformeVentas.rpt";
        private static readonly string rpt_informeVentas = rutaInstalacion + @"informes\plantillas\app_INFORME_VENTAS.rpt";
        private static readonly string rpt_informeFactura = rutaInstalacion + @"informes\plantillas\DOGERTY-FACTURA_DE_CLIENTE_3.rpt";
        private static readonly string rpt_informeDevolucion = rutaInstalacion + @"informes\plantillas\app_DEVOLUCIONES.rpt";
        private static readonly string rpt_informeDevolucionBorrador = rutaInstalacion + @"informes\plantillas\app_DEVOLUCIONES_BORRADOR.rpt";
        private static readonly string rpt_informeAbono = rutaInstalacion + @"informes\plantillas\ABONO DE CLIENTE - SEPT 2020.rpt";
        private static readonly string rpt_informeAbonoBorrador = rutaInstalacion + @"informes\plantillas\ABONO DE CLIENTE(borrador) - SEPT 2020.rpt";

        #region Calls Crystal Reports with the template and the parameters needed ***************************************************************
        private static string InformeGenericoToPDF(SAPbobsCOM.Company oCompany, string plantillaRPT,
                                                    string _selectionFormula, List<Tuple<string, object>> listaParam,
                                                    string sufijoNombreFichero)
        {
            string _reportFileName = "";
            ReportDocument crystalReport = new ReportDocument();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            string _DBUserID;
            string _DBpwd;

            try
            {
                crystalReport.Load(plantillaRPT);

                //string lolo = crystalReport.RecordSelectionFormula;
                //crystalReport.RecordSelectionFormula = "{Comando.ClienteNew} > 'M'";
                if (!string.IsNullOrWhiteSpace(_selectionFormula))
                    crystalReport.RecordSelectionFormula = _selectionFormula;
                System.Diagnostics.Debug.WriteLine("****** Datos de BD");
                System.Diagnostics.Debug.WriteLine(oCompany.DbUserName);
                System.Diagnostics.Debug.WriteLine(oCompany.DbPassword);

                _DBUserID = ConfigurationManager.AppSettings.Get("DBUserID");
                _DBpwd = SAPDesglose.Decrypt(ConfigurationManager.AppSettings.Get("DBpwd"));
                crystalReport.SetDatabaseLogon(_DBUserID, _DBpwd, oCompany.Server, oCompany.CompanyDB);
                //crystalReport.SetDatabaseLogon("sa", "SAPB1Admin");

                crConnectionInfo.DatabaseName = oCompany.CompanyDB;
                crConnectionInfo.ServerName = oCompany.Server;
                crConnectionInfo.UserID = _DBUserID;
                crConnectionInfo.Password = _DBpwd;

                foreach (Table crTabla in crystalReport.Database.Tables)
                {
                    crTabla.LogOnInfo.ConnectionInfo = crConnectionInfo;
                    crTabla.ApplyLogOnInfo(crTabla.LogOnInfo);
                }

                //crystalReport.Refresh();
                crystalReport.VerifyDatabase();


                for (int i = 0; i < listaParam.Count; i++)
                {
                    crystalReport.SetParameterValue(listaParam[i].Item1, listaParam[i].Item2);
                }
                //crystalReport.SetParameterValue("Dockey@", 738);

                //crystalReport.ReportDefinition.Sections[4].SectionFormat.EnableSuppress = true;
                //crystalReport.ReportDefinition.Sections[8].SectionFormat.EnableSuppress = true;

                _reportFileName = DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + sufijoNombreFichero + ".pdf";
                crystalReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, carpetaInformes + _reportFileName);
                //crystalReport.ExportToDisk(ExportFormatType.Excel, carpetaInformes + _reportFileName + ".xls");

                crystalReport.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION InformeGenericoToPDF: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                _reportFileName = "Error: " + ex.Message;
            }

            return _reportFileName;
        }
        #endregion ******************************************************************************************************************************


        public static string InformeVentasToPDF (SAPbobsCOM.Company oCompany, int salesPerson, string _dateFrom, string _dateTo)
        {
            ErrorHandler.WriteInLog("* InformeVentasToPDF: sin formatear , Fechas(" + _dateFrom + "," + _dateTo + ")", ErrorHandler.ErrorVerboseLevel.Debug);
            ErrorHandler.WriteInLog("* InformeVentasToPDF: con formateo de fechas (" + System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha") + "), Fechas(" + DateTime.ParseExact(_dateFrom, WebApiConfig.AppDateFormat,
                                                                                  System.Globalization.CultureInfo.InvariantCulture).
                                                              ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")) + "," + DateTime.ParseExact(_dateTo, WebApiConfig.AppDateFormat,
                                                                                  System.Globalization.CultureInfo.InvariantCulture).
                                                              ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")) + ")", ErrorHandler.ErrorVerboseLevel.Debug);

            string _reportFileName = "";
            List<Tuple<string, object>> listaParam = new List<Tuple<string, object>>();

            listaParam.Add(new Tuple<string, object>("id_emp_com", salesPerson.ToString()));
            //listaParam.Add(new Tuple<string, object>("desde", DateTime.ParseExact(_dateFrom, WebApiConfig.AppDateFormat,
            //                                                                      System.Globalization.CultureInfo.InvariantCulture).
            //                                                  ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))));
            //listaParam.Add(new Tuple<string, object>("hasta", DateTime.ParseExact(_dateTo, WebApiConfig.AppDateFormat,
            //                                                                      System.Globalization.CultureInfo.InvariantCulture).
            //                                                  ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"))));
            //AMM 20191126 experimentando porque los filtros de fechas son raros raritos
            listaParam.Add(new Tuple<string, object>("desde", _dateFrom));
            listaParam.Add(new Tuple<string, object>("hasta", _dateTo));


            _reportFileName = InformeGenericoToPDF(oCompany, rpt_informeVentas, "", listaParam, "InformeVentas");

            return _reportFileName;
        }

        public static string InformeFacturaToPDF(SAPbobsCOM.Company oCompany, string docEntry)
        {
            string _reportFileName = "";
            List<Tuple<string, object>> listaParam = new List<Tuple<string, object>>();
            int tempDocEntry;

            if (int.TryParse(docEntry, out tempDocEntry))
            {
                listaParam.Add(new Tuple<string, object>("Dockey@", tempDocEntry));

                _reportFileName = InformeGenericoToPDF(oCompany, rpt_informeFactura, "", listaParam, "InformeFactura");
            }
            else
            {
                _reportFileName = "Error: el docEntry no es un valor numérico válido.";
            }

            return _reportFileName;
        }

        public static string InformeDevolucionToPDF(SAPbobsCOM.Company oCompany, string docEntry, bool isDraft)
        {
            string _reportFileName = "";
            List<Tuple<string, object>> listaParam = new List<Tuple<string, object>>();
            int tempDocEntry;

            if (int.TryParse(docEntry, out tempDocEntry))
            {
                listaParam.Add(new Tuple<string, object>("Dockey@", tempDocEntry));

                if (isDraft)
                    _reportFileName = InformeGenericoToPDF(oCompany, rpt_informeDevolucionBorrador, "", listaParam, "InformeDevolucionBorrador");
                else
                    _reportFileName = InformeGenericoToPDF(oCompany, rpt_informeDevolucion, "", listaParam, "InformeDevolucion");
            }
            else
            {
                _reportFileName = "Error: el docEntry no es un valor numérico válido.";
            }

            return _reportFileName;
        }


        public static string InformeAbonoToPDF(SAPbobsCOM.Company oCompany, string docEntry, bool isDraft)
        {
            string _reportFileName = "";
            List<Tuple<string, object>> listaParam = new List<Tuple<string, object>>();
            int tempDocEntry;

            if (int.TryParse(docEntry, out tempDocEntry))
            {
                listaParam.Add(new Tuple<string, object>("Dockey@", tempDocEntry));

                if (isDraft)
                    _reportFileName = InformeGenericoToPDF(oCompany, rpt_informeAbonoBorrador, "", listaParam, "InformeAbonoBorrador");
                else
                    _reportFileName = InformeGenericoToPDF(oCompany, rpt_informeAbono, "", listaParam, "InformeAbono");
            }
            else
            {
                _reportFileName = "Error: el docEntry no es un valor numérico válido.";
            }

            return _reportFileName;
        }
    }
}