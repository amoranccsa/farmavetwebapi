﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Services.SAP
{
    public class SAPSalesPerson
    {
        public static void GetSalesId(string token, string user, out int idEmpleadoVentas)
        {
            SAPbobsCOM.Company oCompany;
            CompanyConnection oCompanyConnection;
            SAPbobsCOM.Recordset _recordset;
            string query;

            oCompany = ConnectionsManager.GetCompanyByToken(token, false);
            if (oCompany != null)
            {
                if (!oCompany.Connected)
                {
                    oCompanyConnection = ConnectionsManager.GetCompanyConnectionByToken(token);
                    if (oCompanyConnection.Connect() != 0)
                    {
                        throw new Exception("Unable to connect with SAP. Please, try again.");
                    }
                }
                _recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                query = string.Format(SAPSalesPersonQuerys.GetSalesPerson_FromUserId_HANA_OK, user);

                _recordset.DoQuery(query);
                if (_recordset.RecordCount > 0)
                {
                    _recordset.MoveFirst();

                    if (_recordset.Fields.Item("salesPrson").Value == null)
                        idEmpleadoVentas = -1;
                    else
                        idEmpleadoVentas = _recordset.Fields.Item("salesPrson").Value;

                }
                else
                {
                    idEmpleadoVentas = -1;
                }
            }
            else
            {
                //soyJefeVentas = false;
                //idEmpleadoVentas = -1;
                throw new Exception("Unable to connect with SAP. Please, try again.");
            }
        }



        public static string GetSalesPersonName(SAPbobsCOM.Company oCompany, int idEmpleadoVentas)
        {
            SAPbobsCOM.Recordset _recordset;
            string query;
            string slpName = "";

            if ((oCompany != null) && oCompany.Connected)
            {
                _recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                query = string.Format(SAPSalesPersonQuerys.GetSalesPersonName_HANA_OK, idEmpleadoVentas);

                _recordset.DoQuery(query);
                if (_recordset.RecordCount > 0)
                {
                    _recordset.MoveFirst();
                    slpName = _recordset.Fields.Item("SlpName").Value;

                }
            }

            return slpName;
        }

        public static int GetUserEmployeeId(SAPbobsCOM.Company oCompany, int salesPerson)
        {
            SAPbobsCOM.Recordset recordSet;
            string sql;
            int uEmpId = -1;

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    sql = string.Format(SAPSalesPersonQuerys.GetEmployeeFromSalesPersonId_HANA_OK, salesPerson);
                    recordSet.DoQuery(sql);

                    if (!recordSet.EoF)
                    {
                        recordSet.MoveFirst();
                        uEmpId = recordSet.Fields.Item("empID").Value;
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION GetUserEmployeeId: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return uEmpId;
        }
    }
}