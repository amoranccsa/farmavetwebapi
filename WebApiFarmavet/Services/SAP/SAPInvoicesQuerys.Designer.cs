﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApiFarmavet.Services.SAP {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class SAPInvoicesQuerys {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SAPInvoicesQuerys() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WebApiFarmavet.Services.SAP.SAPInvoicesQuerys", typeof(SAPInvoicesQuerys).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a  AND &quot;CardCode&quot; = &apos;{0}&apos; .
        /// </summary>
        internal static string GetInvoices_CardCodeFilter_HANA_OK {
            get {
                return ResourceManager.GetString("GetInvoices_CardCodeFilter_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a  AND &quot;CardName&quot; LIKE &apos;%{0}%&apos; .
        /// </summary>
        internal static string GetInvoices_cardNameFilter_HANA_OK {
            get {
                return ResourceManager.GetString("GetInvoices_cardNameFilter_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a (&quot;DocDate&quot; &gt;= &apos;{0}&apos; AND &quot;DocDate&quot; &lt;= &apos;{1}&apos;).
        /// </summary>
        internal static string GetInvoices_DatesFilter_NO_HANA {
            get {
                return ResourceManager.GetString("GetInvoices_DatesFilter_NO_HANA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a  AND &quot;DocNum&quot; LIKE &apos;%{0}%&apos; .
        /// </summary>
        internal static string GetInvoices_docNumFilter_HANA_OK {
            get {
                return ResourceManager.GetString("GetInvoices_docNumFilter_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetInvoices_HANA_OK
        ///SELECT &quot;DocEntry&quot;, &quot;DocNum&quot;, &quot;CardCode&quot;, &quot;CardName&quot;, &quot;DocTotal&quot;, &quot;DocDate&quot;, &quot;DocDueDate&quot;,
        ///              &quot;DocStatus&quot;, &quot;PeyMethod&quot;, &quot;OPYM&quot;.&quot;Descript&quot;
        ///FROM &quot;OINV&quot;
        ///LEFT JOIN &quot;OPYM&quot; ON &quot;OINV&quot;.&quot;PeyMethod&quot; = &quot;OPYM&quot;.&quot;PayMethCod&quot;
        ///WHERE &quot;CANCELED&quot; = &apos;N&apos;.
        /// </summary>
        internal static string GetInvoices_HANA_OK {
            get {
                return ResourceManager.GetString("GetInvoices_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a  ORDER BY &quot;DocDate&quot; .
        /// </summary>
        internal static string GetInvoices_OrderFilter_HANA_OK {
            get {
                return ResourceManager.GetString("GetInvoices_OrderFilter_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a  AND &quot;OINV&quot;.&quot;PeyMethod&quot; = &apos;{0}&apos; .
        /// </summary>
        internal static string GetInvoices_paymentMethodFilter_HANA_OK {
            get {
                return ResourceManager.GetString("GetInvoices_paymentMethodFilter_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a  AND &quot;SlpCode&quot; = &apos;{0}&apos; .
        /// </summary>
        internal static string GetInvoices_SLPFilter_HANA_OK {
            get {
                return ResourceManager.GetString("GetInvoices_SLPFilter_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetInvoicesSumDocTotal_HANA_OK
        ///SELECT SUM(&quot;DocTotal&quot;) AS &quot;DocTotal&quot; FROM &quot;OINV&quot; WHERE &quot;CANCELED&quot; = &apos;N&apos;.
        /// </summary>
        internal static string GetInvoicesSumDocTotal_HANA_OK {
            get {
                return ResourceManager.GetString("GetInvoicesSumDocTotal_HANA_OK", resourceCulture);
            }
        }
    }
}
