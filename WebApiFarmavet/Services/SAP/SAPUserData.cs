﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Services.SAP
{
    public class SAPUserData
    {
        private static void NameToFirstLastName(string completeName, out string _firstName, out string _lastName)
        {
            string[] elementosNombre = completeName.Split(' ');
            switch (elementosNombre.Length)
            {
                case 1:
                    _firstName = elementosNombre[0];
                    _lastName = "";
                    break;
                case 2:
                    _firstName = elementosNombre[0];
                    _lastName = elementosNombre[1];
                    break;
                case 3: //This will depend on the country :S
                    _firstName = elementosNombre[0];
                    _lastName = elementosNombre[1] + " " + elementosNombre[2];
                    break;
                default:
                    _firstName = elementosNombre[0];
                    for (int i = 1; i < elementosNombre.Length - 2; i++)
                    {
                        _firstName = _firstName + " " + elementosNombre[i];
                    }
                    _lastName = elementosNombre[elementosNombre.Length - 2] + " " +
                                elementosNombre[elementosNombre.Length - 1];
                    break;
            }
        }

        public static int GetFirstLastUserName(string token, string _user, out string _firstName, out string _lastName)
        {
            SAPbobsCOM.Company oCompany;
            CompanyConnection oCompanyConnection;
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string userName;
            int _error = 0;

            _firstName = "";
            _lastName = "";

            //Obtenemos el oCompany de la conexión del usuario por su token
            oCompany = ConnectionsManager.GetCompanyByToken(token, false);
            if (oCompany != null)
            {
                //Comprobamos que el oCompany esté conectado (y si no, se conecta)
                if (!oCompany.Connected)
                {
                    oCompanyConnection = ConnectionsManager.GetCompanyConnectionByToken(token);
                    if (oCompanyConnection.Connect() != 0)
                    {
                        _error = -1;
                    }
                }

                //Con la conexión hecha, solicitamos la información
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                sql = string.Format("SELECT \"U_NAME\" FROM \"OUSR\" WHERE \"USER_CODE\" = '{0}'", _user);
                recordSet.DoQuery(sql);

                if (!recordSet.EoF)
                {
                    recordSet.MoveFirst();
                    userName = recordSet.Fields.Item("U_NAME").Value;

                    NameToFirstLastName(userName, out _firstName, out _lastName);
                }
                else
                {
                    _error = -2;
                }
            }

            return _error;
        }



        public static int GetPricesLists(SAPbobsCOM.Company oCompany, string _userCode, out string _whiteList, out string _blackList)
        {
            SAPbobsCOM.Recordset recordSet;
            string sql;
            int _error = 0;

            _whiteList = "";
            _blackList = "";

            if ((oCompany != null) && (oCompany.Connected) && !string.IsNullOrWhiteSpace(_userCode))
            {
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);


                sql = string.Format(SAPUserDataQuerys.GetPricesLists_HANA_OK, _userCode);
                recordSet.DoQuery(sql);

                if (!recordSet.EoF)
                {
                    recordSet.MoveFirst();
                    _whiteList = recordSet.Fields.Item("U_DOI_ListaBlanca").Value;
                    _blackList = recordSet.Fields.Item("U_DOI_ListaNegra").Value;
                }
                else
                {
                    _error = -1;
                }
            }

            return _error;
        }

    }
}