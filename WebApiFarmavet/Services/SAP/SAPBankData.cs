﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPBankData
    {
        private static CRMBankCodeModel GetBankCodeModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            CRMBankCodeModel _bCode = new CRMBankCodeModel();

            _bCode.Id = _recordSet.Fields.Item("BankCode").Value.ToString();
            _bCode.BankName = _recordSet.Fields.Item("AcctName").Value;
            _bCode.GLAccount = _recordSet.Fields.Item("GLAccount").Value;

            return _bCode;
        }

        private static CRMBankAccountModel GetBankAccountModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            CRMBankAccountModel _bAccount = new CRMBankAccountModel();

            _bAccount.Id = _recordSet.Fields.Item("BankCode").Value.ToString();
            _bAccount.BankName = _recordSet.Fields.Item("AcctName").Value;
            _bAccount.GLAccount = _recordSet.Fields.Item("GLAccount").Value;
            _bAccount.Branch = _recordSet.Fields.Item("Branch").Value;
            _bAccount.ControlKey = _recordSet.Fields.Item("ControlKey").Value;
            _bAccount.Account = _recordSet.Fields.Item("Account").Value;
            _bAccount.IBAN = _recordSet.Fields.Item("IBAN").Value;

            return _bAccount;
        }




        public static List<CRMBankCodeModel> GetBankCodesList(SAPbobsCOM.Company oCompany)
        {
            List<CRMBankCodeModel> _bankCodesList = null;
            SAPbobsCOM.Recordset recordSet;
            CRMBankCodeModel tempBCode;

            string sql = "";

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    sql = "SELECT \"BankCode\", \"AcctName\", \"GLAccount\" FROM \"DSC1\" ORDER BY \"AcctName\"";

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        _bankCodesList = new List<CRMBankCodeModel>();
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempBCode = GetBankCodeModelFromRecordset(oCompany, recordSet);
                            _bankCodesList.Add(tempBCode);
                            recordSet.MoveNext();
                        }
                    }
                    else
                    {
                        _bankCodesList = null;
                        ErrorHandler.WriteInLog("No se ha encontrado ningún código de banco.", ErrorHandler.ErrorVerboseLevel.None);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }


            return _bankCodesList;
        }

        public static CRMBankCodeModel GetBankCodeData(SAPbobsCOM.Company oCompany, string _GLAccount)
        {
            SAPbobsCOM.Recordset recordSet;
            CRMBankCodeModel bankCodeData = null;

            string sql = "";

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    sql = string.Format("SELECT \"BankCode\", \"AcctName\", \"GLAccount\" FROM \"DSC1\" WHERE \"GLAccount\" = '{0}'", _GLAccount);

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        if (!recordSet.EoF)
                        {
                            bankCodeData = GetBankCodeModelFromRecordset(oCompany, recordSet);
                        }
                    }
                    else
                    {
                        bankCodeData = null;
                        ErrorHandler.WriteInLog("No se ha encontrado ningún código de banco.", ErrorHandler.ErrorVerboseLevel.None);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }


            return bankCodeData;
        }

        public static CRMBankAccountModel GetBankAccountData(SAPbobsCOM.Company oCompany, string _GLAccount)
        {
            SAPbobsCOM.Recordset recordSet;
            CRMBankAccountModel bankAccountData = null;

            string sql = "";

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    sql = string.Format("SELECT \"BankCode\", \"AcctName\", \"GLAccount\", \"Branch\", \"ControlKey\", \"Account\", \"IBAN\" " +
                                        "FROM \"DSC1\" WHERE \"GLAccount\" = '{0}'", _GLAccount);

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        if (!recordSet.EoF)
                        {
                            bankAccountData = GetBankAccountModelFromRecordset(oCompany, recordSet);
                        }
                    }
                    else
                    {
                        bankAccountData = null;
                        ErrorHandler.WriteInLog("No se ha encontrado ningún código de banco.", ErrorHandler.ErrorVerboseLevel.None);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }


            return bankAccountData;
        }
    }
}