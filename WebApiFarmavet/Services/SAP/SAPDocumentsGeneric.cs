﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPDocumentsGeneric
    {
        // Devuelve el nombre del documento
        public static string GetDocumentName(int docType) //SAPbobsCOM.BoObjectTypes docType)
        {
            string var;


            switch (docType)
            {
                case (int)SAPbobsCOM.BoObjectTypes.oOrders:
                    var = "Pedidos de Clientes";
                    break;
                case (int)SAPbobsCOM.BoObjectTypes.oInvoices:
                    var = "Facturas de Clientes";
                    break;
                case (int)SAPbobsCOM.BoObjectTypes.oItems:
                    var = "Artículos";
                    break;
                case (int)SAPbobsCOM.BoObjectTypes.oDrafts:
                    var = "Borrador de documento";
                    break;
                default:
                    var = "Documento";
                    break;
            }

            return var;
        }

        /// <summary>
        /// Returns the name of the table of the lines for a document type
        /// </summary>
        /// <param name="docType"></param>
        /// <returns></returns>
        private static string GetLineTable(SAPbobsCOM.BoObjectTypes docType)
        {
            string table;

            switch (docType)
            {
                case SAPbobsCOM.BoObjectTypes.oOrders:
                    table = "RDR1";
                    break;
                case SAPbobsCOM.BoObjectTypes.oInvoices:
                    table = "INV1";
                    break;
                case SAPbobsCOM.BoObjectTypes.oItems:
                    table = "ITM1";
                    break;
                case SAPbobsCOM.BoObjectTypes.oDrafts:
                    table = "DRF1";
                    break;
                default:
                    table = "";
                    break;
            }

            return table;
        }

        /// <summary>
        /// Look in the database if a lineNum exists for a DocEntry in any document
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="docType"></param>
        /// <param name="_docEntry"></param>
        /// <param name="_line"></param>
        /// <returns></returns>
        public static bool LineExists(SAPbobsCOM.Company oCompany, SAPbobsCOM.BoObjectTypes docType, int _docEntry, int _line)
        {
            string table;
            string sql;
            SAPbobsCOM.Recordset recordset;
            bool found = false;

            table = GetLineTable(docType);
            if (!string.IsNullOrWhiteSpace(table))
            {

                recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                sql = string.Format("SELECT \"LineNum\" FROM \"" + table + "\" WHERE \"DocEntry\" = '{0}' AND \"LineNum\" = '{1}'",
                                    _docEntry.ToString(), _line.ToString());

                recordset.DoQuery(sql);
                if (recordset.RecordCount > 0)
                {
                    found = true;
                }
            }


            return found;
        }


        #region SET DOCUMENT LINE ************************************************************************************

        public static bool SetDocumentLine(SAPbobsCOM.Documents _document, int _line)
        {
            int i = 0;
            bool found = false;

            while (!found && (i < _document.Lines.Count))
            {
                _document.Lines.SetCurrentLine(i);
                if (_document.Lines.LineNum == _line)
                    found = true;
                i++;
            }

            return found;
        }


        public static bool SetDocumentLine(SAPbobsCOM.Documents _document, string _sLine)
        {
            int _line;
            bool found = false;

            //Previous checks
            if (!string.IsNullOrWhiteSpace(_sLine))
            {
                if (Int32.TryParse(_sLine, out _line))
                {
                    found = SetDocumentLine(_document, _line);
                }
            }

            return found;

        }

        #endregion ***************************************************************************************************
    }
}