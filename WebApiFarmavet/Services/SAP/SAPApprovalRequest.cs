﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPApprovalRequest
    {
        #region Models constructors from recordset *************************************************************************
        private static CRMApprovalRequestModel GetModelFromRecordset(SAPbobsCOM.Recordset recordset)
        {
            CRMApprovalRequestModel tempModel = new CRMApprovalRequestModel();

            tempModel.Id = recordset.Fields.Item("DocEntry").Value.ToString();
            tempModel.docNum = recordset.Fields.Item("DocNum").Value.ToString();
            tempModel.cardCode = recordset.Fields.Item("CardCode").Value.ToString();
            tempModel.cardName = recordset.Fields.Item("CardName").Value.ToString();
            tempModel.docDueDate = recordset.Fields.Item("DocDueDate").Value;
            tempModel.docTotal = recordset.Fields.Item("DocTotal").Value;
            tempModel.appReqEntry = recordset.Fields.Item("WddCode").Value.ToString();
            
            switch (recordset.Fields.Item("Status").Value.ToString())
            {
                case "W": tempModel.status = CRMApprovalRequestModel.ApprovalRequestStatusEnum.Pending; break;
                case "Y": tempModel.status = CRMApprovalRequestModel.ApprovalRequestStatusEnum.Approved; break;
                case "N": tempModel.status = CRMApprovalRequestModel.ApprovalRequestStatusEnum.NotApproved; break;
                case "P": tempModel.status = CRMApprovalRequestModel.ApprovalRequestStatusEnum.Generated; break;
                case "A": tempModel.status = CRMApprovalRequestModel.ApprovalRequestStatusEnum.GeneratedByAuthorizer; break;
                case "C": tempModel.status = CRMApprovalRequestModel.ApprovalRequestStatusEnum.Cancelled; break;
                default: tempModel.status = CRMApprovalRequestModel.ApprovalRequestStatusEnum.None; break;
            }
            switch (recordset.Fields.Item("IsDraft").Value.ToString())
            {
                case "N": tempModel.draft = false; break;
                default: tempModel.draft = true; break;
            }
            tempModel.comments = recordset.Fields.Item("Comments").Value;

            return tempModel;
        }
        #endregion ********************************************************************************************************

        #region Get approval requests list ********************************************************************************
        public static List<CRMApprovalRequestModel> GetApprovalRequestList(SAPbobsCOM.Company oCompany, int salesPerson)
        {
            List<CRMApprovalRequestModel> aReqList = new List<CRMApprovalRequestModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string orderFilter = "";
            CRMApprovalRequestModel tempAReq;

            ErrorHandler.WriteInLog("Getting Approval Request list: SalesPerson(" + salesPerson.ToString() + ")", ErrorHandler.ErrorVerboseLevel.Trace);

            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPApprovalRequestsQuerys.GetRequests_SlpFilter_HANA_OK, salesPerson.ToString());
                    orderFilter = SAPApprovalRequestsQuerys.GetRequests_OrderFilter_HANA_OK;

                    sql = SAPApprovalRequestsQuerys.GetApprovalRequestedOrders_NO_HANA + salesPersonFilter + orderFilter;

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempAReq = GetModelFromRecordset(recordset);
                            aReqList.Add(tempAReq);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    aReqList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetApprovalRequestList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return aReqList;
        }
        #endregion *********************************************************************************************************

        #region Get approval requests list ********************************************************************************
        public static List<CRMApprovalRequestModel> GetApprovedRequestList(SAPbobsCOM.Company oCompany, int salesPerson)
        {
            List<CRMApprovalRequestModel> aReqList = new List<CRMApprovalRequestModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string orderFilter = "";
            CRMApprovalRequestModel tempAReq;

            ErrorHandler.WriteInLog("Getting Approved Request list: SalesPerson(" + salesPerson.ToString() + ")", ErrorHandler.ErrorVerboseLevel.Trace);

            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPApprovalRequestsQuerys.GetRequests_SlpFilter_HANA_OK, salesPerson.ToString());
                    orderFilter = SAPApprovalRequestsQuerys.GetRequests_OrderFilter_HANA_OK;

                    sql = string.Format(SAPApprovalRequestsQuerys.GetApprovedRequestsOrders_HANA_OK + orderFilter, salesPersonFilter);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempAReq = GetModelFromRecordset(recordset);
                            aReqList.Add(tempAReq);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    aReqList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetApprovalRequestList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return aReqList;
        }
        #endregion *********************************************************************************************************


        /*public static List<CRMApprovalRequestModel> GetAprrovalRequestsList(Company oCompany, string userCode, int statuspend)
        {
            DateTime date;
            string query;
            List<CRMApprovalRequestModel> arList = null;
            SAPbobsCOM.Recordset ors;

            if ((oCompany != null) && oCompany.Connected && !string.IsNullOrWhiteSpace(userCode))
            {
                try
                {
                    arList = new List<CRMApprovalRequestModel>();
                    ors = (Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                    //Pruebas userCode - *
                    //userCode = "%";
                    if (statuspend == (int)ApprovalRequestStatus.arsPending)
                        query = String.Format(SAPApprovalRequestsQuerys.GetApprovalRequestsByUser, userCode, "");
                    else
                        query = String.Format(SAPApprovalRequestsQuerys.GetApprovalRequestsByUser, userCode, "Not ");

                    ors.DoQuery(query);

                    while (!ors.EoF)
                    {
                        CRMApprovalRequestModel oAR = new CRMApprovalRequestModel()
                        {
                            Code = (int)ors.Fields.Item("WddCode").Value,
                            ObjectEntry = (int)ors.Fields.Item("DocEntry").Value,
                            ObjectType = Convert.ToInt16(ors.Fields.Item("ObjType").Value),
                            OwnerID = (int)ors.Fields.Item("OwnerID").Value,
                            OwnerCode = ors.Fields.Item("OwnerCode").Value,
                            Remarks = ors.Fields.Item("Remarks").Value
                        };


                        date = ors.Fields.Item("CreateDate").Value;
                        var hour = ors.Fields.Item("CreateTime").Value;
                        oAR.CreationDate = SAPDate.GetTimeFromSAP(date, hour);


                        var status = ors.Fields.Item("Status").Value;
                        switch (status)
                        {

                            case "W": oAR.Status = ApprovalRequestStatus.arsPending; break;
                            case "Y": oAR.Status = ApprovalRequestStatus.arsApproved; break;
                            case "N": oAR.Status = ApprovalRequestStatus.arsNotApproved; break;
                            case "P": oAR.Status = ApprovalRequestStatus.arsGenerated; break;
                            case "A":
                                oAR.Status = ApprovalRequestStatus.arsParsGeneratedByAuthorizerending;
                                break;
                            case "C":
                                oAR.Status = ApprovalRequestStatus.arsCancelled;
                                break;
                            default:
                                oAR.Status = ApprovalRequestStatus.arsPending;
                                break;
                        }
                        var done = ors.Fields.Item("IsDraft").Value;
                        if (done == "Y")
                            oAR.IsDraft = true;
                        else
                            oAR.IsDraft = false;

                        oAR.UserID = (int)ors.Fields.Item("UserId").Value;
                        oAR.UserCode = ors.Fields.Item("UserCode").Value;

                        date = ors.Fields.Item("RequestDate").Value;
                        hour = ors.Fields.Item("RequestTime").Value;
                        oAR.RequestDate = SAPDate.GetTimeFromSAP(date, hour);

                        status = ors.Fields.Item("StatusRespond").Value;
                        switch (status)
                        {

                            case "W": oAR.StatusRespond = ApprovalRequestStatus.arsPending; break;
                            case "Y": oAR.StatusRespond = ApprovalRequestStatus.arsApproved; break;
                            case "N": oAR.StatusRespond = ApprovalRequestStatus.arsNotApproved; break;
                            default:
                                oAR.StatusRespond = ApprovalRequestStatus.arsPending;
                                break;
                        }
                        oAR.RemarksRespond = ors.Fields.Item("RemarksRespond").Value;


                        oAR.StepCode = (int)ors.Fields.Item("StepCode").Value;
                        oAR.StepName = ors.Fields.Item("StepName").Value;
                        oAR.StepRemarks = ors.Fields.Item("StepRemarks").Value;


                        // Datos del documento
                        // Ponemos esta forma ya que se podría ampliar a mas tipos
                        switch (oAR.ObjectType)
                        {
                            case 112:
                                SAPbobsCOM.Documents oDraft = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                                oDraft.GetByKey(oAR.ObjectEntry);
                                oAR.DocNum = oDraft.DocNum;
                                oAR.DocTotal = oDraft.DocTotal;
                                oAR.DocName = oDraft.DocObjectCodeEx;
                                oAR.DocName = SAPDocumentsGeneric.GetDocumentName((int)oDraft.DocObjectCode);
                                oAR.DocDate = oDraft.DocDueDate;
                                oAR.DocCardCode = oDraft.CardCode;
                                oAR.DocCardName = oDraft.CardName;
                                break;
                            default:
                                var odoc = oCompany.GetBusinessObject((BoObjectTypes)oAR.ObjectType);
                                odoc.GetByKey(oAR.ObjectEntry);
                                oAR.DocNum = odoc.DocNum;
                                oAR.DocTotal = odoc.DocTotal;
                                oAR.DocName = odoc.DocObjectCodeEx;
                                oAR.DocName = SAPDocumentsGeneric.GetDocumentName((int)odoc.DocObjectCode);
                                oAR.DocDate = odoc.DocDueDate;
                                oAR.DocCardCode = odoc.CardCode;
                                oAR.DocCardName = odoc.CardName;

                                break;
                        }


                        //oDraft.GetByKey(oApprovalRequest.ObjectEntry)


                        //'Get actual document type. 
                        //txtRealDocType.Text = oDraft.DocObjectCode
                        //    End If
                        //                    GetDocumentName

                        //                    Enum.GetName(BoObjectTypes, BoObjectTypes.oActivityLocations);



                        arList.Add(oAR);

                        ors.MoveNext();

                    }

                }
                catch (Exception ex)
                {
                    arList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            return arList;

        }


        public static SAPErrorType UpdateAprrovalRequest(Company oCompany, CRMApprovalRequestModel oAR)
        {
            SAPErrorType result = SAPErrorType.error;

            if ((oCompany != null) && oCompany.Connected && (oAR != null))
            {
                try
                {
                    SAPbobsCOM.CompanyService oCompanyService = oCompany.GetCompanyService();
                    SAPbobsCOM.ApprovalRequestsService approvalSrv = oCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.ApprovalRequestsService);
                    SAPbobsCOM.ApprovalRequestParams oParams = approvalSrv.GetDataInterface(SAPbobsCOM.ApprovalRequestsServiceDataInterfaces.arsApprovalRequestParams);

                    oParams.Code = oAR.Code;
                    SAPbobsCOM.ApprovalRequest oData = approvalSrv.GetApprovalRequest(oParams);
                    if (oData != null)
                    {
                        //Add an approval decision 
                        oData.ApprovalRequestDecisions.Add();
                        oData.ApprovalRequestDecisions.Item(0).ApproverUserName = oAR.UserCode;
                        oData.ApprovalRequestDecisions.Item(0).ApproverPassword = oAR.UserPass;
                        oData.ApprovalRequestDecisions.Item(0).Status = (SAPbobsCOM.BoApprovalRequestDecisionEnum)oAR.StatusRespond;
                        oData.ApprovalRequestDecisions.Item(0).Remarks = oAR.RemarksRespond;

                        //Update the approval request 
                        approvalSrv.UpdateRequest(oData);
                        result = SAPErrorType.ok;
                    }

                }
                catch (Exception ex)
                {

                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return result;
        }*/
    }
}