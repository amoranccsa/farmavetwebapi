﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPWarehouses
    {
        #region Models constructors from recordsets ********************************************************************
        private static CRMWhsDistanceStockModel GetDistanceModelFromRecordset(SAPbobsCOM.Recordset recordset)
        {
            CRMWhsDistanceStockModel tempModel = new CRMWhsDistanceStockModel();

            tempModel.Id = recordset.Fields.Item("WhsCode").Value.ToString();
            tempModel.Name = recordset.Fields.Item("WhsName").Value.ToString();
            tempModel.codState = recordset.Fields.Item("CodProvincia").Value.ToString();
            tempModel.nameState = recordset.Fields.Item("NombreProvincia").Value;
            tempModel.distanceOrder = recordset.Fields.Item("Prioridad").Value;
            tempModel.stock = recordset.Fields.Item("OnHand").Value;
            tempModel.avaiable = recordset.Fields.Item("Disponible").Value;

            return tempModel;
        }
        #endregion *****************************************************************************************************


        public static List<CRMWhsDistanceStockModel> GetWhsDistanceStock (SAPbobsCOM.Company oCompany, string itemCode, string state)
        {
            List<CRMWhsDistanceStockModel> whsDistancesList = new List<CRMWhsDistanceStockModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            CRMWhsDistanceStockModel tempData;

            ErrorHandler.WriteInLog("Getting warehouses distances list: ItemCode(" + itemCode + "); State(" + state + ")",
                                    ErrorHandler.ErrorVerboseLevel.Trace);
            
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by
                    sql = string.Format(SAPWarehousesQuerys.Warehouse_GetDistanceStock_NO_HANA, itemCode, state);
                    ErrorHandler.WriteInLog("SAPWarehousesQuerys.Warehouse_GetDistanceStock " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191115 

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempData = GetDistanceModelFromRecordset(recordset);
                            whsDistancesList.Add(tempData);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    whsDistancesList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetWhsDistanceStock: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return whsDistancesList;
        }


        public static CRMBaseModel GetDefaultWarehouse (SAPbobsCOM.Company oCompany, string userCode)
        {
            CRMBaseModel whsData = new CRMBaseModel();
            SAPbobsCOM.Recordset recordset;
            string sql;
            
            ErrorHandler.WriteInLog("Getting default warehouse for userCode '" + userCode + "'",
                                    ErrorHandler.ErrorVerboseLevel.Trace);

            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by
                    sql = string.Format(SAPWarehousesQuerys.Warehouse_GetDefaultWarehouse_HANA_OK, userCode);
                    ErrorHandler.WriteInLog("SAPWarehousesQuerys.Warehouse_GetDefaultWarehouse " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191115 

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (!recordset.EoF)
                    {
                        recordset.MoveFirst();
                        if (recordset.RecordCount > 0)
                        {
                            whsData.Id = recordset.Fields.Item("Warehouse").Value.ToString();
                            whsData.Name = recordset.Fields.Item("WhsName").Value.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    whsData = null;
                    ErrorHandler.WriteInLog("EXCEPTION GetDefaultWarehouse: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return whsData;
        }
    }
}