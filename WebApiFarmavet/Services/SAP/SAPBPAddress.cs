﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPBPAddress
    {
        public enum AddressType { Bill, Shipping, Both };

        #region INFO CONVERTERS **********************************************************************************
        private static CRMBPAddressModel GetModelFromRecordset(SAPbobsCOM.Recordset _recordSet)
        {
            CRMBPAddressModel _address = new CRMBPAddressModel();

            _address.Id = _recordSet.Fields.Item("Address").Value;
            _address.Street = _recordSet.Fields.Item("Street").Value;
            _address.Building = _recordSet.Fields.Item("Building").Value;   //Adds build. nº, floor, room...
            _address.City = _recordSet.Fields.Item("City").Value;
            _address.ZipCode = _recordSet.Fields.Item("ZipCode").Value;
            _address.StateName = _recordSet.Fields.Item("StateName").Value;   //This is a string from OCST table
            _address.CountryName = _recordSet.Fields.Item("CountryName").Value; //This is a string from OCRY table

            switch (_recordSet.Fields.Item("AdresType").Value)
            {
                case "B":
                    _address.Type = CRMBPAddressModel.AddressType.Bill;
                    break;
                case "S":
                    _address.Type = CRMBPAddressModel.AddressType.Shipping;
                    break;
            }

            _address.LicTradNum = _recordSet.Fields.Item("LicTradNum").Value.ToString();

            return _address;
        }
        #endregion ***********************************************************************************************

        #region GETTING DATA ****************************************************************************************
        public static List<CRMBPAddressModel> GetAddressList(SAPbobsCOM.Company oCompany, string cardCode, AddressType addressType)
        {
            List<CRMBPAddressModel> addressList = new List<CRMBPAddressModel>();
            SAPbobsCOM.Recordset recordSet;
            CRMBPAddressModel tempAddress;

            string sql = "";
            string cardCodeFilter;
            string addressTypeFilter;

            if ((oCompany != null) && oCompany.Connected && !string.IsNullOrWhiteSpace(cardCode))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    //Initializing filters
                    cardCodeFilter = string.Format("WHERE \"CardCode\" = '{0}' ", cardCode);
                    switch (addressType)
                    {
                        case AddressType.Bill:
                            addressTypeFilter = " AND \"AdresType\" = 'B' ";
                            break;
                        case AddressType.Shipping:
                            addressTypeFilter = " AND \"AdresType\" = 'S' ";
                            break;
                        default:
                            addressTypeFilter = "";
                            break;
                    }

                    sql = "SELECT \"CRD1\".\"Address\", \"CRD1\".\"Street\", \"CRD1\".\"Building\", \"CRD1\".\"City\", " +
                          "       \"CRD1\".\"ZipCode\", \"CRD1\".\"AdresType\", " +
                          "       \"OCST\".\"Name\" AS \"StateName\", \"OCRY\".\"Name\" AS \"CountryName\", " +
                          "       \"CRD1\".\"LicTradNum\" " +
                          "FROM \"CRD1\" " +
                          "LEFT JOIN \"OCST\" ON \"CRD1\".\"State\" = \"OCST\".\"Code\" " +
                          "LEFT JOIN \"OCRY\" ON \"CRD1\".\"Country\" = \"OCRY\".\"Code\" " +
                          cardCodeFilter + addressTypeFilter;

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempAddress = GetModelFromRecordset(recordSet);
                            addressList.Add(tempAddress);
                            recordSet.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    addressList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            return addressList;
        }


        public static CRMBPAddressModel GetAddressData(SAPbobsCOM.Company oCompany, string cardCode, AddressType addressType, string addressID)
        {
            SAPbobsCOM.Recordset recordSet;
            CRMBPAddressModel tempAddress = null;

            string sql = "";
            string addressIDFilter;
            string cardCodeFilter;
            string addressTypeFilter;

            if ((oCompany != null) && oCompany.Connected && !string.IsNullOrWhiteSpace(cardCode) && !string.IsNullOrWhiteSpace(addressID))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    //Initializing filters
                    addressIDFilter = string.Format(" WHERE \"Address\" = '{0}' ", addressID);
                    cardCodeFilter = string.Format(" AND \"CardCode\" = '{0}' ", cardCode);
                    switch (addressType)
                    {
                        case AddressType.Bill:
                            addressTypeFilter = " AND \"AdresType\" = 'B' ";
                            break;
                        case AddressType.Shipping:
                            addressTypeFilter = " AND \"AdresType\" = 'S' ";
                            break;
                        default:
                            addressTypeFilter = "";
                            break;
                    }

                    sql = "SELECT \"CRD1\".\"Address\", \"CRD1\".\"Street\", \"CRD1\".\"Building\", \"CRD1\".\"City\", " +
                          "       \"CRD1\".\"ZipCode\", \"CRD1\".\"AdresType\", " +
                          "       \"OCST\".\"Name\" AS \"StateName\", \"OCRY\".\"Name\" AS \"CountryName\", " +
                          "       \"CRD1\".\"LicTradNum\" " +
                          "FROM \"CRD1\" " +
                          "LEFT JOIN \"OCST\" ON \"CRD1\".\"State\" = \"OCST\".\"Code\" " +
                          "LEFT JOIN \"OCRY\" ON \"CRD1\".\"Country\" = \"OCRY\".\"Code\" " +
                          addressIDFilter + cardCodeFilter + addressTypeFilter;

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        if (!recordSet.EoF)
                        {
                            tempAddress = GetModelFromRecordset(recordSet);
                        }
                    }
                }
                catch (Exception ex)
                {
                    tempAddress = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            return tempAddress;
        }
        #endregion *************************************************************************************************

        #region ADDING DATA ****************************************************************************************
        public static string AddAddressList (SAPbobsCOM.Company oCompany, CRMBPAddressListModel list)
        {
            SAPbobsCOM.BusinessPartners accountSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            string _error = "";
            int iError;

            string tempError = "";

            try
            {
                //Checking if CardCode is not empty
                if (string.IsNullOrWhiteSpace(list.Id))
                    _error = string.Format("-e001|CardCode no válido ({0})", list.Id);

                //Checking if list is not empty
                if (string.IsNullOrWhiteSpace(_error) && ((list.List == null) || (list.List.Count == 0)))
                    _error = "-e005|La lista de direcciones está vacía";

                //Getting the business partner by its CardCode
                if (string.IsNullOrWhiteSpace(_error))
                {
                    if (!accountSAP.GetByKey(list.Id))
                        _error = string.Format("-e002|CardCode no encontrado en SAP ({0})", list.Id);
                    else
                    {
                        
                        //Adding non existent contacts
                        if (!string.IsNullOrWhiteSpace(accountSAP.Addresses.AddressName))
                            accountSAP.Addresses.Add();

                        for (int i = 0; i < list.List.Count; i++)
                        {
                            if (!AddressExists(oCompany, list.Id, list.List[i].Id) && !list.List[i].Deleted)
                            {
                                accountSAP.Addresses.AddressName = list.List[i].Id;
                                if (!string.IsNullOrWhiteSpace(list.List[i].Street))
                                    accountSAP.Addresses.Street = list.List[i].Street;
                                if (!string.IsNullOrWhiteSpace(list.List[i].Building))
                                    accountSAP.Addresses.BuildingFloorRoom = list.List[i].Building;
                                if (!string.IsNullOrWhiteSpace(list.List[i].City))
                                    accountSAP.Addresses.City = list.List[i].City;
                                if (!string.IsNullOrWhiteSpace(list.List[i].ZipCode))
                                    accountSAP.Addresses.ZipCode = list.List[i].ZipCode;
                                if (!string.IsNullOrWhiteSpace(list.List[i].StateName))
                                    accountSAP.Addresses.State = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCST", "Code", "\"Name\" = '" + list.List[i].StateName + "'");
                                if (!string.IsNullOrWhiteSpace(list.List[i].CountryName))
                                    accountSAP.Addresses.Country = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRY", "Code", "\"Name\" = '" + list.List[i].CountryName + "'");
                                switch (list.List[i].Type)
                                {
                                    case CRMBPAddressModel.AddressType.Bill:
                                        accountSAP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_BillTo;
                                        break;
                                    case CRMBPAddressModel.AddressType.Shipping:
                                        accountSAP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
                                        break;
                                }

                                if (!string.IsNullOrWhiteSpace(list.List[i].LicTradNum))
                                    accountSAP.Addresses.FederalTaxID = list.List[i].LicTradNum;

                                accountSAP.Addresses.Add();
                            }
                            else
                            {
                                if (!list.List[i].Deleted)
                                    tempError += string.Format("-e006|La dirección '{0}' ya existe.", list.List[i].Id) + Environment.NewLine;
                            }
                        }

                        //Setting the new Main Contact
                        //accountSAP.ContactPerson = list.CntctPrsnId;
                        accountSAP.Update();

                        oCompany.GetLastError(out iError, out _error);
                        if (iError != 0)
                        {
                            _error = iError.ToString() + ": " + _error;
                        }

                        if (!string.IsNullOrWhiteSpace(_error))
                            _error += Environment.NewLine + tempError;
                        else
                            _error = tempError;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Address list update error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }
        #endregion *************************************************************************************************

        #region UPDATING DATA **************************************************************************************

        public static string UpdateAddressList (SAPbobsCOM.Company oCompany, CRMBPAddressListModel list)
        {
            SAPbobsCOM.BusinessPartners accountSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            string _error = "";
            int iError;
            int addressesCount;
            List<CRMBPAddressModel> shippingAddressList = new List<CRMBPAddressModel>();
            List<CRMBPAddressModel> billAddressList = new List<CRMBPAddressModel>();
            CRMBPAddressModel tempAddress = null;

            try
            {
                //Checking if CardCode is not empty
                if (string.IsNullOrWhiteSpace(list.Id))
                    _error = string.Format("-e001|CardCode no válido ({0})", list.Id);

                //Checking if list is not empty
                if (string.IsNullOrWhiteSpace(_error) && ((list.List == null) || (list.List.Count == 0)))
                    _error = "-e005|La lista de direcciones está vacía";

                //Getting the business partner by its CardCode
                if (string.IsNullOrWhiteSpace(_error))
                {
                    if (!accountSAP.GetByKey(list.Id))
                        _error = string.Format("-e002|CardCode no encontrado en SAP ({0})", list.Id);
                    else
                    {
                        /*System.Diagnostics.Debug.WriteLine("*** Number: " + accountSAP.ContactEmployees.Count);
                        System.Diagnostics.Debug.WriteLine("*** Contact: " + accountSAP.ContactEmployees.Name);*/

                        //Splitting data in two lists
                        shippingAddressList = list.List.FindAll(x => x.Type == CRMBPAddressModel.AddressType.Shipping);
                        billAddressList = list.List.FindAll(x => x.Type == CRMBPAddressModel.AddressType.Bill);

                        //Updating existing addresses
                        addressesCount = accountSAP.Addresses.Count;
                        if ((addressesCount > 0) && (!string.IsNullOrWhiteSpace(accountSAP.Addresses.AddressName)))
                        {
                            for (int i = 0; i < accountSAP.Addresses.Count; i++)
                            {
                                accountSAP.Addresses.SetCurrentLine(i);
                                if (!string.IsNullOrWhiteSpace(accountSAP.Addresses.AddressName))
                                {
                                    //Look for the address in the edited addresses list
                                    switch (accountSAP.Addresses.AddressType)
                                    {
                                        case SAPbobsCOM.BoAddressType.bo_BillTo:
                                            tempAddress = billAddressList.Find(x => x.Id == accountSAP.Addresses.AddressName);
                                            break;
                                        case SAPbobsCOM.BoAddressType.bo_ShipTo:
                                            tempAddress = shippingAddressList.Find(x => x.Id == accountSAP.Addresses.AddressName);
                                            break;
                                    }

                                    if (tempAddress != null)
                                    {
                                        //Check if contact person was marked to delete
                                        if (tempAddress.Deleted)
                                        {
                                            //If the main address was marked to delete (because other was chosen for that position), it must removed before deleting
                                            switch (accountSAP.Addresses.AddressType)
                                            {
                                                case SAPbobsCOM.BoAddressType.bo_BillTo:
                                                    System.Diagnostics.Debug.WriteLine("**** " + accountSAP.BilltoDefault);
                                                    if (accountSAP.BilltoDefault == tempAddress.Id)
                                                        accountSAP.BilltoDefault = "";
                                                    break;
                                                case SAPbobsCOM.BoAddressType.bo_ShipTo:
                                                    System.Diagnostics.Debug.WriteLine("**** " + accountSAP.ShipToDefault);
                                                    if (accountSAP.ShipToDefault == tempAddress.Id)
                                                        accountSAP.ShipToDefault = "";
                                                    break;
                                            }
                                            accountSAP.Addresses.Delete();
                                            i--;
                                        }
                                        else
                                        {
                                            //Check if address was edited
                                            if (tempAddress.UpdatedAt != null)
                                            {
                                                if (!string.IsNullOrWhiteSpace(tempAddress.Street))
                                                    accountSAP.Addresses.Street = tempAddress.Street;
                                                if (!string.IsNullOrWhiteSpace(tempAddress.Building))
                                                    accountSAP.Addresses.BuildingFloorRoom = tempAddress.Building;
                                                if (!string.IsNullOrWhiteSpace(tempAddress.City))
                                                    accountSAP.Addresses.City = tempAddress.City;
                                                if (!string.IsNullOrWhiteSpace(tempAddress.ZipCode))
                                                    accountSAP.Addresses.ZipCode = tempAddress.ZipCode;
                                                if (!string.IsNullOrWhiteSpace(tempAddress.StateName))
                                                    accountSAP.Addresses.State = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCST", "Code", "\"Name\" = '" + tempAddress.StateName + "'");
                                                if (!string.IsNullOrWhiteSpace(tempAddress.CountryName))
                                                    accountSAP.Addresses.Country = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRY", "Code", "\"Name\" = '" + tempAddress.CountryName + "'");

                                                if (!string.IsNullOrWhiteSpace(tempAddress.LicTradNum))
                                                    accountSAP.Addresses.FederalTaxID = tempAddress.LicTradNum;

                                                accountSAP.Addresses.Add();
                                            }
                                        }
                                    }
                                }
                            }
                        }



                        //Adding non existent contacts
                        if (!string.IsNullOrWhiteSpace(accountSAP.Addresses.AddressName))
                            accountSAP.Addresses.Add();

                        for (int i = 0; i < list.List.Count; i++)
                        {
                            if (!AddressExists(oCompany, list.Id, list.List[i].Id) && !list.List[i].Deleted)
                            {
                                accountSAP.Addresses.AddressName = list.List[i].Id;
                                if (!string.IsNullOrWhiteSpace(list.List[i].Street))
                                    accountSAP.Addresses.Street = list.List[i].Street;
                                if (!string.IsNullOrWhiteSpace(list.List[i].Building))
                                    accountSAP.Addresses.BuildingFloorRoom = list.List[i].Building;
                                if (!string.IsNullOrWhiteSpace(list.List[i].City))
                                    accountSAP.Addresses.City = list.List[i].City;
                                if (!string.IsNullOrWhiteSpace(list.List[i].ZipCode))
                                    accountSAP.Addresses.ZipCode = list.List[i].ZipCode;
                                if (!string.IsNullOrWhiteSpace(list.List[i].StateName))
                                    accountSAP.Addresses.State = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCST", "Code", "\"Name\" = '" + list.List[i].StateName + "'");
                                if (!string.IsNullOrWhiteSpace(list.List[i].CountryName))
                                    accountSAP.Addresses.Country = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRY", "Code", "\"Name\" = '" + list.List[i].CountryName + "'");
                                switch (list.List[i].Type)
                                {
                                    case CRMBPAddressModel.AddressType.Bill:
                                        accountSAP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_BillTo;
                                        break;
                                    case CRMBPAddressModel.AddressType.Shipping:
                                        accountSAP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
                                        break;
                                }

                                if (!string.IsNullOrWhiteSpace(list.List[i].LicTradNum))
                                    accountSAP.Addresses.FederalTaxID = list.List[i].LicTradNum;

                                accountSAP.Addresses.Add();
                            }
                        }

                        //Setting the new Main Contact
                        //accountSAP.ContactPerson = list.CntctPrsnId;
                        accountSAP.Update();

                        oCompany.GetLastError(out iError, out _error);
                        if (iError != 0)
                        {
                            _error = iError.ToString() + ": " + _error;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Address list update error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }

        #endregion *************************************************************************************************


        #region Other functions **********************************************************************************
        public static bool AddressExists(SAPbobsCOM.Company oCompany, string cardCode, string name)
        {
            bool cpExists = false;

            try
            {
                SAPbobsCOM.Recordset recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string sql = "SELECT \"Address\", \"CardCode\", \"Street\" " +
                             "FROM \"CRD1\" " +
                             "WHERE \"CardCode\" = '" + cardCode + "' AND \"Address\" = '" + name + "'";

                if (!string.IsNullOrWhiteSpace(sql))
                {
                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);

                    if (recordSet.RecordCount > 0)
                    {
                        cpExists = true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION ContactPersonExists(" + cardCode + ", " + name + "): " + ex, ErrorHandler.ErrorVerboseLevel.None);
            }

            return cpExists;
        }
        #endregion************************************************************************************************
    }
}