﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WebApiFarmavet.Services.SAP
{
    public enum SAPErrorType
    {
        [DescriptionAttribute("Correcto")] ok = 0,
        [DescriptionAttribute("Error")] error = -1
    }
    public static class SAPError
    {
        private static string GetDescriptionAttribute(object enumvalue)
        {
            FieldInfo fi = enumvalue.GetType().GetField(enumvalue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            return enumvalue.ToString();
        }



        public static string GetNameError(SAPErrorType er)
        {
            return GetDescriptionAttribute(er);
        }

    }
}