﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public class SAPDeliveries
    {
        #region Models constructors from recordset *************************************************************************
        private static CRMDeliveredItemLineModel GetItemDeliveredModelFromRecordset(SAPbobsCOM.Recordset recordset, bool withBatchs)
        {
            CRMDeliveredItemLineModel tempModel = new CRMDeliveredItemLineModel
            {
                Id = recordset.Fields.Item("ItemCode").Value.ToString(),
                /*docEntry = recordset.Fields.Item("DocEntry").Value.ToString(),
                docNum = recordset.Fields.Item("DocNum").Value.ToString(),
                docDate = recordset.Fields.Item("DocDate").Value,*/

                itemName = recordset.Fields.Item("ItemName").Value,
                description = recordset.Fields.Item("Dscription").Value,
                price = recordset.Fields.Item("Price").Value,
                vatPercent = recordset.Fields.Item("VatPrcnt").Value,
                quantity = Convert.ToInt32(recordset.Fields.Item("Quantity").Value),
                avaiable = recordset.Fields.Item("Avaiable").Value,
                invUnitMsrId = recordset.Fields.Item("IUoMEntry").Value,
                invUnitMsr = recordset.Fields.Item("IUomCode").Value,
                salesUnitMsrId = recordset.Fields.Item("SUoMEntry").Value,
                salesUnitMsr = recordset.Fields.Item("SUomCode").Value,

                baseLine = recordset.Fields.Item("LineNum").Value,
                lineTotal = recordset.Fields.Item("LineTotal").Value,

                customerDscnt = recordset.Fields.Item("DiscPrcnt").Value
            };

            if (withBatchs)
            {
                tempModel.batchNum = recordset.Fields.Item("BatchNum").Value.ToString();
                tempModel.batchQty = Convert.ToInt32(recordset.Fields.Item("BatchQty").Value);
            }

            return tempModel;
        }
        #endregion ********************************************************************************************************

        #region Get Delivered items list from order ***********************************************************************
        /// <summary>
        /// Retunrs a list of delivered items from an order
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson"></param>
        /// <returns></returns>
        public static List<CRMDeliveredItemModel> GetDeliveredItemsFromOrder(SAPbobsCOM.Company oCompany, int salesPerson, string order, bool withBatchs)
        {
            List<CRMDeliveredItemModel> deliveredItemList = new List<CRMDeliveredItemModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string orderFilter = "";
            string orderBy = "";
            CRMDeliveredItemModel tempDelivery;
            CRMDeliveredItemLineModel tempDeliveredItem;


            ErrorHandler.WriteInLog("Getting items delivered from an order list: SalesPerson(" + salesPerson.ToString() +
                                    "); Order(" + order + ")",
                                    ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = " AND " + string.Format(SAPDeliveriesQuerys.GetDeliveredItemsFromOrder_SalesPersonFilter_HANA_OK, salesPerson.ToString());
                    //Order Filter
                    if (!string.IsNullOrWhiteSpace(order))
                        orderFilter = " AND " + string.Format(SAPDeliveriesQuerys.GetDeliveredItemsFromOrder_OrderFilter_HANA_OK, order);
                    //Ordering by DocNum
                    orderBy = " " + SAPDeliveriesQuerys.GetDeliveredItemsFromOrder_OrderBy_HANA_OK;

                    if (!withBatchs)
                    {
                        sql = SAPDeliveriesQuerys.GetDeliveredItemsFromOrder_HANA_OK + salesPersonFilter + orderFilter + orderBy;
                        ErrorHandler.WriteInLog("SAPDeliveriesQuerys.GetDeliveredItemsFromOrder  " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191127 WriteSQL log
                    }
                    else
                    {
                        sql = SAPDeliveriesQuerys.GetDeliveredItemsFromOrder_WithBatchs_HANA_OK + salesPersonFilter + orderFilter + orderBy;
                        ErrorHandler.WriteInLog("SAPSAPDeliveriesQuerysSalesQuerys.GetDeliveredItemsFromOrder_WithBatchs  " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191127 WriteSQL log
                    }

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempDelivery = deliveredItemList.FirstOrDefault(x => x.docEntry == recordset.Fields.Item("DocEntry").Value.ToString());
                            //Searching for an delivery on the list with this code
                            if (tempDelivery == null)
                            {
                                //No delivery found. We must add a new one
                                tempDelivery = new CRMDeliveredItemModel()
                                {
                                    docEntry = recordset.Fields.Item("DocEntry").Value.ToString(),
                                    docNum = recordset.Fields.Item("DocNum").Value.ToString(),
                                    docDate = recordset.Fields.Item("DocDate").Value
                                };
                                deliveredItemList.Add(tempDelivery);
                            }
                            //Now tempDelivery have the delivery model with its item list
                            //Getting the data of the item
                            tempDeliveredItem = GetItemDeliveredModelFromRecordset(recordset, withBatchs);
                            tempDelivery.Add(tempDeliveredItem); //adding item to the delivery
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    deliveredItemList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetDeliveredItemsFromOrder: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return deliveredItemList;
        }
        #endregion ********************************************************************************************************

        #region Get delivered item info ***********************************************************************************
        public static CRMDeliveredItemInfoModel GetDeliveredItemInfo (SAPbobsCOM.Company oCompany, string cardCode, string itemCode)
        {
            CRMDeliveredItemInfoModel deliveredItem = null;
            SAPbobsCOM.Recordset recordset;
            string sql;
            string customerFilter = "";
            string itemFilter = "";
            

            ErrorHandler.WriteInLog("Getting delivered item info: CardCode(" + cardCode + "); Item(" + itemCode + ")",
                                    ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Customer Filter
                    if (!string.IsNullOrWhiteSpace(cardCode))
                        customerFilter = string.Format(SAPDeliveriesQuerys.GetDeliveredItemInfo_CustomerFilter_HANA_OK, cardCode);
                    //ItemFilter
                    if (!string.IsNullOrWhiteSpace(itemCode))
                        itemFilter = string.Format(SAPDeliveriesQuerys.GetDeliveredItemInfo_ItemFilter_HANA_OK, itemCode);


                    sql = string.Format(SAPDeliveriesQuerys.GetDeliveredItemInfo_HANA_OK, customerFilter + itemFilter);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        if (!recordset.EoF)
                        {
                            deliveredItem = new CRMDeliveredItemInfoModel
                            {
                                Id = recordset.Fields.Item("ItemCode").Value.ToString(),
                                itemName = recordset.Fields.Item("ItemName").Value.ToString(),
                                quantity = Convert.ToInt32(recordset.Fields.Item("Quantity").Value),
                                openQty = Convert.ToInt32(recordset.Fields.Item("OpenQty").Value)
                            };
                        }
                    }
                    else
                    {
                        deliveredItem = new CRMDeliveredItemInfoModel
                                            { Id = itemCode, itemName = "", quantity = 0, openQty = 0 };
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION GetDeliveredItemsInfo: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return deliveredItem;
        }
        #endregion ********************************************************************************************************
    }
}