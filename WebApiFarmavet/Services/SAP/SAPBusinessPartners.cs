﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public class SAPBusinessPartners
    {
        public enum BusinessPartnerType { Customer, Lead, Supplier, NotDefined };


        #region Other functions ******************************************************************************************

        public static void NameToFirstLastName(string completeName, out string _firstName, out string _lastName)
        {
            string[] elementosNombre = completeName.Split(' ');
            switch (elementosNombre.Length)
            {
                case 1:
                    _firstName = elementosNombre[0];
                    _lastName = "";
                    break;
                case 2:
                    _firstName = elementosNombre[0];
                    _lastName = elementosNombre[1];
                    break;
                case 3: //This will depend on the country :S
                    _firstName = elementosNombre[0];
                    _lastName = elementosNombre[1] + " " + elementosNombre[2];
                    break;
                default:
                    _firstName = elementosNombre[0];
                    for (int i = 1; i < elementosNombre.Length - 2; i++)
                    {
                        _firstName = _firstName + " " + elementosNombre[i];
                    }
                    _lastName = elementosNombre[elementosNombre.Length - 2] + " " +
                                elementosNombre[elementosNombre.Length - 1];
                    break;
            }
        }

        #endregion *******************************************************************************************************




        #region INFO CONVERTERS **********************************************************************************
        private static CRMAccountModel GetModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            CRMAccountModel _account = new CRMAccountModel();
            string tempString1, tempString2;

            _account.BillAddress = new CRMBPAddressModel();
            _account.ShippingAddress = new CRMBPAddressModel();

            _account.Id = _recordSet.Fields.Item("CardCode").Value;
            _account.Company = _recordSet.Fields.Item("CardName").Value;
            _account.ImageUrl = _recordSet.Fields.Item("Picture").Value;
            switch (_recordSet.Fields.Item("CardType").Value)
            {
                case "C":
                    _account.Type = CRMSummaryAccountModel.BpType.Customer;
                    break;
                case "L":
                    _account.Type = CRMSummaryAccountModel.BpType.Lead;
                    break;
                case "S":
                    _account.Type = CRMSummaryAccountModel.BpType.Supplier;
                    break;
            }

            //Contact data *******************************************************************************
            _account.CntctPrsnId = _recordSet.Fields.Item("CntctPrsn").Value;

            if (string.IsNullOrWhiteSpace(_recordSet.Fields.Item("FirstName").Value) &&
                string.IsNullOrWhiteSpace(_recordSet.Fields.Item("LastName").Value))
            {
                NameToFirstLastName(_recordSet.Fields.Item("CntctPrsn").Value,
                    out tempString1, out tempString2);
                _account.FirstName = tempString1;
                _account.LastName = tempString2;
            }
            else
            {
                _account.FirstName = _recordSet.Fields.Item("FirstName").Value;
                if (!string.IsNullOrWhiteSpace(_recordSet.Fields.Item("MiddleName").Value))
                    _account.FirstName = _account.FirstName + " " + _recordSet.Fields.Item("MiddleName").Value;

                _account.LastName = _recordSet.Fields.Item("LastName").Value;
            }

            _account.JobTitle = _recordSet.Fields.Item("Position").Value;
            _account.Phone1 = _recordSet.Fields.Item("Phone1").Value;
            _account.Phone2 = _recordSet.Fields.Item("Phone2").Value;
            _account.MPhone = _recordSet.Fields.Item("Cellular").Value;
            _account.Email = _recordSet.Fields.Item("E_Mail").Value;
            _account.Nif = _recordSet.Fields.Item("LicTradNum").Value;

            _account.SalesPerson = _recordSet.Fields.Item("SlpCode").Value;

            //Bill-Address data ***************************************************************************
            _account.BillAddress.Id = _recordSet.Fields.Item("B_AddressID").Value;      //Address ID
            _account.BillAddress.Street = _recordSet.Fields.Item("B_Street").Value;
            _account.BillAddress.Building = _recordSet.Fields.Item("B_Building").Value;   //Adds build. nº, floor, room...
            _account.BillAddress.City = _recordSet.Fields.Item("B_City").Value;
            _account.BillAddress.ZipCode = _recordSet.Fields.Item("B_ZipCode").Value;
            _account.BillAddress.StateName = _recordSet.Fields.Item("B_StateName").Value;   //This is a string from OCST table
            _account.BillAddress.CountryName = _recordSet.Fields.Item("B_CountryName").Value; //This is a string from OCRY table

            //Shipping-Address data ***********************************************************************
            _account.ShippingAddress.Id = _recordSet.Fields.Item("S_AddressID").Value;      //Address ID
            _account.ShippingAddress.Street = _recordSet.Fields.Item("S_Street").Value;
            _account.ShippingAddress.Building = _recordSet.Fields.Item("S_Building").Value;   //Adds build. nº, floor, room...
            _account.ShippingAddress.City = _recordSet.Fields.Item("S_City").Value;
            _account.ShippingAddress.ZipCode = _recordSet.Fields.Item("S_ZipCode").Value;
            _account.ShippingAddress.StateName = _recordSet.Fields.Item("S_StateName").Value;   //This is a string from OCST table
            _account.ShippingAddress.CountryName = _recordSet.Fields.Item("S_CountryName").Value; //This is a string from OCRY table

            //Rest of data
            //_account.Longitude = 0;
            //_account.Latitude = 0;
            _account.Industry = _recordSet.Fields.Item("IndName").Value;
            _account.OpportunityPercent = _recordSet.Fields.Item("avgPercent").Value; //Sacar de JSonIgnore, y meter Opportunity stage
            _account.OpportunitySize = _recordSet.Fields.Item("sumPotential").Value;


            //FARMAVET data ******************************************************************************
            _account.RouteId = _recordSet.Fields.Item("RouteID").Value.ToString();
            _account.RouteName = _recordSet.Fields.Item("RouteName").Value.ToString();


            return _account;
        }


        private static CRMSummaryAccountModel GetSummarizedModelFromRecordset(SAPbobsCOM.Company oCompany,
                                                                        SAPbobsCOM.Recordset _recordSet,
                                                                        BusinessPartnerType _type)
        {
            CRMSummaryAccountModel _account = new CRMSummaryAccountModel();
            string tempString1, tempString2;

            _account.Id = _recordSet.Fields.Item("CardCode").Value;

            //Contact data *******************************************************************************
            if (string.IsNullOrWhiteSpace(_recordSet.Fields.Item("FirstName").Value) &&
                string.IsNullOrWhiteSpace(_recordSet.Fields.Item("LastName").Value))
            {
                NameToFirstLastName(_recordSet.Fields.Item("CntctPrsn").Value,
                    out tempString1, out tempString2);
                _account.FirstName = tempString1;
                _account.LastName = tempString2;
            }
            else
            {
                _account.FirstName = _recordSet.Fields.Item("FirstName").Value;
                if (!string.IsNullOrWhiteSpace(_recordSet.Fields.Item("MiddleName").Value))
                    _account.FirstName = _account.FirstName + " " + _recordSet.Fields.Item("MiddleName").Value;

                _account.LastName = _recordSet.Fields.Item("LastName").Value;
            }



            //Rest of data
            _account.Company = _recordSet.Fields.Item("CardName").Value;
            switch (_type)
            {
                case BusinessPartnerType.Customer:
                    _account.Type = CRMSummaryAccountModel.BpType.Customer;
                    break;
                case BusinessPartnerType.Lead:
                    _account.Type = CRMSummaryAccountModel.BpType.Lead;
                    break;
                case BusinessPartnerType.Supplier:
                    _account.Type = CRMSummaryAccountModel.BpType.Supplier;
                    break;
                //If there is no BP type predefined, let's check on the SQL
                default:
                    tempString1 = _recordSet.Fields.Item("CardType").Value;
                    switch (tempString1)
                    {
                        case "C": _account.Type = CRMSummaryAccountModel.BpType.Customer; break;
                        case "L": _account.Type = CRMSummaryAccountModel.BpType.Lead; break;
                        case "S": _account.Type = CRMSummaryAccountModel.BpType.Supplier; break;
                    }
                    break;
            }

            return _account;
        }
        #endregion ***********************************************************************************************


        #region Business partners getters ********************************************************************************

        /// <summary>
        /// Get a complete list of a given Business Partner type, but only with basic data: company name
        /// and contact person name
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="bpType"></param>
        /// <returns></returns>
        protected static List<CRMSummaryAccountModel> GetSumarizedBusinessPartnerTypeList(SAPbobsCOM.Company oCompany,
                                                                BusinessPartnerType bpType, int salesPerson, string extraFilters, string marcaFilters)
        {
            List<CRMSummaryAccountModel> businessPartnersList = new List<CRMSummaryAccountModel>();
            SAPbobsCOM.Recordset recordSet;
            CRMSummaryAccountModel tempAccount;

            /*ErrorHandler.WriteInLog(string.Format("*Getting BP List: BPType({0}), SalesPerson({1}), filters: {2}",
                                                    bpType.ToString(), salesPerson.ToString(), extraFilters), ErrorHandler.ErrorVerboseLevel.Trace);*/

            string bpQueryCardType = "";
            string gbptlQuery = "";
            string salesPersonFilter = "";

            if ((oCompany != null) && (oCompany.Connected))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Getting all the customers on SAP
                    //We need all the cardcodes of customers, to use bPartners to retrieve info. OCDR is the table
                    // of Business Partners, it has customers (cardtype = 'C'), vendors ('S') and leads ('L')
                    switch (bpType)
                    {
                        case BusinessPartnerType.Customer:
                            bpQueryCardType = "C";
                            break;
                        case BusinessPartnerType.Lead:
                            bpQueryCardType = "L";
                            break;
                        case BusinessPartnerType.Supplier:
                            bpQueryCardType = "S";
                            break;
                    }

                    //Adding sales person filter if valid

                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_SalesPersonFilter_HANA_OK, salesPerson.ToString());

                    gbptlQuery = string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_main_HANA_OK,
                                               string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_SalesPersonTypeFilter_HANA_OK, bpQueryCardType),
                                               salesPersonFilter,
                                               extraFilters);
                    
                    if (!string.IsNullOrWhiteSpace(gbptlQuery))
                    {
                        recordSet.DoQuery(gbptlQuery);
                        ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);

                        if (recordSet.RecordCount > 0)
                        {
                            recordSet.MoveFirst();
                            while (!recordSet.EoF)
                            {
                                tempAccount = GetSummarizedModelFromRecordset(oCompany, recordSet, bpType);
                                if (tempAccount != null)
                                    businessPartnersList.Add(tempAccount);
                                recordSet.MoveNext();
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    businessPartnersList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return businessPartnersList;
        }


        public static CRMAccountModel GetBusinessPartner(SAPbobsCOM.Company oCompany, string _cardCode)
        {
            CRMAccountModel businessPartner = new CRMAccountModel();
            SAPbobsCOM.Recordset recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            try
            {
                //string gbptlQuery = string.Format(SAPBusinessPartnersQuerys.BPData, _cardCode);
                string gbptlQuery = string.Format(SAPBusinessPartnersQuerys.BPData_Farmavet_HANA_OK, _cardCode);

                recordSet.DoQuery(gbptlQuery);
                ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                if (recordSet.RecordCount > 0)
                {
                    recordSet.MoveFirst();
                    businessPartner = GetModelFromRecordset(oCompany, recordSet);
                }
                else
                {
                    businessPartner = null;
                    ErrorHandler.WriteInLog("BusinessPartner '" + _cardCode + "' no encontrado.", ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            catch (Exception ex)
            {
                businessPartner = null;
                ErrorHandler.WriteInLog("EXCEPTION! (GetBusinessPartner '" + _cardCode + "'): " + ex, ErrorHandler.ErrorVerboseLevel.None);
            }

            return businessPartner;
        }
        #endregion *******************************************************************************************************




        #region Customers ************************************************************************************************
        /// <summary>
        /// Returns a list of sumarized version of CRMAccount, just with the DisplayContact (FirstName and
        /// LastName) and Company
        /// </summary>
        /// <param name="oCompany">SAP Company. It has not to be null, and has to be connected!</param>
        /// <returns></returns>
        public static List<CRMSummaryAccountModel> GetSummarizedCustomersList(SAPbobsCOM.Company oCompany, int salesPerson)
        {
            return GetSumarizedBusinessPartnerTypeList(oCompany, BusinessPartnerType.Customer, salesPerson, null, null);
        }

        /// <summary>
        /// Returns a list of sumarized version of CRMAccount, just with the DisplayContact (FirstName and
        /// LastName) and Company, filtered by some fields
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson"></param>
        /// <param name="cardName"></param>
        /// <param name="customer"></param>
        /// <param name="route"></param>
        /// <returns></returns>
        public static List<CRMSummaryAccountModel> GetSummarizedCustomersList(SAPbobsCOM.Company oCompany, int salesPerson,
                                                                              string cardCode, string cardName, string customer, string route)
        {
            string extraFilters = "";
            string marcaFilters = "";

            if (!string.IsNullOrWhiteSpace(cardCode))
                extraFilters = string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_CardCodeFilter_HANA_OK, cardCode);
            if (!string.IsNullOrWhiteSpace(cardName))
                extraFilters += string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_CardNameFilter_HANA_OK, cardName);
            if (!string.IsNullOrWhiteSpace(customer))
                extraFilters += string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_ContactFilter_HANA_OK, customer);
            if (!string.IsNullOrWhiteSpace(route))
                extraFilters += string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_RouteFilter_HANA_OK, route);
            
            
            return GetSumarizedBusinessPartnerTypeList(oCompany, BusinessPartnerType.Customer, salesPerson, extraFilters, marcaFilters);
        }


        public static CRMAccountModel GetCustomer(SAPbobsCOM.Company oCompany, string _cardCode)
        {
            return GetBusinessPartner(oCompany, _cardCode);
        }
        #endregion *******************************************************************************************************

        #region Leads ****************************************************************************************************
        /// <summary>
        /// Returns a list of sumarized version of CRMAccount, just with the DisplayContact (FirstName and
        /// LastName) and Company
        /// </summary>
        /// <param name="oCompany">SAP Company. It has not to be null, and has to be connected!</param>
        /// <returns></returns>
        public static List<CRMSummaryAccountModel> GetSummarizedLeadsList(SAPbobsCOM.Company oCompany, int salesPerson)
        {
            return GetSumarizedBusinessPartnerTypeList(oCompany, BusinessPartnerType.Lead, salesPerson, null, null);
        }

        /// <summary>
        /// Returns a list of sumarized version of CRMAccount, just with the DisplayContact (FirstName and
        /// LastName) and Company, filtered by some fields
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson"></param>
        /// <param name="cardName"></param>
        /// <param name="contact"></param>
        /// <param name="route"></param>
        /// <returns></returns>
        public static List<CRMSummaryAccountModel> GetSummarizedLeadsList(SAPbobsCOM.Company oCompany, int salesPerson,
                                                                          string cardCode, string cardName, string contact, string route)
        {
            string extraFilters = "";
            string marcaFilters = "";

            if (!string.IsNullOrWhiteSpace(cardCode))
                extraFilters = string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_CardCodeFilter_HANA_OK, cardCode);
            if (!string.IsNullOrWhiteSpace(cardName))
                extraFilters += string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_CardNameFilter_HANA_OK, cardName);
            if (!string.IsNullOrWhiteSpace(contact))
                extraFilters += string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_ContactFilter_HANA_OK, contact);
            if (!string.IsNullOrWhiteSpace(route))
                extraFilters += string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_RouteFilter_HANA_OK, route);
            
            return GetSumarizedBusinessPartnerTypeList(oCompany, BusinessPartnerType.Lead, salesPerson, extraFilters, marcaFilters);
        }


        public static CRMAccountModel GetLead(SAPbobsCOM.Company oCompany, string _cardCode)
        {
            return GetBusinessPartner(oCompany, _cardCode);
        }
        #endregion *******************************************************************************************************


        #region Business partners paged getters **************************************************************************

        public static List<CRMSummaryAccountModel> GetSumarizedBusinessPartnerPagedList(SAPbobsCOM.Company oCompany,
                                                                                        bool getCustomers, bool getLeads, bool getSuppliers,
                                                                                        int salesPerson, string cardCode, string cardName,
                                                                                        string contact, string route, string marcaFilters,
                                                                                        int page)
        {
            List<CRMSummaryAccountModel> businessPartnersList = new List<CRMSummaryAccountModel>();
            SAPbobsCOM.Recordset recordSet;
            CRMSummaryAccountModel tempAccount;

            /*ErrorHandler.WriteInLog(string.Format("*Getting BP List: BPType({0}), SalesPerson({1}), filters: {2}",
                                                    bpType.ToString(), salesPerson.ToString(), extraFilters), ErrorHandler.ErrorVerboseLevel.Trace);*/

            string gbptlQuery = "";
            string cardTypeFilter = "";
            string salesPersonFilter = "";
            string extraFilters = "";
            string pageFilter = "";

            int firstRow;
            int lastRow;
            int pageRows;

            if ((oCompany != null) && (oCompany.Connected))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Getting all the business partners on SAP
                    //We need all the cardcodes of customers, to use bPartners to retrieve info. OCDR is the table
                    // of Business Partners, it has customers (cardtype = 'C'), vendors ('S') and leads ('L')
                    if (getCustomers)
                        cardTypeFilter = " \"CardType\" = 'C' ";
                    if (getLeads)
                    {
                        if (cardTypeFilter.Length > 0)
                            cardTypeFilter += " OR ";
                        cardTypeFilter += " \"CardType\" = 'L' ";
                    }
                    if (getSuppliers)
                    {
                        if (cardTypeFilter.Length > 0)
                            cardTypeFilter += " OR ";
                        cardTypeFilter += " \"CardType\" = 'S' ";
                    }
                    if (cardTypeFilter.Length > 0)
                        cardTypeFilter = " AND (" + cardTypeFilter + ") ";


                    //Adding sales person filter if valid
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_SalesPersonFilter_HANA_OK, salesPerson.ToString());

                    //Adding more filters
                    if (!string.IsNullOrWhiteSpace(cardCode))
                        extraFilters = string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_CardCodeFilter_HANA_OK, cardCode);
                    if (!string.IsNullOrWhiteSpace(cardName))
                        extraFilters += string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_CardNameFilter_HANA_OK, cardName);
                    if (!string.IsNullOrWhiteSpace(contact))
                        extraFilters += string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_ContactFilter_HANA_OK, contact);
                    if (!string.IsNullOrWhiteSpace(route))
                        extraFilters += string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_RouteFilter_HANA_OK, route);

                    //Page filter
                    if (page >= 0)
                    {
                        if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("PageRows"), out pageRows))
                            pageRows = 10;
                        firstRow = (page * pageRows) + 1;
                        lastRow = firstRow + pageRows - 1;
                        pageFilter = string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_PageFilter_HANA_OK, firstRow, lastRow);
                    }

                    gbptlQuery = string.Format(SAPBusinessPartnersQuerys.BPSummarizedList_main_HANA_OK,
                                               cardTypeFilter,
                                               salesPersonFilter,
                                               extraFilters) + 
                                 pageFilter;

                    if (!string.IsNullOrWhiteSpace(gbptlQuery))
                    {
                        recordSet.DoQuery(gbptlQuery);
                        ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);

                        if (recordSet.RecordCount > 0)
                        {
                            recordSet.MoveFirst();
                            while (!recordSet.EoF)
                            {
                                tempAccount = GetSummarizedModelFromRecordset(oCompany, recordSet, BusinessPartnerType.NotDefined);
                                if (tempAccount != null)
                                    businessPartnersList.Add(tempAccount);
                                recordSet.MoveNext();
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    businessPartnersList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return businessPartnersList;
        }

        #endregion *******************************************************************************************************
    }
}