﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPInvoices
    {
        #region Models constructors from recordset *************************************************************************
        private static CRMInvoiceModel GetInvoiceFromRecordset(SAPbobsCOM.Recordset recordset)
        {
            CRMInvoiceModel tempModel = new CRMInvoiceModel();

            tempModel.Id = recordset.Fields.Item("DocEntry").Value.ToString();
            tempModel.Name = recordset.Fields.Item("DocNum").Value.ToString();
            tempModel.CardCode = recordset.Fields.Item("CardCode").Value.ToString();
            tempModel.CardName = recordset.Fields.Item("CardName").Value.ToString();
            tempModel.DocTotal = recordset.Fields.Item("DocTotal").Value;
            tempModel.DocDate = recordset.Fields.Item("DocDate").Value;
            tempModel.DocDueDate = recordset.Fields.Item("DocDueDate").Value;

            switch(recordset.Fields.Item("DocStatus").Value)
            {
                case "C": tempModel.Open = false; break;
                default: tempModel.Open = true; break;
            }
            tempModel.PayMethodId = recordset.Fields.Item("PeyMethod").Value;
            tempModel.PayMethodName = recordset.Fields.Item("Descript").Value;

            return tempModel;
        }
        #endregion ********************************************************************************************************

        #region Get pending payment list *********************************************************************************
        /// <summary>
        /// Retunrs a list of pending payments to this sales person
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson"></param>
        /// <returns></returns>
        public static List<CRMInvoiceModel> GetInvoicesList(SAPbobsCOM.Company oCompany, int salesPerson, string cardCode, string docNum, string cardName, string paymentMethod,
                                                            string dateFrom, string dateTo)
        {
            List<CRMInvoiceModel> invoicesList = new List<CRMInvoiceModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string cardCodeFilter = "";
            string docNumFilter = "";
            string cardNameFilter = "";
            string paymentMethodFilter = "";
            string orderFilter = "";
            string dateFilter = "";
            CRMInvoiceModel tempInvoice;

            ErrorHandler.WriteInLog("Getting Invoices list: SalesPerson(" + salesPerson.ToString() + "), CardCode(" + cardCode + "), DocNum(" + docNum + "), CardName(" + cardName +
                                    "), Payment Method(" + paymentMethod + ")",
                                    ErrorHandler.ErrorVerboseLevel.Trace);
            
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPInvoicesQuerys.GetInvoices_SLPFilter_HANA_OK, salesPerson.ToString());
                    if (!string.IsNullOrWhiteSpace(cardCode))
                        cardCodeFilter = string.Format(SAPInvoicesQuerys.GetInvoices_CardCodeFilter_HANA_OK, cardCode);
                    if (!string.IsNullOrWhiteSpace(docNum))
                        docNumFilter = string.Format(SAPInvoicesQuerys.GetInvoices_docNumFilter_HANA_OK, docNum);
                    if (!string.IsNullOrWhiteSpace(cardName))
                        cardNameFilter = string.Format(SAPInvoicesQuerys.GetInvoices_cardNameFilter_HANA_OK, cardName);
                    if (!string.IsNullOrWhiteSpace(paymentMethod))
                        paymentMethodFilter = string.Format(SAPInvoicesQuerys.GetInvoices_paymentMethodFilter_HANA_OK, paymentMethod);
                    //Filtering list by dates
                    if (!string.IsNullOrWhiteSpace(dateFrom) && !string.IsNullOrWhiteSpace(dateTo))
                    {
                        dateFilter = " AND " + string.Format(SAPInvoicesQuerys.GetInvoices_DatesFilter_NO_HANA,
                            DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")),
                            DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));
                    }
                    orderFilter = SAPInvoicesQuerys.GetInvoices_OrderFilter_HANA_OK;

                    sql = SAPInvoicesQuerys.GetInvoices_HANA_OK + salesPersonFilter + cardCodeFilter + docNumFilter + cardNameFilter + paymentMethodFilter + dateFilter + orderFilter;

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempInvoice = GetInvoiceFromRecordset(recordset);
                            invoicesList.Add(tempInvoice);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    invoicesList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetPendingPaymentList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return invoicesList;
        }
        #endregion ********************************************************************************************************

        #region Get Orders individual data *******************************************************************************
        public static double GetSumDocTotal(SAPbobsCOM.Company oCompany, int salesPerson, string cardCode, string docNum, string cardName, string paymentMethod,
                                            string dateFrom, string dateTo)
        {
            SAPbobsCOM.Recordset recordset;
            double docTotalSum = 0;
            string sql;
            string salesPersonFilter = "";
            string docNumFilter = "";
            string cardCodeFilter = "";
            string cardNameFilter = "";
            string paymentMethodFilter = "";
            string dateFilter = "";

            ErrorHandler.WriteInLog("Getting Orders DocTotal Sum: SalesPerson(" + salesPerson.ToString() + "); DocNum(" + docNum +
                                    "*); Customer(" + cardName + "*); Payment Method(" + paymentMethod + ")", ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPInvoicesQuerys.GetInvoices_SLPFilter_HANA_OK, salesPerson.ToString());
                    if (!string.IsNullOrWhiteSpace(cardCode))
                        cardCodeFilter = string.Format(SAPInvoicesQuerys.GetInvoices_CardCodeFilter_HANA_OK, cardCode);
                    if (!string.IsNullOrWhiteSpace(docNum))
                        docNumFilter = string.Format(SAPInvoicesQuerys.GetInvoices_docNumFilter_HANA_OK, docNum);
                    if (!string.IsNullOrWhiteSpace(cardName))
                        cardNameFilter = string.Format(SAPInvoicesQuerys.GetInvoices_cardNameFilter_HANA_OK, cardName);
                    if (!string.IsNullOrWhiteSpace(paymentMethod))
                        paymentMethodFilter = string.Format(SAPInvoicesQuerys.GetInvoices_paymentMethodFilter_HANA_OK, paymentMethod);
                    //Filtering list by dates
                    if (!string.IsNullOrWhiteSpace(dateFrom) && !string.IsNullOrWhiteSpace(dateTo))
                    {
                        dateFilter = " AND " + string.Format(SAPInvoicesQuerys.GetInvoices_DatesFilter_NO_HANA,
                            DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")),
                            DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));
                    }


                    //Creando la SQL componiendo la SQL de documentos y borradores
                    sql = SAPInvoicesQuerys.GetInvoicesSumDocTotal_HANA_OK + salesPersonFilter + cardCodeFilter + docNumFilter + paymentMethodFilter + cardNameFilter + dateFilter;

                    //ErrorHandler.WriteInLog(sql, ErrorHandler.ErrorVerboseLevel.Trace);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (!recordset.EoF)
                    {
                        recordset.MoveFirst();
                        docTotalSum = recordset.Fields.Item("DocTotal").Value;
                    }
                }
                catch (Exception ex)
                {
                    docTotalSum = 0;
                    ErrorHandler.WriteInLog("EXCEPTION Get orders DocTotal Sum: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            //}

            return docTotalSum;
        }
        #endregion *******************************************************************************************************
    }
}