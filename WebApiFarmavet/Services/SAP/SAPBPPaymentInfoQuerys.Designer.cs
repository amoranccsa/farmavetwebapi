﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApiFarmavet.Services.SAP {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class SAPBPPaymentInfoQuerys {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SAPBPPaymentInfoQuerys() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WebApiFarmavet.Services.SAP.SAPBPPaymentInfoQuerys", typeof(SAPBPPaymentInfoQuerys).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetPaymentConditions_HANA_OK
        ///SELECT &quot;GroupNum&quot;, &quot;PymntGroup&quot; FROM &quot;OCTG&quot;.
        /// </summary>
        internal static string GetPaymentConditions_HANA_OK {
            get {
                return ResourceManager.GetString("GetPaymentConditions_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a WHERE &quot;OCRD&quot;.&quot;CardCode&quot; = &apos;{0}&apos;.
        /// </summary>
        internal static string GetPaymentInfo_CardCodeFilter_HANA_OK {
            get {
                return ResourceManager.GetString("GetPaymentInfo_CardCodeFilter_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetPaymentInfo_HANA_OK
        ///SELECT &quot;OCRD&quot;.&quot;CardCode&quot;, &quot;OCRD&quot;.&quot;BankCode&quot;, &quot;OCRD&quot;.&quot;DflBranch&quot;,
        ///       &quot;OCRD&quot;.&quot;BankCtlKey&quot;, &quot;OCRD&quot;.&quot;DflAccount&quot;, &quot;OCRD&quot;.&quot;DflIBAN&quot;,
        ///       &quot;OCTG&quot;.&quot;GroupNum&quot;, &quot;OCTG&quot;.&quot;PymntGroup&quot;, &quot;OCRD&quot;.&quot;Free_Text&quot;,
        ///       &quot;OCRD&quot;.&quot;PymCode&quot;, &quot;OPYM&quot;.&quot;Descript&quot;
        ///FROM &quot;OCRD&quot;
        ///LEFT JOIN &quot;OCTG&quot; ON &quot;OCRD&quot;.&quot;GroupNum&quot; = &quot;OCTG&quot;.&quot;GroupNum&quot;
        ///LEFT JOIN &quot;OPYM&quot; ON &quot;OCRD&quot;.&quot;PymCode&quot; = &quot;OPYM&quot;.&quot;PayMethCod&quot;.
        /// </summary>
        internal static string GetPaymentInfo_HANA_OK {
            get {
                return ResourceManager.GetString("GetPaymentInfo_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetPaymentMethods_ForBP_HANA_OK
        ///SELECT &quot;PayMethCod&quot;, &quot;Descript&quot;
        ///FROM &quot;CRD2&quot;
        ///LEFT JOIN &quot;OPYM&quot; ON &quot;CRD2&quot;.&quot;PymCode&quot; = &quot;OPYM&quot;.&quot;PayMethCod&quot;
        ///WHERE &quot;Type&quot; = &apos;I&apos; AND &quot;CardCode&quot; = &apos;{0}&apos;.
        /// </summary>
        internal static string GetPaymentMethods_ForBP_HANA_OK {
            get {
                return ResourceManager.GetString("GetPaymentMethods_ForBP_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetPaymentMethods_HANA_OK
        ///SELECT &quot;PayMethCod&quot;, &quot;Descript&quot; FROM &quot;OPYM&quot; WHERE &quot;Type&quot; = &apos;I&apos;.
        /// </summary>
        internal static string GetPaymentMethods_HANA_OK {
            get {
                return ResourceManager.GetString("GetPaymentMethods_HANA_OK", resourceCulture);
            }
        }
    }
}
