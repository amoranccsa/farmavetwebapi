﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPBatchs
    {
        public static List<CRMBatchModel> GetNotReturnedItemsByBatch (SAPbobsCOM.Company oCompany, string itemCode, string cardCode)
        {
            List<CRMBatchModel> batchList = new List<CRMBatchModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string itemCodeFilter = "";
            string cardCodeFilter = "";
            CRMBatchModel tempBatch;


            ErrorHandler.WriteInLog("Getting items by batch delivered and not returned: ItemCode(" + itemCode +
                                    "); CardCode(" + cardCode + ")",
                                    ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by item code
                    if (!string.IsNullOrWhiteSpace(itemCode))
                        itemCodeFilter = string.Format(SAPBatchsQuerys.GetDeliveredNotReturnedItemsByBatch_ItemCodeFilter_HANA_OK, itemCode);
                    //Filtering list by CardCode
                    if (!string.IsNullOrWhiteSpace(cardCode))
                        cardCodeFilter = string.Format(SAPBatchsQuerys.GetDeliveredNotReturnedItemsByBatch_CardCodeFilter_HANA_OK, cardCode);

                    if (!SAPItems.CheckItemManagedByBatch(oCompany, itemCode))
                    {
                        /*batchList.Add(new CRMBatchModel()
                        {
                            Id = null,
                            quantity = 0,
                            itemCode = itemCode
                        });*/
                    }
                    else
                    {
                        sql = string.Format(SAPBatchsQuerys.GetDeliveredNotReturnedItemsByBatch_HANA_OK, itemCodeFilter + cardCodeFilter);

                        recordset.DoQuery(sql);
                        ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                        if (recordset.RecordCount > 0)
                        {
                            recordset.MoveFirst();
                            while (!recordset.EoF)
                            {
                                tempBatch = new CRMBatchModel()
                                {
                                    Id = recordset.Fields.Item("BatchNum").Value.ToString(),
                                    itemCode = recordset.Fields.Item("ItemCode").Value.ToString(),
                                    quantity = Convert.ToInt32(recordset.Fields.Item("QtyDelivered").Value),
                                    expDate = recordset.Fields.Item("ExpDate").Value
                                };
                                batchList.Add(tempBatch);

                                recordset.MoveNext();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    batchList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetNotReturnedItemsByBatch: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return batchList;
        }
    }
}