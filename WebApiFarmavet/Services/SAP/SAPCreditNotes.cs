﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPCreditNotes
    {
        #region Models constructors from recordset *************************************************************************
        private static CRMCreditNoteModel GetCreditNoteModelFromRecordset(SAPbobsCOM.Recordset recordset)
        {
            CRMCreditNoteModel tempModel = new CRMCreditNoteModel();

            tempModel.Id = recordset.Fields.Item("DocEntry").Value.ToString();
            tempModel.docNum = recordset.Fields.Item("DocNum").Value.ToString();
            tempModel.cardCode = recordset.Fields.Item("CardCode").Value;
            tempModel.cardName = recordset.Fields.Item("CardName").Value;
            tempModel.docDate = recordset.Fields.Item("DocDate").Value;
            tempModel.docTotal = recordset.Fields.Item("DocTotal").Value;

            //Código FARMAVET *****************************************************
            if (recordset.Fields.Item("Draft").Value == "Y")
                tempModel.draft = true;
            else
                tempModel.draft = false;

            if (recordset.Fields.Item("DocStatus").Value == "O")
                tempModel.open = true;
            else
                tempModel.open = false;
            //*********************************************************************

            return tempModel;
        }
        
        #endregion ********************************************************************************************************

        #region Get credit note list **************************************************************************************
        /// <summary>
        /// Returns the list of refunds for the sales person ID selected, or all if the code is -2
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson">-2: Returns all the credit notes, -1: Isn't a sales person, returns an empty list, >=0: only his/her credit notes</param>
        /// <param name="creditNoteId">The DocNUM start (not the DocENTRY)</param>
        /// <param name="customer">Customer CardName</param>
        /// <returns></returns>
        public static List<CRMCreditNoteModel> GetCreditNotesList(SAPbobsCOM.Company oCompany, int salesPerson, string creditNoteId, string customer,
                                                                  string dateFrom, string dateTo, bool drafts)
        {
            List<CRMCreditNoteModel> creditNotesList = new List<CRMCreditNoteModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string creditNoteFilter = "";
            string customerFilter = "";
            string dateFilter = "";
            CRMCreditNoteModel tempCreditNote;

            ErrorHandler.WriteInLog("Getting Credit Notes list: SalesPerson(" + salesPerson.ToString() + "); DocNum(" + creditNoteId +
                                    "*); Customer(" + customer + "*)", ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPCreditNotesQuerys.GetCreditNotesList_SalesPersonFilter_HANA_OK, salesPerson);
                    //Filtering list by credit note id
                    if (!string.IsNullOrWhiteSpace(creditNoteId))
                        creditNoteFilter = string.Format(SAPCreditNotesQuerys.GetCreditNotesList_CreditNoteFilter_HANA_OK, creditNoteId);
                    //Filtering list by customer name
                    if (!string.IsNullOrWhiteSpace(customer))
                        customerFilter = string.Format(SAPCreditNotesQuerys.GetCreditNotesList_CustomerFilter_HANA_OK, customer);
                    //Filtering list by dates
                    if (!string.IsNullOrWhiteSpace(dateFrom) && !string.IsNullOrWhiteSpace(dateTo))
                    {
                        dateFilter = " AND " + string.Format(SAPCreditNotesQuerys.GetCreditNotesList_DatesFilter_NO_HANA,
                            DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")),
                            DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));
                    }

                    if (!drafts)
                        sql = SAPCreditNotesQuerys.GetCreditNotesList_HANA_OK + salesPersonFilter + creditNoteFilter + customerFilter + dateFilter;
                    else
                        sql = SAPCreditNotesQuerys.GetCreditNotesDraftsList_HANA_OK + salesPersonFilter + creditNoteFilter + customerFilter + dateFilter;

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempCreditNote = GetCreditNoteModelFromRecordset(recordset);
                            tempCreditNote.draft = drafts;
                            creditNotesList.Add(tempCreditNote);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    creditNotesList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetCreditNotesList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return creditNotesList;
        }


        /// <summary>
        /// Return one page of the refunds list. You can select if you want documents, drafts or both
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson"></param>
        /// <param name="creditNoteId"></param>
        /// <param name="customer"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="documents">set to TRUE if you want the documents on the list</param>
        /// <param name="drafts">set to TRUE if you want the drafts on the list</param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static List<CRMCreditNoteModel> GetCreditNotesPagedList(SAPbobsCOM.Company oCompany, int salesPerson, string creditNoteId, string customer, string dateFrom, string dateTo,
                                                                   bool documents, bool drafts, int page)
        {
            List<CRMCreditNoteModel> creditNotesList = new List<CRMCreditNoteModel>();
            SAPbobsCOM.Recordset recordset;
            string sqlDoc;
            string sqlDrf;
            string sqlUnion;
            string sql;
            string salesPersonFilter = "";
            string creditNoteFilter = "";
            string customerFilter = "";
            string dateFilter = "";
            CRMCreditNoteModel tempCreditNote;

            int nElements;
            int firstRow;
            int lastRow;

            ErrorHandler.WriteInLog("Getting Credit Notes paged list: SalesPerson(" + salesPerson.ToString() + "); DocNum(" + creditNoteId +
                                    "*); Customer(" + customer + "*); Get documents (" + documents + "); get Drafts (" + drafts + "); Page " + page,
                                    ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    //Getting first and last row to fetch
                    if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("PageRows"), out nElements))
                        nElements = 10;

                    firstRow = (page * nElements) + 1; //first row is number 1
                    lastRow = (page * nElements) + nElements;

                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPCreditNotesQuerys.GetCreditNotesList_SalesPersonFilter_HANA_OK, salesPerson);
                    //Filtering list by credit note id
                    if (!string.IsNullOrWhiteSpace(creditNoteId))
                        creditNoteFilter = string.Format(SAPCreditNotesQuerys.GetCreditNotesList_CreditNoteFilter_HANA_OK, creditNoteId);
                    //Filtering list by customer name
                    if (!string.IsNullOrWhiteSpace(customer))
                        customerFilter = string.Format(SAPCreditNotesQuerys.GetCreditNotesList_CustomerFilter_HANA_OK, customer);
                    //Filtering list by dates
                    if (!string.IsNullOrWhiteSpace(dateFrom) && !string.IsNullOrWhiteSpace(dateTo))
                    {
                        /*dateFilter = " AND " + string.Format(SAPCreditNotesQuerys.GetCreditNotes_DatesFilter,
                            DateTime.ParseExact(dateFrom, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture),
                            DateTime.ParseExact(dateTo, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture));*/
                        dateFilter = " AND " + string.Format(SAPCreditNotesQuerys.GetCreditNotesList_DatesFilter_NO_HANA,
                            DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")),
                            DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));
                    }

                    //Creando la SQL componiendo la SQL de documentos y borradores
                    if (documents)
                        sqlDoc = SAPCreditNotesQuerys.GetCreditNotesList_HANA_OK + salesPersonFilter + creditNoteFilter + customerFilter + dateFilter;
                    else
                        sqlDoc = "";

                    if (drafts)
                        sqlDrf = SAPCreditNotesQuerys.GetCreditNotesDraftsList_HANA_OK + salesPersonFilter + creditNoteFilter + customerFilter + dateFilter;
                    else
                        sqlDrf = "";

                    if (documents && drafts)
                        sqlUnion = " UNION ALL ";
                    else
                        sqlUnion = "";

                    sql = string.Format(SAPCreditNotesQuerys.GetCreditNotesDraftsPaged_HANA_OK, sqlDoc, sqlUnion, sqlDrf, firstRow, lastRow);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempCreditNote = GetCreditNoteModelFromRecordset(recordset);
                            creditNotesList.Add(tempCreditNote);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    creditNotesList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetCreditNotesList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return creditNotesList;
        }

        
        #endregion ********************************************************************************************************

    }
}