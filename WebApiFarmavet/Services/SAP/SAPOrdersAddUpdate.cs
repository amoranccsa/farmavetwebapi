﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAPbobsCOM;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPOrdersAddUpdate
    {
        /// <summary>
        /// Create the new lines for the order
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="orderSAP"></param>
        /// <param name="lines"></param>
        /// <param name="isNewDocument"></param>
        public static bool UpdateOrderNewLines(SAPbobsCOM.Company oCompany, SAPbobsCOM.Documents orderSAP, List<CRMOrderLineModel> lines,
                                                bool isNewDocument, bool draft, int salesPersonCode, int claseExpedicion)
        {
            int intLineNum;
            bool firstLine = true;
            bool _created = false;
            SAPbobsCOM.BoObjectTypes docType;


            if ((lines != null) && (lines.Count > 0))
            {
                //We have 3 possibles scenarios
                // Add line: the line doesn't exists on SAP
                // Update line: the line exists on SAP
                // Delete line: quantity is set to 0

                try
                {

                    if (!draft)
                        docType = SAPbobsCOM.BoObjectTypes.oOrders;
                    else
                        docType = SAPbobsCOM.BoObjectTypes.oDrafts;

                    for (int i = 0; i < lines.Count; i++)
                    {
                        //First, try to get the line. If it doesn't exists it's an Add scenario
                        if (string.IsNullOrWhiteSpace(lines[i].Id) || !Int32.TryParse(lines[i].Id, out intLineNum) ||
                            !SAPDocumentsGeneric.LineExists(oCompany, docType, orderSAP.DocEntry, intLineNum))
                        {
                            //Check if Quantity is different to Zero in order to make the line
                            if (lines[i].quantity > 0)
                            {
                                //If it's a new document, we don't need to put an ADD for the first line.
                                //But if the document was loaded, we need an ADD, because we already have the first line loaded
                                if (!isNewDocument || !firstLine)
                                    orderSAP.Lines.Add();

                                orderSAP.Lines.ItemCode = lines[i].itemCode;
                                orderSAP.Lines.ItemDescription = lines[i].description;
                                //orderSAP.Lines.InventoryQuantity = lines[i].quantity;
                                orderSAP.Lines.Quantity = lines[i].quantity;
                                //orderSAP.Lines.UoMEntry = lines[i].uomEntry;
                                orderSAP.Lines.UnitPrice = lines[i].unitPrice;
                                orderSAP.Lines.TaxPercentagePerRow = lines[i].vatPercent;
                                //orderSAP.Lines.LineTotal = lines[i].lineTotal;    //SOPSAP-459 Se quita para que no se redondee el descuento de usuario
                                orderSAP.Lines.ItemDetails = lines[i].text;

                                orderSAP.Lines.SalesPersonCode = salesPersonCode;


                                if (!string.IsNullOrWhiteSpace(lines[i].whsCode))
                                    orderSAP.Lines.WarehouseCode = lines[i].whsCode;


                                orderSAP.Lines.UserFields.Fields.Item("U_camp").Value = lines[i].nombreCampana;
                                if (lines[i].aplicarCampana)
                                    orderSAP.Lines.UserFields.Fields.Item("U_c_camp").Value = "Y";
                                else
                                    orderSAP.Lines.UserFields.Fields.Item("U_c_camp").Value = "N";

                                if (lines[i].dogertyDiscounts.dtoCampana != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_ca").Value = lines[i].dogertyDiscounts.dtoCampana;
                                if (lines[i].dogertyDiscounts.dto1 != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_1").Value = lines[i].dogertyDiscounts.dto1;
                                if (lines[i].dogertyDiscounts.dto2 != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_2").Value = lines[i].dogertyDiscounts.dto2;
                                if (lines[i].dogertyDiscounts.dto3 != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_3").Value = lines[i].dogertyDiscounts.dto3;
                                if (lines[i].dogertyDiscounts.dtoExtra != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_xtra").Value = lines[i].dogertyDiscounts.dtoExtra;
                                if (lines[i].dogertyDiscounts.dto4 != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_4").Value = lines[i].dogertyDiscounts.dto4;
                                if (lines[i].discPrcnt > 0)
                                    orderSAP.Lines.DiscountPercent = lines[i].discPrcnt;

                                if (!string.IsNullOrWhiteSpace(lines[i].saleCode) && !string.IsNullOrWhiteSpace(lines[i].saleName))
                                {
                                    orderSAP.Lines.UserFields.Fields.Item("U_DOI_CPro").Value = lines[i].saleCode;
                                    orderSAP.Lines.UserFields.Fields.Item("U_DOI_NPro").Value = lines[i].saleName;
                                }

                                if (claseExpedicion != -1)
                                    orderSAP.Lines.ShippingMethod = claseExpedicion;

                                firstLine = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION UpdateOrderNewLines: " + ex.Message + Environment.NewLine + ex.StackTrace,
                                            ErrorHandler.ErrorVerboseLevel.None);
                }

                _created = true;
            }

            return _created;
        }


        /// <summary>
        /// Updates the existing lines of the order
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="orderSAP"></param>
        /// <param name="lines"></param>
        public static bool UpdateOrderOldLines(SAPbobsCOM.Company oCompany, SAPbobsCOM.Documents orderSAP, List<CRMOrderLineModel> lines, int salesPersonCode)
        {
            CRMOrderLineModel tempLine;
            bool _updated = false;


            if ((lines != null) && (lines.Count > 0))
            {
                //We have 3 possibles scenarios
                // Add line: the line doesn't exists on SAP
                // Update line: the line exists on SAP
                // Delete line: quantity is set to 0

                //Now we get through all lines of the documents to check if the line must be updated or deleted
                try
                {
                    //In reverse order!!! Because we can delete some lines and change the position of the higher ones.
                    for (int i = orderSAP.Lines.Count - 1; i >= 0; i--)
                    {
                        orderSAP.Lines.SetCurrentLine(i);

                        if (orderSAP.Lines.ItemDescription == "NULL")   //Se borra la línea si es la que se creó para forzar la actualización del DocTotal
                            orderSAP.Lines.Delete();
                        else
                        {

                            tempLine = lines.SingleOrDefault(x => x.Id == orderSAP.Lines.LineNum.ToString());

                            if (tempLine != null)
                            {
                                //Checking if line must be updated or deleted
                                if (tempLine.quantity == 0)
                                {
                                    orderSAP.Lines.Delete();
                                }
                                else
                                {
                                    orderSAP.Lines.ItemCode = tempLine.itemCode;
                                    orderSAP.Lines.ItemDescription = tempLine.description;
                                    //orderSAP.Lines.InventoryQuantity = tempLine.quantity;
                                    orderSAP.Lines.Quantity = tempLine.quantity;
                                    //orderSAP.Lines.UoMEntry = tempLine.uomEntry;
                                    orderSAP.Lines.UnitPrice = tempLine.unitPrice;
                                    orderSAP.Lines.TaxPercentagePerRow = tempLine.vatPercent;
                                    //orderSAP.Lines.LineTotal = tempLine.lineTotal;    //SOPSAP-459 Se quita para que no se redondee el descuento de usuario
                                    orderSAP.Lines.ItemDetails = tempLine.text;

                                    orderSAP.Lines.SalesPersonCode = salesPersonCode;


                                    orderSAP.Lines.UserFields.Fields.Item("U_camp").Value = lines[i].nombreCampana;
                                    if (lines[i].aplicarCampana)
                                        orderSAP.Lines.UserFields.Fields.Item("U_c_camp").Value = "Y";
                                    else
                                        orderSAP.Lines.UserFields.Fields.Item("U_c_camp").Value = "N";

                                    //AMM 20200115 Quitamos estos if y asignamos directamente siempre el valor
                                    //if (lines[i].dogertyDiscounts.dtoCampana != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_ca").Value = lines[i].dogertyDiscounts.dtoCampana;
                                    //if (lines[i].dogertyDiscounts.dto1 != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_1").Value = lines[i].dogertyDiscounts.dto1;
                                    //if (lines[i].dogertyDiscounts.dto2 != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_2").Value = lines[i].dogertyDiscounts.dto2;
                                    //if (lines[i].dogertyDiscounts.dto3 != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_3").Value = lines[i].dogertyDiscounts.dto3;
                                    //if (lines[i].dogertyDiscounts.dtoExtra != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_xtra").Value = lines[i].dogertyDiscounts.dtoExtra;
                                    //if (lines[i].dogertyDiscounts.dto4 != 0)
                                    orderSAP.Lines.UserFields.Fields.Item("U_l_dto_4").Value = lines[i].dogertyDiscounts.dto4;
                                    //if (lines[i].discPrcnt > 0)
                                    orderSAP.Lines.DiscountPercent = lines[i].discPrcnt;


                                    if ((!string.IsNullOrWhiteSpace(tempLine.whsCode)) && (orderSAP.Lines.WarehouseCode != tempLine.whsCode))
                                        orderSAP.Lines.WarehouseCode = tempLine.whsCode;
                                }
                            }
                        }
                    }

                    _updated = true;
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION GetOrderLines: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return _updated;
        }

        /// <summary>
        /// Busca las lineas de tempDeleteList en el pedido, y añade una nueva con valores de quantity y LineTotal negativos
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="orderSAP"></param>
        /// <param name="tempDeleteList"></param>
        /// <param name="tempDataList"></param>
        /// <returns></returns>
        private static bool UpdateOrderDeleteLines(Company oCompany, Documents orderSAP, List<CRMOrderLineModel> tempDeleteList, List<CRMOrderLineModel> tempDataList, int salesPersonCode)
        {
            //int intLineNum;
            //bool firstLine = true;
            //bool _created = false;
            //SAPbobsCOM.BoObjectTypes docType;
            CRMOrderLineModel tempLineDelete;
            CRMOrderLineModel tempLineData;
            bool _updated = true;

            if ((tempDeleteList != null) && (tempDeleteList.Count > 0) && (tempDataList != null) && (tempDataList.Count > 0) && (tempDataList.Count >= tempDeleteList.Count))
            {
                try
                {

                    //In reverse order!!! Because we can delete some lines and change the position of the higher ones.
                    for (int i = orderSAP.Lines.Count - 1; i >= 0; i--)
                    {
                        orderSAP.Lines.SetCurrentLine(i);
                        tempLineDelete = tempDeleteList.SingleOrDefault(x => x.Id == orderSAP.Lines.LineNum.ToString());

                        if (tempLineDelete != null)
                        {
                            tempLineData = tempDataList.SingleOrDefault(x => x.Id == tempLineDelete.Id);
                            if (tempLineData != null)
                            {
                                orderSAP.Lines.Add();

                                orderSAP.Lines.ItemCode = tempLineData.itemCode;
                                orderSAP.Lines.ItemDescription = tempLineData.description;
                                //orderSAP.Lines.InventoryQuantity = -tempLineData.quantity;
                                orderSAP.Lines.Quantity = -tempLineData.quantity;
                                //orderSAP.Lines.UoMEntry = lines[i].uomEntry;
                                orderSAP.Lines.UnitPrice = tempLineData.unitPrice;
                                orderSAP.Lines.TaxPercentagePerRow = tempLineData.vatPercent;
                                orderSAP.Lines.LineTotal = -tempLineData.lineTotal;
                                orderSAP.Lines.ItemDetails = tempLineData.text;

                                orderSAP.Lines.SalesPersonCode = salesPersonCode;


                                if (!string.IsNullOrWhiteSpace(tempLineData.whsCode))
                                    orderSAP.Lines.WarehouseCode = tempLineData.whsCode;

                                if (tempLineData.discPrcnt > 0)
                                    orderSAP.Lines.DiscountPercent = tempLineData.discPrcnt;

                                if (!string.IsNullOrWhiteSpace(tempLineData.saleCode) && !string.IsNullOrWhiteSpace(tempLineData.saleName))
                                {
                                    orderSAP.Lines.UserFields.Fields.Item("U_DOI_CPro").Value = tempLineData.saleCode;
                                    orderSAP.Lines.UserFields.Fields.Item("U_DOI_NPro").Value = tempLineData.saleName;
                                }
                            }
                            else
                            {
                                _updated = false;
                                ErrorHandler.WriteInLog(String.Format("No se ha podido borrar la línea {0} porque no se encuentran sus datos en la BBDD", tempLineDelete.Id) + Environment.NewLine,
                                            ErrorHandler.ErrorVerboseLevel.None);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION UpdateOrderNewLines: " + ex.Message + Environment.NewLine + ex.StackTrace,
                                            ErrorHandler.ErrorVerboseLevel.None);
                    _updated = false;
                }
                
            }

            return _updated;
        }


        /// <summary>
        /// Adds an empty line to force the DocTotal update
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="orderSAP"></param>
        /*public static void AddEmptyLine(Company oCompany, Documents orderSAP, bool isNewDocument)
        {
            if (!isNewDocument)
                orderSAP.Lines.Add();   //Al cargar un pedido, la línea contiene los datos de la primera

            orderSAP.Lines.ItemDescription = "NULL";
            orderSAP.Lines.InventoryQuantity = 0;
            orderSAP.Lines.LineTotal = 0;
            orderSAP.Lines.Quantity = 1;
            
        }*/


        /// <summary>
        /// Updates an existing order, so order.Id must exists
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="order"></param>
        public static string UpdateOrder(SAPbobsCOM.Company oCompany, CRMOrderDetailModel order)
        {
            SAPbobsCOM.Documents orderSAP;
            int docEntry;
            string _error = "";
            int iError;

            //Checking if DocEntry is an int
            if (!Int32.TryParse(order.Id, out docEntry))
                _error = string.Format("-e001|Número de DocEntry no válido ({0})", order.Id);

            //Getting the order by its DocEntry
            if (string.IsNullOrWhiteSpace(_error))
            {

                if (!order.draft)
                    orderSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
                else
                {
                    orderSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                    orderSAP.DocObjectCodeEx = ((int)SAPbobsCOM.BoObjectTypes.oOrders).ToString();
                }

                if (!orderSAP.GetByKey(docEntry))
                    _error = string.Format("-e002|DocEntry no encontrado en SAP ({0})", docEntry.ToString());
                else
                {
                    orderSAP.DocDueDate = order.docDueDate;
                    orderSAP.Comments = order.comments;
                    orderSAP.ShipToCode = order.shipAddress.Id;

                    orderSAP.SalesPersonCode = order.salesPerson;
                    
                    

                    //Condiciones de pago
                    if (orderSAP.PaymentGroupCode != order.paymentTermsId)
                        orderSAP.PaymentGroupCode = order.paymentTermsId;
                    if (orderSAP.PaymentMethod != order.paymentMethodId)
                        orderSAP.PaymentMethod = order.paymentMethodId;
                    

                    if (orderSAP.DiscountPercent != order.discPrcnt)
                        orderSAP.DiscountPercent = order.discPrcnt;

                    //Añadiendo "Utilizar cuenta de mercancías expedidas"
                    //orderSAP.UseShpdGoodsAct = BoYesNoEnum.tYES;

                    //No se modifica el docTotal, dejamos que sea SAP el que lo calcule en función de los LineTotal y el % de descuento
                    /*if (orderSAP.DocTotal != order.docTotal)
                        orderSAP.DocTotal = order.docTotal;*/

                    /*if (order.lines.Any(x => x.quantity == 0))
                        AddEmptyLine(oCompany, orderSAP, false);*/

                    //Adding new lines

                    //CALO 20200909 A petición del cliente, a las líneas hay que añadirles un valor en el campo TrnspCode que se obtiene del maestro de IC
                    string tempClaseExpedicion = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRD", "ShipType", string.Format("\"CardCode\" = '{0}'", order.cardCode));
                    int claseExpedicion = -1;
                    if (!string.IsNullOrWhiteSpace(tempClaseExpedicion))
                        claseExpedicion = int.Parse(tempClaseExpedicion);

                    if (!UpdateOrderNewLines(oCompany, orderSAP, order.lines, false, order.draft, order.salesPerson, claseExpedicion))
                        _error = string.Format("-e003|Error al añadir líneas al pedido ({0})", docEntry.ToString());
                    //Delete lines *************************************************************************************************************************************************************
                    //Debido a un problema con SAP, que no es capaz de actualizar el docTotal de un pedido si no se añaden líneas, la única forma de hacerlo es añadir una copia de las lineas
                    //con un valor negativo, y debe hacerse antes de actualizar las lineas, y con los valores que se guardan en la BBDD
                    /*List<CRMOrderLineModel> tempDeleteList = order.lines.FindAll(x => x.quantity == 0);
                    List<CRMOrderLineModel> tempDataList;
                    if ((tempDeleteList != null) && (tempDeleteList.Count > 0))
                    {
                        tempDataList = SAPOrdersGet.GetOrderLines(oCompany, order.Id, order.draft);
                        if (!UpdateOrderDeleteLines(oCompany, orderSAP, tempDeleteList, tempDataList))
                            _error = _error + string.Format("-e005|Error al borrar las líneas del pedido ({0})", docEntry.ToString());
                    }*/
                    //**************************************************************************************************************************************************************************
                    //Updating old lines
                    if (!UpdateOrderOldLines(oCompany, orderSAP, order.lines, order.salesPerson))
                        _error = _error + string.Format("-e004|Error al actualizar las líneas del pedido ({0})", docEntry.ToString());

                    //AMM 20191030 SOPSAP-440 Al eliminar lineas de un pedido.. el DocTotal no siempre se actualiza correctamente
                    //... parece que los Pedidos va bien, pero en los DRAFT ... no se actualiza el doctotal cuando eliminamos lineas
                    //... probamos forzar el doctotal cuando estamos en draft...
                    //OJO!!!! QUE HACE COSAS RARAS Y PONE DTOS RAROS A PIE DE DOCUMENTO..PERO CREO QUE ES DERIVADO DE SOPSAP-452 RECARGO EQUIVALENCIA (RE , R.E.)
                    if(order.draft)
                    {
                        //ErrorHandler.WriteInLog("ANTES SOPSAP-440 orderSAP.DiscountPercent=" + orderSAP.DiscountPercent.ToString() + " ; orderSAP.DocTotal= " + orderSAP.DocTotal.ToString(), ErrorHandler.ErrorVerboseLevel.None);
                        //ErrorHandler.WriteInLog("ANTES SOPSAP-440 order.docTotal =" + order.docTotal.ToString() + " ; order.docTotal ROUNDED= " + Math.Round(order.docTotal,2).ToString(), ErrorHandler.ErrorVerboseLevel.None);

                        if (orderSAP.DiscountPercent != order.discPrcnt)
                            orderSAP.DiscountPercent = order.discPrcnt;
                        //AMM 20191105 SOPSAP-440 Fruto de liminar
                        if (orderSAP.DocTotal != order.docTotal)
                            orderSAP.DocTotal = Math.Round(order.docTotal, 2);//AMM 20191105 SOPSAP-440 ver si redondeando eliminamos el problema de pequeños %dto a pie documento... order.docTotal;

                        //ErrorHandler.WriteInLog("DESPUES SOPSAP-440 orderSAP.DiscountPercent=" + orderSAP.DiscountPercent.ToString() + " ; orderSAP.DocTotal= " + orderSAP.DocTotal.ToString(), ErrorHandler.ErrorVerboseLevel.None);

                    }

                    //Modificamos el comentario de autentificación
                    if (order.authComments == null)
                        order.authComments = "";
                    orderSAP.UserFields.Fields.Item("U_DOI_APPAUTH").Value = order.authComments;

                    orderSAP.Update();

                    oCompany.GetLastError(out iError, out _error);
                    if (iError != 0)
                    {
                        _error = iError.ToString() + ": " + _error;
                    }

                }
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Order update error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }

        //***********************************************************************************************************

        private static string GetOrderDocNum(SAPbobsCOM.Company oCompany, string docEntry, bool asDraft)
        {
            SAPbobsCOM.Recordset recordset;
            string sql;
            string docNum = "";

            if (!string.IsNullOrWhiteSpace(docEntry) && (oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (!asDraft)
                        sql = string.Format(SAPOrdersQuerys.GetDocumentNumber_HANA_OK, docEntry);
                    else
                        sql = string.Format(SAPOrdersQuerys.GetDocumentDraftNumber_HANA_OK, docEntry);

                    recordset.DoQuery(sql);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        docNum = recordset.Fields.Item("DocNum").Value.ToString();
                        ErrorHandler.WriteInLog("DocNum found: " + docNum, ErrorHandler.ErrorVerboseLevel.Trace);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION GetOrderDocNum: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return docNum;
        }

        /// <summary>
        /// Creates a new order
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public static string AddOrder(SAPbobsCOM.Company oCompany, CRMOrderDetailModel order, bool asDraft)
        {
            SAPbobsCOM.Documents orderSAP;
            //string autoIDSerie;
            //int autoIDSerieInt;
            int empId;
            string _error = "";
            int iError;

            try
            {
                //Getting the order by its DocEntry
                if (order == null)
                {
                    _error = "-e005|El pedido está vacío";
                }
                else
                {
                    if (!asDraft)
                        orderSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
                    else
                    {
                        orderSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                        orderSAP.DocObjectCodeEx = ((int)SAPbobsCOM.BoObjectTypes.oOrders).ToString();
                    }

                    ///TODO: activar esto si se desea que se pueda usar un DocNum diferente a la serie numérica de pedidos
                    /*autoIDSerie = System.Configuration.ConfigurationManager.AppSettings.Get("AppOrderSeries");
                    if (int.TryParse(autoIDSerie, out autoIDSerieInt))
                        orderSAP.Series = autoIDSerieInt;
                    else
                        _error = "-e006|Número de serie no válida.";*/

                    orderSAP.CardCode = order.cardCode;
                    orderSAP.CardName = order.cardName;

                    orderSAP.DocDueDate = order.docDueDate;
                    orderSAP.DocDate = order.docDate;
                    orderSAP.Comments = order.comments;
                    if ((order.shipAddress != null) && (!string.IsNullOrWhiteSpace(order.shipAddress.Id)))
                        orderSAP.ShipToCode = order.shipAddress.Id;

                    orderSAP.SalesPersonCode = order.salesPerson;
                    empId = SAPSalesPerson.GetUserEmployeeId(oCompany, order.salesPerson);
                    if (empId > -1)
                        orderSAP.DocumentsOwner = empId;

                    if (order.discPrcnt != 0)
                        orderSAP.DiscountPercent = order.discPrcnt;

                    if (order.docTotal == 0)
                        orderSAP.DocTotal = 0;

                    //Condiciones de pago
                    orderSAP.PaymentGroupCode = order.paymentTermsId;
                    orderSAP.PaymentMethod = order.paymentMethodId;

                    //Añadiendo "Utilizar cuenta de mercancías expedidas"
                    //orderSAP.UseShpdGoodsAct = BoYesNoEnum.tYES;

                    //Modificamos el comentario de autentificación
                    if (order.authComments == null)
                        order.authComments = "";
                    orderSAP.UserFields.Fields.Item("U_DOI_APPAUTH").Value = order.authComments;
                    orderSAP.UserFields.Fields.Item("U_origenAPP").Value = 1;

                    //Adding new lines

                    //CALO 20200909 A petición del cliente, a las líneas hay que añadirles un valor en el campo TrnspCode que se obtiene del maestro de IC
                    string tempClaseExpedicion = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRD", "ShipType", string.Format("\"CardCode\" = '{0}'", order.cardCode));
                    int claseExpedicion = -1;
                    if (!string.IsNullOrWhiteSpace(tempClaseExpedicion))
                        claseExpedicion = int.Parse(tempClaseExpedicion);

                    if (!UpdateOrderNewLines(oCompany, orderSAP, order.lines, true, asDraft, order.salesPerson, claseExpedicion))
                        _error = "-e003|Error al añadir líneas al nuevo pedido.";

                    if (string.IsNullOrWhiteSpace(_error))
                    {
                        orderSAP.Add();

                        oCompany.GetLastError(out iError, out _error);
                        if (iError != 0)
                        {
                            _error = iError.ToString() + ": " + _error;
                        }
                        else
                        {
                            _error = oCompany.GetNewObjectKey();
                            _error = GetOrderDocNum(oCompany, _error, asDraft);
                        }
                    }
                }

                //Notifing error
                if (!string.IsNullOrWhiteSpace(_error))
                {
                    ErrorHandler.WriteInLog("Order new error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                    _error = _error.Replace("|", ": ");
                }
            } catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION UpdateOrderNewLines: " + ex.Message + Environment.NewLine + ex.StackTrace,
                                            ErrorHandler.ErrorVerboseLevel.None);
                _error = "-e000: Excepción. " + ex.Message;
            }

            return _error;
        }


        /// <summary>
        /// Close an order
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="_order"></param>
        /// <returns></returns>
        public static string DeleteOrder(SAPbobsCOM.Company oCompany, string _order, bool draft)
        {
            SAPbobsCOM.Documents orderSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
            int docEntry;
            string _error = "";
            int iError;

            //Getting the order by its DocEntry
            if (string.IsNullOrWhiteSpace(_order))
            {
                _error = string.Format("-e001|Número de DocEntry no válido ({0})", _order);
            }
            else
            {
                //Checking if DocEntry is an int
                if (!Int32.TryParse(_order, out docEntry))
                    _error = string.Format("-e001|Número de DocEntry no válido ({0})", _order);

                //Getting the order by its DocEntry
                if (string.IsNullOrWhiteSpace(_error))
                {

                    if (!draft)
                        orderSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
                    else
                    {
                        orderSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                        orderSAP.DocObjectCodeEx = ((int)SAPbobsCOM.BoObjectTypes.oOrders).ToString();
                    }

                    if (!orderSAP.GetByKey(docEntry))
                        _error = string.Format("-e002|DocEntry no encontrado en SAP ({0})", docEntry.ToString());
                    else
                    {
                        if (!draft)
                            orderSAP.Close();
                        else
                            orderSAP.Remove();

                        oCompany.GetLastError(out iError, out _error);
                        if (iError != 0)
                        {
                            _error = iError.ToString() + ": " + _error;
                        }

                    }
                }
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Order new error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }
    }
}