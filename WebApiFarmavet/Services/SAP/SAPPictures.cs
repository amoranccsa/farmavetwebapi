﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPPictures
    {
        private static string GetPictureFolder (SAPbobsCOM.Company oCompany)
        {
            return SAPBDGenerics.GetFirstValueFromTable(oCompany, "OADP", "BitmapPath", "");
        }

        public static CRMPictureModel GetPicture (SAPbobsCOM.Company oCompany, string pictureName, int thumbSize)
        {
            string picturePath;
            Image tempImg;
            Image img;
            byte[] pictureBArray = null;
            CRMPictureModel pictureModel;

            try
            {
                picturePath = GetPictureFolder(oCompany);
                if (!string.IsNullOrWhiteSpace(picturePath))
                {
                    if (File.Exists(picturePath + pictureName))
                    {
                        if (thumbSize <= 0)
                            img = Image.FromFile(picturePath + pictureName);
                        else
                        {
                            tempImg = Image.FromFile(picturePath + pictureName);
                            img = GetPictureThumbnail(tempImg, thumbSize);
                        }
                        using (MemoryStream imgStream = new MemoryStream())
                        {
                            img.Save(imgStream, System.Drawing.Imaging.ImageFormat.Png);
                            pictureBArray = imgStream.ToArray();

                            pictureModel = new CRMPictureModel()
                            {
                                Id = pictureName,
                                format = CRMPictureModel.Format.PNG,
                                width = img.Width,
                                height = img.Height,
                                pictureData = pictureBArray
                            };
                        }
                    }
                    else
                    {
                        ErrorHandler.WriteInLog("Error retrieving image: picture path/name does not exists: '" + picturePath + pictureName + "'", ErrorHandler.ErrorVerboseLevel.OnlyError);
                        pictureModel = new CRMPictureModel()
                        {
                            Id = pictureName,
                            format = CRMPictureModel.Format.Unknown,
                            error = "picture path/name does not exists. Ask your administrator."
                        };
                    }
                }
                else
                {
                    ErrorHandler.WriteInLog("Error retrieving image: picture path not set on SAP", ErrorHandler.ErrorVerboseLevel.OnlyError);
                    pictureModel = new CRMPictureModel()
                    {
                        Id = pictureName,
                        format = CRMPictureModel.Format.Unknown,
                        error = "picture path not set on SAP"
                    };
                }
            } catch(Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                pictureModel = new CRMPictureModel()
                {
                    Id = pictureName,
                    format = CRMPictureModel.Format.Unknown,
                    error = ex.Message
                };
            }

            return pictureModel;
        }


        private static Image GetPictureThumbnail(Image tempImg, int thumbSize)
        {
            int newWidth;
            int newHeight;

            if (tempImg.Width > tempImg.Height)
            {
                newWidth = thumbSize;
                newHeight = (tempImg.Height * thumbSize) / tempImg.Width;
            }
            else
            {
                newHeight = thumbSize;
                newWidth = (tempImg.Width * thumbSize) / tempImg.Height;
            }

            return tempImg.GetThumbnailImage(newWidth, newHeight, () => false, IntPtr.Zero);
        }


    }
}