﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPContactPerson
    {
        #region INFO CONVERTERS **********************************************************************************
        private static CRMBPContactModel GetModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            CRMBPContactModel _contact = new CRMBPContactModel();

            _contact.Id = _recordSet.Fields.Item("CntctCode").Value.ToString();
            _contact.Name = _recordSet.Fields.Item("Name").Value;
            _contact.FirstName = _recordSet.Fields.Item("FirstName").Value;
            _contact.MiddleName = _recordSet.Fields.Item("MiddleName").Value;
            _contact.LastName = _recordSet.Fields.Item("LastName").Value;
            _contact.Position = _recordSet.Fields.Item("Position").Value;
            _contact.Phone1 = _recordSet.Fields.Item("Tel1").Value;
            _contact.Phone2 = _recordSet.Fields.Item("Tel2").Value;
            _contact.MPhone = _recordSet.Fields.Item("Cellolar").Value;
            _contact.E_Mail = _recordSet.Fields.Item("E_MailL").Value;

            return _contact;
        }
        #endregion ***********************************************************************************************

        #region Getters ******************************************************************************************
        public static List<CRMBPContactModel> GetContactList(SAPbobsCOM.Company oCompany, string _cardCode)
        {
            List<CRMBPContactModel> contactList = new List<CRMBPContactModel>();
            CRMBPContactModel tempContact;

            try
            {
                SAPbobsCOM.Recordset recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string sql = "SELECT \"CntctCode\", \"Name\", \"FirstName\", \"MiddleName\", \"LastName\", \"Position\", " +
                             "      \"Tel1\", \"Tel2\", \"Cellolar\", \"E_MailL\" " +
                             "FROM \"OCPR\" " +
                             "WHERE \"CardCode\" = '" + _cardCode + "' AND \"Active\" = 'Y'";

                if (!string.IsNullOrWhiteSpace(sql))
                {
                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);

                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempContact = GetModelFromRecordset(oCompany, recordSet);
                            if (tempContact != null)
                                contactList.Add(tempContact);
                            recordSet.MoveNext();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION GetContactList(" + _cardCode + "): " + ex, ErrorHandler.ErrorVerboseLevel.None);
                contactList.Clear();
            }

            return contactList;
        }

        #endregion ***********************************************************************************************

        #region Updaters *****************************************************************************************
        public static string UpdateContactList(SAPbobsCOM.Company oCompany, CRMBPContactListModel list)
        {
            SAPbobsCOM.BusinessPartners accountSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            string _error = "";
            int iError;
            int contactEmployeesCount;
            //bool firstContact = true;
            CRMBPContactModel tempContact;

            try
            {
                //Checking if CardCode is not empty
                if (string.IsNullOrWhiteSpace(list.Id))
                    _error = string.Format("-e001|CardCode no válido ({0})", list.Id);

                //Checking if list is not empty
                if (string.IsNullOrWhiteSpace(_error) && ((list.ContactList == null) || (list.ContactList.Count == 0)))
                    _error = "-e005|La lista de contactos está vacía";

                //Getting the business partner by its CardCode
                if (string.IsNullOrWhiteSpace(_error))
                {
                    if (!accountSAP.GetByKey(list.Id))
                        _error = string.Format("-e002|CardCode no encontrado en SAP ({0})", list.Id);
                    else
                    {
                        /*System.Diagnostics.Debug.WriteLine("*** Number: " + accountSAP.ContactEmployees.Count);
                        System.Diagnostics.Debug.WriteLine("*** Contact: " + accountSAP.ContactEmployees.Name);*/



                        //Updating existing contacts
                        contactEmployeesCount = accountSAP.ContactEmployees.Count;
                        if ((contactEmployeesCount > 0) && (!string.IsNullOrWhiteSpace(accountSAP.ContactEmployees.Name)))
                        {
                            for (int i = 0; i < accountSAP.ContactEmployees.Count; i++)
                            {
                                accountSAP.ContactEmployees.SetCurrentLine(i);
                                if (!string.IsNullOrWhiteSpace(accountSAP.ContactEmployees.Name))
                                {
                                    //Look for the contact person in the edited contact list
                                    tempContact = list.ContactList.Find(x => x.Name == accountSAP.ContactEmployees.Name);
                                    if (tempContact != null)
                                    {
                                        //Check if contact person was marked to delete
                                        if (tempContact.Deleted)
                                        {
                                            //If the main contact person was marked to delete (because other was chosen for that position), he/she must removed before deleting
                                            System.Diagnostics.Debug.WriteLine("**** " + accountSAP.ContactPerson);
                                            if (accountSAP.ContactPerson == tempContact.Name)
                                                accountSAP.ContactPerson = "";
                                            accountSAP.ContactEmployees.Delete();
                                        }
                                        else
                                        {
                                            //Check if contact person was edited
                                            if (tempContact.UpdatedAt != null)
                                            {
                                                accountSAP.ContactEmployees.FirstName = tempContact.FirstName;
                                                if (!string.IsNullOrWhiteSpace(tempContact.MiddleName))
                                                    accountSAP.ContactEmployees.MiddleName = tempContact.MiddleName;
                                                if (!string.IsNullOrWhiteSpace(tempContact.LastName))
                                                    accountSAP.ContactEmployees.LastName = tempContact.LastName;
                                                if (!string.IsNullOrWhiteSpace(tempContact.Position))
                                                    accountSAP.ContactEmployees.Position = tempContact.Position;
                                                if (!string.IsNullOrWhiteSpace(tempContact.Phone1))
                                                    accountSAP.ContactEmployees.Phone1 = tempContact.Phone1;
                                                if (!string.IsNullOrWhiteSpace(tempContact.Phone2))
                                                    accountSAP.ContactEmployees.Phone2 = tempContact.Phone2;
                                                if (!string.IsNullOrWhiteSpace(tempContact.MPhone))
                                                    accountSAP.ContactEmployees.MobilePhone = tempContact.MPhone;
                                                if (!string.IsNullOrWhiteSpace(tempContact.E_Mail))
                                                    accountSAP.ContactEmployees.E_Mail = tempContact.E_Mail;

                                                accountSAP.ContactEmployees.Add();
                                            }
                                        }
                                    }
                                }
                            }
                        }



                        //Adding non existent contacts
                        if (!string.IsNullOrWhiteSpace(accountSAP.ContactEmployees.Name))
                            accountSAP.ContactEmployees.Add();

                        for (int i = 0; i < list.ContactList.Count; i++)
                        {
                            if (!ContactPersonExists(oCompany, list.Id, list.ContactList[i].Name))
                            {
                                accountSAP.ContactEmployees.Name = list.ContactList[i].Name;
                                accountSAP.ContactEmployees.FirstName = list.ContactList[i].FirstName;
                                if (!string.IsNullOrWhiteSpace(list.ContactList[i].MiddleName))
                                    accountSAP.ContactEmployees.MiddleName = list.ContactList[i].MiddleName;
                                if (!string.IsNullOrWhiteSpace(list.ContactList[i].LastName))
                                    accountSAP.ContactEmployees.LastName = list.ContactList[i].LastName;
                                if (!string.IsNullOrWhiteSpace(list.ContactList[i].Position))
                                    accountSAP.ContactEmployees.Position = list.ContactList[i].Position;
                                if (!string.IsNullOrWhiteSpace(list.ContactList[i].Phone1))
                                    accountSAP.ContactEmployees.Phone1 = list.ContactList[i].Phone1;
                                if (!string.IsNullOrWhiteSpace(list.ContactList[i].Phone2))
                                    accountSAP.ContactEmployees.Phone2 = list.ContactList[i].Phone2;
                                if (!string.IsNullOrWhiteSpace(list.ContactList[i].MPhone))
                                    accountSAP.ContactEmployees.MobilePhone = list.ContactList[i].MPhone;
                                if (!string.IsNullOrWhiteSpace(list.ContactList[i].E_Mail))
                                    accountSAP.ContactEmployees.E_Mail = list.ContactList[i].E_Mail;

                                accountSAP.ContactEmployees.Add();
                            }
                        }

                        //Setting the new Main Contact
                        accountSAP.ContactPerson = list.CntctPrsnId;
                        accountSAP.Update();

                        oCompany.GetLastError(out iError, out _error);
                        if (iError != 0)
                        {
                            _error = iError.ToString() + ": " + _error;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Contact person list update error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }

        #endregion ***********************************************************************************************

        #region Other functions **********************************************************************************
        public static bool ContactPersonExists(SAPbobsCOM.Company oCompany, string cardCode, string name)
        {
            bool cpExists = false;

            try
            {
                SAPbobsCOM.Recordset recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string sql = "SELECT \"CntctCode\", \"Name\", \"CardCode\" " +
                             "FROM \"OCPR\" " +
                             "WHERE \"CardCode\" = '" + cardCode + "' AND \"Name\" = '" + name + "'";

                if (!string.IsNullOrWhiteSpace(sql))
                {
                    recordSet.DoQuery(sql);

                    if (recordSet.RecordCount > 0)
                    {
                        cpExists = true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION ContactPersonExists(" + cardCode + ", " + name + "): " + ex, ErrorHandler.ErrorVerboseLevel.None);
            }

            return cpExists;
        }
        #endregion************************************************************************************************
    }
}