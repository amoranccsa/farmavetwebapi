﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public class SAPBusinessPartnersAddUpdate
    {
        #region private methods ******************************************************************************************

        private static string GetCardCodeFromRecordset(Company oCompany, Recordset recordSet)
        {
            string newCardCode = "";
            string _formatString;
            string tempString;

            tempString = recordSet.Fields.Item("BeginStr").Value;
            if (!string.IsNullOrWhiteSpace(tempString))
                newCardCode += tempString;

            _formatString = "D" + recordSet.Fields.Item("NumSize").Value.ToString();
            newCardCode += recordSet.Fields.Item("NextNumber").Value.ToString(_formatString);

            tempString = recordSet.Fields.Item("EndStr").Value;
            if (!string.IsNullOrWhiteSpace(tempString))
                newCardCode += tempString;

            return newCardCode;
        }

        private static string GetNewCardCode(Company oCompany, string bpAutoIDSerie)
        {
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string newCardCode = "";

            ErrorHandler.WriteInLog("Getting new CardCode for a new Business Partner from series " + bpAutoIDSerie,
                                    ErrorHandler.ErrorVerboseLevel.Trace);

            if ((oCompany != null) && (oCompany.Connected))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    sql = "SELECT \"BeginStr\", \"NextNumber\", \"NumSize\", \"EndStr\" " +
                          "FROM \"NNM1\" WHERE \"Series\" = '" + bpAutoIDSerie + "'";

                    recordSet.DoQuery(sql);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        newCardCode = GetCardCodeFromRecordset(oCompany, recordSet);
                    }

                }
                catch (Exception ex)
                {
                    newCardCode = "";
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            else
            {
                newCardCode = "";
            }

            return newCardCode;
        }
        #endregion *******************************************************************************************************


        #region ADD Business Parnters ************************************************************************************
        /// <summary>
        /// Creates a new business partner
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static string AddBusinessPartner(SAPbobsCOM.Company oCompany, CRMAccountModel account)
        {
            SAPbobsCOM.BusinessPartners accountSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            
            string bpAutoIDSerie = "";
            int _serie;
            string _error = "";
            int iError;

            try
            {
                //Getting the order by its DocEntry
                if (account == null)
                {
                    _error = "-e005|El interlocutor comercial está vacío";
                }
                else
                {

                    switch (account.Type)
                    {
                        case CRMSummaryAccountModel.BpType.Customer:
                            accountSAP.CardType = SAPbobsCOM.BoCardTypes.cCustomer;
                            bpAutoIDSerie = System.Configuration.ConfigurationManager.AppSettings.Get("CardCodeCustomerSeries");
                            break;
                        case CRMSummaryAccountModel.BpType.Lead:
                            accountSAP.CardType = SAPbobsCOM.BoCardTypes.cLid;
                            bpAutoIDSerie = System.Configuration.ConfigurationManager.AppSettings.Get("CardCodeLeadSeries");
                            break;
                        case CRMSummaryAccountModel.BpType.Supplier:
                            accountSAP.CardType = SAPbobsCOM.BoCardTypes.cSupplier;
                            bpAutoIDSerie = System.Configuration.ConfigurationManager.AppSettings.Get("CardCodeSupplierSeries");
                            break;
                    }

                    //accountSAP.CardCode = GetNewCardCode(oCompany, bpAutoIDSerie);
                    if (int.TryParse(bpAutoIDSerie, out _serie))
                        accountSAP.Series = _serie;
                    else
                        _error = "-e006|Número de serie no válida.";

                    accountSAP.CardName = account.Company;

                    //If it's new, it don't have contact persons yet. It have to be added fron ContactPerson methods
                    /*if (!string.IsNullOrWhiteSpace(account.CntctPrsnId))
                        accountSAP.ContactPerson = account.CntctPrsnId;*/

                    accountSAP.Phone1 = account.Phone1;
                    accountSAP.Phone2 = account.Phone2;
                    accountSAP.Cellular = account.MPhone;
                    accountSAP.EmailAddress = account.Email;
                    accountSAP.FederalTaxID = account.Nif;
                    accountSAP.Valid = BoYesNoEnum.tYES;    //Is put NO by default!!!

                    if (account.SalesPerson > 0)
                        accountSAP.SalesPersonCode = account.SalesPerson;

                    //Set addresses
                    if ((account.BillAddress != null) && (!string.IsNullOrWhiteSpace(account.BillAddress.Id)))
                    {
                        accountSAP.Addresses.AddressName = account.BillAddress.Id;
                        accountSAP.Addresses.Street = account.BillAddress.Street;
                        accountSAP.Addresses.BuildingFloorRoom = account.BillAddress.Building;
                        accountSAP.Addresses.City = account.BillAddress.City;
                        accountSAP.Addresses.ZipCode = account.BillAddress.ZipCode;
                        accountSAP.Addresses.State = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCST", "Code", string.Format("\"Name\" = '{0}'", account.BillAddress.StateName));
                        if (string.IsNullOrWhiteSpace(accountSAP.Addresses.State))
                            _error = string.Format("-e007|Nombre de provincia {0} no encontrado", account.BillAddress.StateName);
                        accountSAP.Addresses.Country = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRY", "Code", string.Format("\"Name\" = '{0}'", account.BillAddress.CountryName));
                        if (string.IsNullOrWhiteSpace(accountSAP.Addresses.Country))
                            _error = string.Format("-e007|Nombre de país {0} no encontrado", account.BillAddress.CountryName);
                        accountSAP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_BillTo;
                        accountSAP.Addresses.Add();
                    }

                    if ((account.ShippingAddress != null) && (!string.IsNullOrWhiteSpace(account.ShippingAddress.Id)))
                    {
                        accountSAP.Addresses.AddressName = account.ShippingAddress.Id;
                        accountSAP.Addresses.Street = account.ShippingAddress.Street;
                        accountSAP.Addresses.BuildingFloorRoom = account.ShippingAddress.Building;
                        accountSAP.Addresses.City = account.ShippingAddress.City;
                        accountSAP.Addresses.ZipCode = account.ShippingAddress.ZipCode;
                        accountSAP.Addresses.State = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCST", "Code", string.Format("\"Name\" = '{0}'", account.ShippingAddress.StateName));
                        if (string.IsNullOrWhiteSpace(accountSAP.Addresses.State))
                            _error = string.Format("-e007|Nombre de provincia {0} no encontrado", account.ShippingAddress.StateName);
                        accountSAP.Addresses.Country = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRY", "Code", string.Format("\"Name\" = '{0}'", account.ShippingAddress.CountryName));
                        if (string.IsNullOrWhiteSpace(accountSAP.Addresses.Country))
                            _error = string.Format("-e007|Nombre de país {0} no encontrado", account.ShippingAddress.CountryName);
                        accountSAP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
                        accountSAP.Addresses.Add();
                    }

                    //accountSAP.IndustryType = account.Industry;
                    //accountSAP.??? = account.OpportunityPercent;
                    //accountSAP.??? = account.OpportunitySize;

                    //FARMAVET data *************************
                    if (!string.IsNullOrWhiteSpace(account.RouteId))
                        accountSAP.UserFields.Fields.Item("U_DOI_RUTCode").Value = account.RouteId;
                    //***************************************

                    if (string.IsNullOrWhiteSpace(_error))
                    {
                        accountSAP.Add();

                        oCompany.GetLastError(out iError, out _error);
                        if (iError != 0)
                        {
                            _error = iError.ToString() + ": " + _error;
                        }
                        else
                        {
                            _error = oCompany.GetNewObjectKey();
                        }
                    }

                }
            } catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                _error = "-000|Exception: " + ex.Message;
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Business Partner new error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }
        #endregion *******************************************************************************************************


        #region UPDATE Business Parnters *********************************************************************************

        /// <summary>
        /// Updates an existing Business Partner, so account.Id must exists
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="account"></param>
        public static string UpdateBusinessPartner(SAPbobsCOM.Company oCompany, CRMAccountModel account)
        {
            SAPbobsCOM.BusinessPartners accountSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            string _error = "";
            int iError;

            //Checking if CardCode is not empty
            if (string.IsNullOrWhiteSpace(account.Id))
                _error = string.Format("-e001|CardCode no válido ({0})", account.Id);

            //Getting the order by its DocEntry
            if (string.IsNullOrWhiteSpace(_error))
            {
                if (!accountSAP.GetByKey(account.Id))
                    _error = string.Format("-e002|CardCode no encontrado en SAP ({0})", account.Id);
                else
                {
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(account.Company))
                            accountSAP.CardName = account.Company;


                        if (!string.IsNullOrWhiteSpace(account.CntctPrsnId) && SAPContactPerson.ContactPersonExists(oCompany, account.Id, account.CntctPrsnId))
                            accountSAP.ContactPerson = account.CntctPrsnId;

                        if (!string.IsNullOrWhiteSpace(account.Phone1))
                            accountSAP.Phone1 = account.Phone1;
                        if (!string.IsNullOrWhiteSpace(account.Phone2))
                            accountSAP.Phone2 = account.Phone2;
                        if (!string.IsNullOrWhiteSpace(account.MPhone))
                            accountSAP.Cellular = account.MPhone;
                        if (!string.IsNullOrWhiteSpace(account.Email))
                            accountSAP.EmailAddress = account.Email;
                        if (!string.IsNullOrWhiteSpace(account.Nif))
                            accountSAP.FederalTaxID = account.Nif;

                        if ((account.BillAddress != null) && !string.IsNullOrWhiteSpace(account.BillAddress.Id))
                        {
                            if (!string.IsNullOrWhiteSpace(SAPBDGenerics.GetFirstValueFromTable(oCompany, "CRD1", "Address",
                                                            string.Format("\"CardCode\" = '{0}' AND \"AdresType\" = 'B' AND \"Address\" = '{1}'", account.Id, account.BillAddress.Id))))
                                accountSAP.BilltoDefault = account.BillAddress.Id;
                            else
                                _error = string.Format("-e008|ID de dirección de facturación {0} no encontrado", account.BillAddress.Id);
                        }

                        if ((account.ShippingAddress != null) && !string.IsNullOrWhiteSpace(account.ShippingAddress.Id))
                        {
                            if (!string.IsNullOrWhiteSpace(SAPBDGenerics.GetFirstValueFromTable(oCompany, "CRD1", "Address",
                                                            string.Format("\"CardCode\" = '{0}' AND \"AdresType\" = 'S' AND \"Address\" = '{1}'", account.Id, account.ShippingAddress.Id))))
                                accountSAP.ShipToDefault = account.ShippingAddress.Id;
                            else
                                _error = string.Format("-e008|ID de dirección de envío {0} no encontrado", account.ShippingAddress.Id);
                        }

                        //FARMAVET data *************************
                        if (string.IsNullOrWhiteSpace(account.RouteId))
                            accountSAP.UserFields.Fields.Item("U_DOI_RUTCode").Value = "";
                        else
                            accountSAP.UserFields.Fields.Item("U_DOI_RUTCode").Value = account.RouteId;
                        //***************************************

                    } catch (Exception ex)
                    {
                        ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                        _error = "-000|Exception: " + ex.Message;
                    }

                    if (string.IsNullOrWhiteSpace(_error))
                    {
                        accountSAP.Update();

                        oCompany.GetLastError(out iError, out _error);
                        if (iError != 0)
                        {
                            _error = iError.ToString() + ": " + _error;
                        }
                    }

                }
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Business Partner update error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }
        #endregion *******************************************************************************************************


        #region DELETE Business Parnters *********************************************************************************
        private static bool BPHasOrders(SAPbobsCOM.Company oCompany, string _cardCode)
        {
            SAPbobsCOM.Recordset recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string sql;
            bool _hasOrders = false;

            if ((oCompany != null) && (oCompany.Connected) && !string.IsNullOrWhiteSpace(_cardCode))
            {
                try
                {
                    sql = string.Format("SELECT TOP 1 1 FROM \"ORDR\" WHERE \"CardCode\" = '{0}'", _cardCode);
                    recordSet.DoQuery(sql);
                    _hasOrders = recordSet.RecordCount > 0;

                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return _hasOrders;
        }

        /// <summary>
        /// Delete a Business Partner
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="_account"></param>
        /// <returns></returns>
        public static string DeleteBusinessPartner(SAPbobsCOM.Company oCompany, string _account)
        {
            SAPbobsCOM.BusinessPartners accountSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            string _error = "";
            int iError;

            ErrorHandler.WriteInLog(string.Format("Deleting Business Partner '{0}'", _account), ErrorHandler.ErrorVerboseLevel.Trace);
            //Getting the order by its DocEntry
            if (string.IsNullOrWhiteSpace(_account))
            {
                _error = string.Format("-e001|Número de CardCode no válido ({0})", _account);
            }
            else
            {
                //Checking if the Business Partner has done any order
                if (BPHasOrders(oCompany, _account))
                    _error = string.Format("-e007|No se puede eliminar el interlocutor comercial {0} porque tiene pedidos creados.", _account);

                //Getting the order by its DocEntry
                if (string.IsNullOrWhiteSpace(_error))
                {
                    if (!accountSAP.GetByKey(_account))
                        _error = string.Format("-e002|CardCode no encontrado en SAP ({0})", _account);
                    else
                    {

                        accountSAP.Remove();
                        oCompany.GetLastError(out iError, out _error);
                        if (iError != 0)
                        {
                            _error = iError.ToString() + ": " + _error;
                        }

                    }
                }
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Delete Business Partner error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }
        #endregion *******************************************************************************************************

    }
}