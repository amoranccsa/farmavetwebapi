﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPBPPaymentInfo
    {
        #region Business Partner payment info ****************************************************************************

        private static CRMBPPaymentModel GetModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            CRMBPPaymentModel _payment = new CRMBPPaymentModel();

            _payment.Id = _recordSet.Fields.Item("CardCode").Value;
            _payment.CCCBankCode = _recordSet.Fields.Item("BankCode").Value;
            _payment.CCCBranch = _recordSet.Fields.Item("DflBranch").Value;
            _payment.CCCControlKey = _recordSet.Fields.Item("BankCtlKey").Value;
            _payment.CCCAccount = _recordSet.Fields.Item("DflAccount").Value;
            _payment.Iban = _recordSet.Fields.Item("DflIBAN").Value;
            _payment.PaymentGroupId = _recordSet.Fields.Item("GroupNum").Value.ToString();
            _payment.PaymentGroup = _recordSet.Fields.Item("PymntGroup").Value;
            _payment.FreeText = _recordSet.Fields.Item("Free_Text").Value;
            _payment.PaymentWayId = _recordSet.Fields.Item("PymCode").Value;
            _payment.PaymentWay = _recordSet.Fields.Item("Descript").Value;

            _payment.FreeText = _payment.FreeText.Replace("\r", "\n");

            return _payment;
        }


        public static CRMBPPaymentModel GetBPPaymentInfo(SAPbobsCOM.Company oCompany, string _cardCode)
        {
            CRMBPPaymentModel _paymentInfo = null;
            SAPbobsCOM.Recordset recordSet;

            string sql = "";
            string cardCodeFilter;

            if ((oCompany != null) && oCompany.Connected && !string.IsNullOrWhiteSpace(_cardCode))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    cardCodeFilter = string.Format(SAPBPPaymentInfoQuerys.GetPaymentInfo_CardCodeFilter_HANA_OK, _cardCode);
                    sql = SAPBPPaymentInfoQuerys.GetPaymentInfo_HANA_OK + cardCodeFilter;

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        _paymentInfo = GetModelFromRecordset(oCompany, recordSet);
                    }
                    else
                    {
                        _paymentInfo = null;
                        ErrorHandler.WriteInLog("BusinessPartner '" + _cardCode + "' no encontrado.", ErrorHandler.ErrorVerboseLevel.None);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }


            return _paymentInfo;
        }

        #endregion *******************************************************************************************************


        #region Business Partner payment aditional info, other lists ******************************************************

        private static CRMBPPaymentMethodModel GetMethodModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            CRMBPPaymentMethodModel _method = new CRMBPPaymentMethodModel();

            _method.Id = _recordSet.Fields.Item("PayMethCod").Value;
            _method.Description = _recordSet.Fields.Item("Descript").Value;

            return _method;
        }

        public static List<CRMBPPaymentMethodModel> GetPaymentMethods(SAPbobsCOM.Company oCompany, string cardCode)
        {
            List<CRMBPPaymentMethodModel> _methodsList = null;
            SAPbobsCOM.Recordset recordSet;
            CRMBPPaymentMethodModel tempMethod;

            string sql;

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    if (string.IsNullOrWhiteSpace(cardCode))
                        sql = SAPBPPaymentInfoQuerys.GetPaymentMethods_HANA_OK;
                    else
                        sql = string.Format(SAPBPPaymentInfoQuerys.GetPaymentMethods_ForBP_HANA_OK, cardCode);

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        _methodsList = new List<CRMBPPaymentMethodModel>();
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempMethod = GetMethodModelFromRecordset(oCompany, recordSet);
                            _methodsList.Add(tempMethod);
                            recordSet.MoveNext();
                        }
                    }
                    else
                    {
                        _methodsList = null;
                        ErrorHandler.WriteInLog("No se ha encontrado ningún medio de pago.", ErrorHandler.ErrorVerboseLevel.None);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }


            return _methodsList;
        }



        private static CRMBPPaymentConditionModel GetPConditionModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            CRMBPPaymentConditionModel _method = new CRMBPPaymentConditionModel();

            _method.Id = _recordSet.Fields.Item("GroupNum").Value.ToString();
            _method.PymntGroup = _recordSet.Fields.Item("PymntGroup").Value;

            return _method;
        }

        public static List<CRMBPPaymentConditionModel> GetPaymentConditions(SAPbobsCOM.Company oCompany)
        {
            List<CRMBPPaymentConditionModel> _paymentConditionsList = null;
            SAPbobsCOM.Recordset recordSet;
            CRMBPPaymentConditionModel tempPCondition;

            string sql = "";

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    sql = SAPBPPaymentInfoQuerys.GetPaymentConditions_HANA_OK;

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        _paymentConditionsList = new List<CRMBPPaymentConditionModel>();
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempPCondition = GetPConditionModelFromRecordset(oCompany, recordSet);
                            _paymentConditionsList.Add(tempPCondition);
                            recordSet.MoveNext();
                        }
                    }
                    else
                    {
                        _paymentConditionsList = null;
                        ErrorHandler.WriteInLog("No se ha encontrado ninguna condición de pago.", ErrorHandler.ErrorVerboseLevel.None);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }


            return _paymentConditionsList;
        }



        #endregion *******************************************************************************************************

        #region Business Partner payment update info ************************************************************************
        public static string UpdateBPPaymentInfo(SAPbobsCOM.Company oCompany, CRMBPPaymentModel _paymentInfo)
        {
            SAPbobsCOM.BusinessPartners accountSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            string _error = "";
            int iError;

            //Checking if CardCode is not empty
            if (_paymentInfo == null)
                _error = "-e001|CardCode no válido (null)";
            else
            {
                if (string.IsNullOrWhiteSpace(_paymentInfo.Id))
                    _error = string.Format("-e001|CardCode no válido ({0})", _paymentInfo.Id);
            }

            //Getting the order by its DocEntry
            if (string.IsNullOrWhiteSpace(_error))
            {
                if (!accountSAP.GetByKey(_paymentInfo.Id))
                    _error = string.Format("-e002|CardCode no encontrado en SAP ({0})", _paymentInfo.Id);
                else
                {
                    if ((!string.IsNullOrWhiteSpace(_paymentInfo.CCCBankCode)) &&
                        (!string.IsNullOrWhiteSpace(_paymentInfo.CCCBranch)) &&
                        (!string.IsNullOrWhiteSpace(_paymentInfo.CCCControlKey)) &&
                        (!string.IsNullOrWhiteSpace(_paymentInfo.CCCAccount)) &&
                        (!string.IsNullOrWhiteSpace(_paymentInfo.Iban)))
                    {
                        SAPbobsCOM.BPBankAccounts _privBAccount = accountSAP.BPBankAccounts;
                        _privBAccount.BankCode = _paymentInfo.CCCBankCode;
                        _privBAccount.Branch = _paymentInfo.CCCBranch;
                        _privBAccount.ControlKey = _paymentInfo.CCCControlKey;
                        _privBAccount.AccountNo = _paymentInfo.CCCAccount;
                        _privBAccount.IBAN = _paymentInfo.Iban;
                        _privBAccount.Add();
                    }

                    /*if (!string.IsNullOrWhiteSpace(_paymentInfo.CCCBankCode))
                        accountSAP.DefaultBankCode = _paymentInfo.CCCBankCode;
                    if (!string.IsNullOrWhiteSpace(_paymentInfo.CCCBranch))
                        accountSAP.DefaultBranch = _paymentInfo.CCCBranch;*/
                    /*if (!string.IsNullOrWhiteSpace(_paymentInfo.CCCControlKey))
                        accountSAP. = _paymentInfo.CCCControlKey;*/
                    /*if (!string.IsNullOrWhiteSpace(_paymentInfo.CCCAccount))
                        accountSAP.DefaultAccount = _paymentInfo.CCCAccount;
                    if (!string.IsNullOrWhiteSpace(_paymentInfo.Iban))
                        accountSAP.IBAN = _paymentInfo.Iban;*/

                    if (!string.IsNullOrWhiteSpace(_paymentInfo.PaymentGroupId))
                        accountSAP.PayTermsGrpCode = int.Parse(_paymentInfo.PaymentGroupId);
                    if (!string.IsNullOrWhiteSpace(_paymentInfo.FreeText))
                        accountSAP.FreeText = _paymentInfo.FreeText;
                    if (!string.IsNullOrWhiteSpace(_paymentInfo.PaymentWayId))
                    {
                        SAPbobsCOM.BPPaymentMethods _privPMethods = accountSAP.BPPaymentMethods;
                        _privPMethods.PaymentMethodCode = _paymentInfo.PaymentWayId;
                        _privPMethods.Add();
                        accountSAP.PeymentMethodCode = _paymentInfo.PaymentWayId;
                    }

                    accountSAP.Update();

                    oCompany.GetLastError(out iError, out _error);
                    if (iError != 0)
                    {
                        _error = iError.ToString() + ": " + _error;
                    }
                }
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Business Partner payment info update error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }

        #endregion **********************************************************************************************************
    }
}