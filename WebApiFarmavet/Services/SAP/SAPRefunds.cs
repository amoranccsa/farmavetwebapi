﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public class SAPRefunds
    {
        #region Models constructors from recordset *************************************************************************
        private static CRMRefundModel GetRefundModelFromRecordset(SAPbobsCOM.Recordset recordset)
        {
            CRMRefundModel tempModel = new CRMRefundModel();

            tempModel.Id = recordset.Fields.Item("DocEntry").Value.ToString();
            tempModel.docNum = recordset.Fields.Item("DocNum").Value.ToString();
            tempModel.cardCode = recordset.Fields.Item("CardCode").Value;
            tempModel.cardName = recordset.Fields.Item("CardName").Value;
            tempModel.docDate = recordset.Fields.Item("DocDate").Value;
            tempModel.docTotal = recordset.Fields.Item("DocTotal").Value;

            //Código FARMAVET *****************************************************
            if (recordset.Fields.Item("Draft").Value == "Y")
                tempModel.draft = true;
            else
                tempModel.draft = false;

            if (recordset.Fields.Item("DocStatus").Value == "O")
                tempModel.open = true;
            else
                tempModel.open = false;
            //*********************************************************************

            return tempModel;
        }

        private static CRMOrderRefundItemWithBatch GetOrderRefundItemWithBatchModelFromRecordset(SAPbobsCOM.Recordset recordset)
        {
            CRMOrderRefundItemWithBatch tempModel = new CRMOrderRefundItemWithBatch();

            tempModel.Id = recordset.Fields.Item("OrderEntry").Value.ToString();
            tempModel.deliveryEntry = recordset.Fields.Item("DeliveryEntry").Value.ToString();
            tempModel.returnEntry = recordset.Fields.Item("ReturnEntry").Value.ToString();
            tempModel.itemCode = recordset.Fields.Item("ItemCode").Value.ToString();
            tempModel.quantity = Convert.ToInt32(recordset.Fields.Item("Quantity").Value);
            tempModel.batchNum = recordset.Fields.Item("BatchNum").Value;
            tempModel.batchQty = Convert.ToInt32(recordset.Fields.Item("BatchQty").Value);

            return tempModel;
        }
        #endregion ********************************************************************************************************

        #region Get refunds list ******************************************************************************************
        /// <summary>
        /// Returns the list of refunds for the sales person ID selected, or all if the code is -2
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson">-2: Returns all the orders, -1: Isn't a sales person, returns an empty list, >=0: only his/her orders</param>
        /// <param name="refundId">The DocNUM start (not the DocENTRY)</param>
        /// <param name="customer">Customer CardName</param>
        /// <returns></returns>
        public static List<CRMRefundModel> GetRefundsList(SAPbobsCOM.Company oCompany, int salesPerson, string refundId, string customer, string dateFrom, string dateTo, bool drafts)
        {
            List<CRMRefundModel> refundsList = new List<CRMRefundModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string refundFilter = "";
            string customerFilter = "";
            string dateFilter = "";
            CRMRefundModel tempRefund;

            ErrorHandler.WriteInLog("Getting Refunds list: SalesPerson(" + salesPerson.ToString() + "); DocNum(" + refundId +
                                    "*); Customer(" + customer + "*)", ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPRefundsQuerys.GetRefundsList_SalesPersonFilter_HANA_OK, salesPerson);
                    //Filtering list by order id
                    if (!string.IsNullOrWhiteSpace(refundId))
                        refundFilter = string.Format(SAPRefundsQuerys.GetRefundsList_RefundFilter_HANA_OK, refundId);
                    //Filtering list by customer name
                    if (!string.IsNullOrWhiteSpace(customer))
                        customerFilter = string.Format(SAPRefundsQuerys.GetRefundsList_CustomerFilter_HANA_OK, customer);
                    //Filtering list by dates
                    if (!string.IsNullOrWhiteSpace(dateFrom) && !string.IsNullOrWhiteSpace(dateTo))
                    {
                        /*dateFilter = " AND " + string.Format(SAPRefundsQuerys.GetRefunds_DatesFilter,
                            DateTime.ParseExact(dateFrom, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture),
                            DateTime.ParseExact(dateTo, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture));*/
                        dateFilter = " AND " + string.Format(SAPOrdersQuerys.GetOrders_DatesFilter_NO_HANA,
                            DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")),
                            DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));
                    }

                    if (!drafts)
                        sql = SAPRefundsQuerys.GetRefundsList_HANA_OK + salesPersonFilter + refundFilter + customerFilter + dateFilter;
                    else
                        sql = SAPRefundsQuerys.GetRefundsDraftsList_HANA_OK + salesPersonFilter + refundFilter + customerFilter + dateFilter;

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempRefund = GetRefundModelFromRecordset(recordset);
                            tempRefund.draft = drafts;
                            refundsList.Add(tempRefund);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    refundsList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetRefundsList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return refundsList;
        }


        /// <summary>
        /// Return one page of the refunds list. You can select if you want documents, drafts or both
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson"></param>
        /// <param name="refundId"></param>
        /// <param name="customer"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="documents">set to TRUE if you want the documents on the list</param>
        /// <param name="drafts">set to TRUE if you want the drafts on the list</param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static List<CRMRefundModel> GetRefundsPagedList(SAPbobsCOM.Company oCompany, int salesPerson, string refundId, string customer, string dateFrom, string dateTo,
                                                                bool documents, bool drafts, int page)
        {
            List<CRMRefundModel> refundsList = new List<CRMRefundModel>();
            SAPbobsCOM.Recordset recordset;
            string sqlDoc;
            string sqlDrf;
            string sqlUnion;
            string sql;
            string salesPersonFilter = "";
            string refundFilter = "";
            string customerFilter = "";
            string dateFilter = "";
            CRMRefundModel tempRefund;

            int nElements;
            int firstRow;
            int lastRow;

            ErrorHandler.WriteInLog("Getting Refunds paged list: SalesPerson(" + salesPerson.ToString() + "); DocNum(" + refundId +
                                    "*); Customer(" + customer + "*); Get documents (" + documents + "); get Drafts (" + drafts + "); Page " + page,
                                    ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    //Getting first and last row to fetch
                    if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("PageRows"), out nElements))
                        nElements = 10;

                    firstRow = (page * nElements) + 1; //first row is number 1
                    lastRow = (page * nElements) + nElements;

                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPRefundsQuerys.GetRefundsList_SalesPersonFilter_HANA_OK, salesPerson);
                    //Filtering list by order id
                    if (!string.IsNullOrWhiteSpace(refundId))
                        refundFilter = string.Format(SAPRefundsQuerys.GetRefundsList_RefundFilter_HANA_OK, refundId);
                    //Filtering list by customer name
                    if (!string.IsNullOrWhiteSpace(customer))
                        customerFilter = string.Format(SAPRefundsQuerys.GetRefundsList_CustomerFilter_HANA_OK, customer);
                    //Filtering list by dates
                    if (!string.IsNullOrWhiteSpace(dateFrom) && !string.IsNullOrWhiteSpace(dateTo))
                    {
                        /*dateFilter = " AND " + string.Format(SAPRefundsQuerys.GetRefunds_DatesFilter,
                            DateTime.ParseExact(dateFrom, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture),
                            DateTime.ParseExact(dateTo, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture));*/
                        dateFilter = " AND " + string.Format(SAPOrdersQuerys.GetOrders_DatesFilter_NO_HANA,
                            DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")),
                            DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));
                    }

                    //Creando la SQL componiendo la SQL de documentos y borradores
                    if (documents)
                        sqlDoc = SAPRefundsQuerys.GetRefundsList_HANA_OK + salesPersonFilter + refundFilter + customerFilter + dateFilter;
                    else
                        sqlDoc = "";

                    if (drafts)
                        sqlDrf = SAPRefundsQuerys.GetRefundsDraftsList_HANA_OK + salesPersonFilter + refundFilter + customerFilter + dateFilter;
                    else
                        sqlDrf = "";

                    if (documents && drafts)
                        sqlUnion = " UNION ALL ";
                    else
                        sqlUnion = "";

                    sql = string.Format(SAPRefundsQuerys.GetRefundsDraftsPaged_HANA_OK, sqlDoc, sqlUnion, sqlDrf, firstRow, lastRow);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempRefund = GetRefundModelFromRecordset(recordset);
                            refundsList.Add(tempRefund);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    refundsList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetRefundsList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return refundsList;
        }


        /// <summary>
        /// Returns all the items returned from an ORDER, with the Delivery entry, Return entry and BATCH of the item
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="orderEntry"></param>
        /// <returns></returns>
        public static List<CRMOrderRefundItemWithBatch> GetOrderRefundsWithBatchs (SAPbobsCOM.Company oCompany, string orderEntry)
        {
            List<CRMOrderRefundItemWithBatch> refundsList = new List<CRMOrderRefundItemWithBatch>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            CRMOrderRefundItemWithBatch tempRefund;

            ErrorHandler.WriteInLog("Getting Refunds list for order: " + orderEntry, ErrorHandler.ErrorVerboseLevel.Trace);
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    
                    sql = string.Format(SAPRefundsQuerys.GetOrderRefundsWithBatchs_HANA_OK, orderEntry);
                    ErrorHandler.WriteInLog("SAPRefundsQuerys.GetOrderRefundsWithBatchs  " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191127 WriteSQL log
                
                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempRefund = GetOrderRefundItemWithBatchModelFromRecordset(recordset);
                            refundsList.Add(tempRefund);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    refundsList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetRefundsList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return refundsList;
        }

        #endregion ********************************************************************************************************


        #region UPDATE Refund *********************************************************************************************
        /// <summary>
        /// Create the new lines for the order
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="RefundSAP"></param>
        /// <param name="lines"></param>
        /// <param name="isNewDocument"></param>
        public static bool UpdateRefundNewLines(SAPbobsCOM.Company oCompany, SAPbobsCOM.Documents RefundSAP, List<CRMRefundLineModel> lines,
                                                bool isNewDocument, bool isDraft, int salesPersonCode)
        {
            int intLineNum;
            bool firstLine = true;
            bool _created = false;

            SAPbobsCOM.Documents tempDelivery = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
            double quantityToBatch;
            int lineFromBatch = 0;

            try
            {
                if ((lines != null) && (lines.Count > 0))
                {
                    //We have 3 possibles scenarios
                    // Add line: the line doesn't exists on SAP
                    // Update line: the line exists on SAP
                    // Delete line: quantity is set to 0

                    for (int i = 0; i < lines.Count; i++)
                    {
                        //First, try to get the line. If it doesn't exists it's an Add scenario
                        if (string.IsNullOrWhiteSpace(lines[i].Id) || !Int32.TryParse(lines[i].Id, out intLineNum) ||
                            !SAPDocumentsGeneric.LineExists(oCompany, SAPbobsCOM.BoObjectTypes.oOrders, RefundSAP.DocEntry, intLineNum))
                        {
                            //Security check
                            if (lines[i].quantity > 0)
                            {
                                //If it's a new document, we don't need to put an ADD for the first line.
                                //But if the document was loaded, we need an ADD, because we already have the first line loaded
                                if (!isNewDocument || !firstLine)
                                    RefundSAP.Lines.Add();

                                RefundSAP.Lines.ItemCode = lines[i].itemCode;
                                //RefundSAP.Lines.ItemDescription = lines[i].itemName;    //Dejamos que SAP ponga el nombre (además, el campo no existe en las líneas). No confundir con descripción del artículo.
                                //RefundSAP.Lines.InventoryQuantity = lines[i].quantity;
                                RefundSAP.Lines.Quantity = lines[i].quantity;
                                //RefundSAP.Lines.UoMEntry = lines[i].invUomEntry;
                                RefundSAP.Lines.UnitPrice = lines[i].unitPrice;
                                RefundSAP.Lines.TaxPercentagePerRow = lines[i].vatPercent;
                                //RefundSAP.Lines.LineTotal = lines[i].lineTotal;
                                RefundSAP.Lines.ItemDetails = lines[i].text;

                                RefundSAP.Lines.SalesPersonCode = salesPersonCode;
                                RefundSAP.Lines.DiscountPercent = lines[i].discount;

                                /*System.Diagnostics.Debug.WriteLine(string.Format("**** LineTotal: {0}€ x {1} x {2}% = {3}",
                                    lines[i].unitPrice, lines[i].quantity, lines[i].vatPercent, lines[i].lineTotal));*/

                                //Check if line is based on a Delivery
                                if (!string.IsNullOrWhiteSpace(lines[i].baseRef))
                                {
                                    RefundSAP.Lines.BaseEntry = Int32.Parse(lines[i].baseRef);
                                    RefundSAP.Lines.BaseLine = lines[i].baseLine;

                                    RefundSAP.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oDeliveryNotes;

                                    //Check if base Delivery is loaded
                                    if ((tempDelivery == null) || (tempDelivery.DocEntry != Int32.Parse(lines[i].baseRef)))
                                        if (!tempDelivery.GetByKey(Int32.Parse(lines[i].baseRef)))
                                            throw new Exception("-e008|No se puede añadir una línea de devolución a un documento si no hay un pedido previo");

                                    //tempDelivery.Lines.SetCurrentLine(lines[i].baseLine);
                                    if (!SAPDocumentsGeneric.SetDocumentLine(tempDelivery, lines[i].baseLine))
                                        throw new Exception("-e010|No se puede añadir una línea de devolución. El número de línea no existe en la entrega.");

                                    //Checking if the item is managed by batchs
                                    if ((lines[i].batchList == null) || (lines[i].batchList.Count == 0))
                                    {
                                        //NOT managed by batch

                                        //Adding items to Batchs, from the first origin batch line to the last
                                        if (SAPItems.CheckItemManagedByBatch(oCompany, lines[i].itemCode))
                                        {
                                            quantityToBatch = lines[i].quantity;
                                            lineFromBatch = 0;
                                            while (quantityToBatch > 0)
                                            {
                                                //Set the batch origin line
                                                tempDelivery.Lines.BatchNumbers.SetCurrentLine(lineFromBatch);
                                                if (string.IsNullOrWhiteSpace(tempDelivery.Lines.BatchNumbers.BatchNumber))
                                                    throw new Exception(string.Format("-e009|Batch number de pedido, para devolución, no válido: {0}",
                                                                                tempDelivery.Lines.BatchNumbers.BatchNumber));
                                                //Adding data
                                                RefundSAP.Lines.BatchNumbers.BatchNumber = tempDelivery.Lines.BatchNumbers.BatchNumber;
                                                RefundSAP.Lines.BatchNumbers.Quantity = Math.Min(quantityToBatch, tempDelivery.Lines.BatchNumbers.Quantity);
                                                //Uncounting items batched
                                                quantityToBatch -= tempDelivery.Lines.BatchNumbers.Quantity;
                                                //Set new line
                                                RefundSAP.Lines.BatchNumbers.Add();
                                                //Look for next batch origin line
                                                lineFromBatch++;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //Managed by batchs
                                        //Adding Batchs (it's a new line, so no batchs were made)
                                        foreach (var item in lines[i].batchList)
                                        {
                                            RefundSAP.Lines.BatchNumbers.BatchNumber = item.Id;
                                            RefundSAP.Lines.BatchNumbers.Quantity = item.selQty;
                                            //Set new line
                                            RefundSAP.Lines.BatchNumbers.Add();
                                        }
                                    }

                                }
                                else
                                {
                                    //WARNING: if line is not for a draft, it needs to set a batch.
                                    if (!isDraft)
                                        throw new Exception("-e008|No se puede añadir una línea de devolución a un documento si no hay un pedido previo");

                                    //Checking if the item is managed by batchs
                                    if ((lines[i].batchList != null) && (lines[i].batchList.Count > 0))
                                    {
                                        //Managed by batchs
                                        //Adding Batchs (it's a new line, so no batchs were made)
                                        foreach (var item in lines[i].batchList)
                                        {
                                            if (item.selQty > 0)
                                            {
                                                RefundSAP.Lines.BatchNumbers.BatchNumber = item.Id;
                                                RefundSAP.Lines.BatchNumbers.Quantity = item.selQty;
                                                //Set new line
                                                RefundSAP.Lines.BatchNumbers.Add();
                                            }
                                        }
                                    }
                                }

                                firstLine = false;
                            }
                        }
                    }

                    _created = true;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog(" ** EXCEPCIÓN Al añadir lineas a devolución!" + ex.Message + Environment.NewLine +
                    ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                _created = false;
            }

            return _created;
        }

        #endregion ********************************************************************************************************

        #region ADD Refund ************************************************************************************************
        private static string GetRefundDocNum(SAPbobsCOM.Company oCompany, string docEntry, bool asDraft)
        {
            SAPbobsCOM.Recordset recordset;
            string sql;
            string docNum = "";

            if (!string.IsNullOrWhiteSpace(docEntry) && (oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (!asDraft)
                        sql = string.Format(SAPRefundsQuerys.GetRefundDocNum_HANA_OK, docEntry);
                    else
                        sql = string.Format(SAPRefundsQuerys.GetRefundDraftDocNum_HANA_OK, docEntry);

                    recordset.DoQuery(sql);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        docNum = recordset.Fields.Item("DocNum").Value.ToString();
                        ErrorHandler.WriteInLog("DocNum found: " + docNum, ErrorHandler.ErrorVerboseLevel.Trace);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION GetRefundDocNum: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return docNum;
        }

        /// <summary>
        /// Creates a new Refund
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="refund"></param>
        /// <returns></returns>
        public static string AddRefund(SAPbobsCOM.Company oCompany, CRMRefundDetailModel refund, bool asDraft)
        {
            SAPbobsCOM.Documents returnSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oReturns);
            string _error = "";
            int iError;

            //Getting the order by its DocEntry
            if (refund == null)
            {
                _error = "-e005|La devolución está vacía";
            }
            else
            {
                if (!asDraft)
                    returnSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oReturns);
                else
                {
                    returnSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                    returnSAP.DocObjectCodeEx = ((int)SAPbobsCOM.BoObjectTypes.oReturns).ToString();
                }

                returnSAP.CardCode = refund.cardCode;
                returnSAP.CardName = refund.cardName;
                returnSAP.SalesPersonCode = refund.slpCode;

                returnSAP.DocDate = refund.docDate;
                //returnSAP.UserFields.Fields.Item("U_CDev").Value = refund.reasonId;
                returnSAP.Comments = refund.comments;

                //Adding new lines
                if (!UpdateRefundNewLines(oCompany, returnSAP, refund.lines, true, asDraft, refund.slpCode))
                    _error = "-e003|Error al añadir líneas a la nueva devolución.";

                if (string.IsNullOrWhiteSpace(_error))
                {
                    returnSAP.Add();

                    oCompany.GetLastError(out iError, out _error);
                    if (iError != 0)
                    {
                        _error = iError.ToString() + ": " + _error;
                    }
                    else
                    {
                        _error = oCompany.GetNewObjectKey();
                        _error = GetRefundDocNum(oCompany, _error, asDraft);
                    }
                }

            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Refund new error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }
        #endregion ********************************************************************************************************
    }
}