﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPRoutes
    {
        #region Models constructors from recordset *************************************************************************
        private static CRMBaseModel GetRouteFromRecordset(SAPbobsCOM.Recordset recordset)
        {
            CRMBaseModel tempModel = new CRMBaseModel();

            tempModel.Id = recordset.Fields.Item("Code").Value.ToString();
            tempModel.Name = recordset.Fields.Item("Name").Value.ToString();
            //tempModel.NCustomers = recordset.Fields.Item("CardCode").Value;

            return tempModel;
        }
        #endregion ********************************************************************************************************

        #region Get Routes list *******************************************************************************************
        public static List<CRMBaseModel> GetRoutesList(SAPbobsCOM.Company oCompany)
        {
            SAPbobsCOM.Recordset recordSet;
            string sql;
            List<CRMBaseModel> routesList = new List<CRMBaseModel>();
            CRMBaseModel tempRoute;
            
            if ((oCompany != null) && oCompany.Connected)
            {
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                sql = string.Format("SELECT \"Code\", \"Name\" FROM \"@DOI_ORUT\"");

                recordSet.DoQuery(sql);
                ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                if (recordSet.RecordCount > 0)
                {
                    recordSet.MoveFirst();
                    while (!recordSet.EoF)
                    {
                        tempRoute = GetRouteFromRecordset(recordSet);
                        if (tempRoute != null)
                            routesList.Add(tempRoute);
                        recordSet.MoveNext();
                    }

                }
            }

            return routesList;
        }

        #endregion ********************************************************************************************************
    }
}