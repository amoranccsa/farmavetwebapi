﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPItems
    {
        public enum ItemPriceType { SAPItemPrice, LastSellingPrice }

        #region Models constructors from recordsets ***********************************************************************
        private static CRMItemModel GetItemModelFromRecordset(SAPbobsCOM.Recordset recordSet, bool addPicture, bool addCardCodeDiscount, bool aplyUoMMultiplier)
        {
            CRMItemModel tempModel = new CRMItemModel();

            tempModel.Id = recordSet.Fields.Item("ItemCode").Value.ToString();
            tempModel.itemName = recordSet.Fields.Item("ItemName").Value;
            tempModel.description = recordSet.Fields.Item("UserText").Value;//.Replace("\r", "\n"); //AMM 20191030 SOPSAP-471 Para poner retornos de carro y que visualmente se vea en la APP
            tempModel.price = recordSet.Fields.Item("Price").Value;
            tempModel.price = Math.Round(tempModel.price, 2, MidpointRounding.AwayFromZero);    //Redondeamos a 2 decimales, como SAP
            tempModel.vatPercent = recordSet.Fields.Item("Rate").Value;
            tempModel.avaiable = recordSet.Fields.Item("OnHand").Value;
            tempModel.invUnitMsrId = recordSet.Fields.Item("IUoMEntry").Value;
            tempModel.invUnitMsr = recordSet.Fields.Item("IUomCode").Value;
            tempModel.salesUnitMsrId = recordSet.Fields.Item("SUoMEntry").Value;
            tempModel.salesUnitMsr = recordSet.Fields.Item("SUomCode").Value;
            if (addPicture)
            {
                tempModel.pictureFile = recordSet.Fields.Item("PicturName").Value;
                /*if (!string.IsNullOrWhiteSpace(pictureID))
                    tempModel.pictureFile = SAPBDGenerics.GetFirstValueFromTable()*/
            }

            //Código Farmavet **************************************************
            tempModel.groupId = recordSet.Fields.Item("ItmsGrpCod").Value.ToString();
            tempModel.groupName = recordSet.Fields.Item("ItmsGrpNam").Value;

            if (addCardCodeDiscount)
            {
                tempModel.customerDscnt = recordSet.Fields.Item("Discount").Value;
            }

            //AMM 20191114 OBSOLETO!!!
            //if (aplyUoMMultiplier)
            //{
            //    tempModel.UoMMultiplier = recordSet.Fields.Item("IUomMultipl").Value;
            //    tempModel.price = tempModel.price * tempModel.UoMMultiplier;
            //}
            //******************************************************************

            return tempModel;
        }
        #endregion ********************************************************************************************************

        public static List<CRMItemModel> GetItemsList(SAPbobsCOM.Company oCompany, string itemName, string specie, string principio1, string principio2, string principio3, string itemGroup, string itemCode, int _itemPriceType)
        {
            List<CRMItemModel> itemList = new List<CRMItemModel>();
            SAPbobsCOM.Recordset recordSet;
            CRMItemModel tempItem;

            string sql = "";
            string itemNameFilter = "";
            string principio1Filter = "";
            string principio2Filter = "";
            string principio3Filter = "";
            string specieFilter = "";
            string itemGroupFilter = "";
            string itemCodeFilter = "";
            string orderFilter;
            ItemPriceType itemPriceType = (ItemPriceType)Enum.ToObject(typeof(ItemPriceType), _itemPriceType);

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    if (!string.IsNullOrWhiteSpace(itemName))
                        itemNameFilter = string.Format(SAPItemsQuerys.GetItemList_ItemNameFilter, itemName);
                    if (!string.IsNullOrWhiteSpace(specie))
                        specieFilter = string.Format(SAPItemsQuerys.GetItemList_SpecieFilter, specie);
                    if (!string.IsNullOrWhiteSpace(principio1))
                        principio1Filter = string.Format(SAPItemsQuerys.GetItemList_PrinAcFilter1, principio1);
                    if (!string.IsNullOrWhiteSpace(principio2))
                        principio2Filter = string.Format(SAPItemsQuerys.GetItemList_PrinAcFilter2, principio2);
                    if (!string.IsNullOrWhiteSpace(principio3))
                        principio3Filter = string.Format(SAPItemsQuerys.GetItemList_PrinAcFilter3, principio3);
                    if (!string.IsNullOrWhiteSpace(itemGroup))
                        itemGroupFilter = string.Format(SAPItemsQuerys.GetItemList_ItemGroupFilter, itemGroup);
                    if (!string.IsNullOrWhiteSpace(itemCode))
                        itemCodeFilter = string.Format(SAPItemsQuerys.GetItemList_ItemCodeFilter, itemCode);
                    orderFilter = SAPItemsQuerys.GetItemList_OrderFilter;

                    switch (itemPriceType)
                    {
                        case ItemPriceType.LastSellingPrice:
                            //sql = string.Format(SAPItemsQuerys.GetItemList_LastSellingPrice, cardCode) + itemNameFilter + orderFilter;
                            break;
                        default:
                            //sql = SAPItemsQuerys.GetItemsList + itemNameFilter + orderFilter;
                            /*sql = SAPItemsQuerys.GetItemList_Farmavet_old + itemNameFilter + specieFilter + 
                                                 principio1Filter + principio2Filter + principio3Filter + orderFilter;*/
                            sql = string.Format(SAPItemsQuerys.GetItemList_Farmavet,
                                                itemNameFilter + specieFilter + principio1Filter + principio2Filter + principio3Filter + itemGroupFilter + itemCodeFilter,
                                                System.Configuration.ConfigurationManager.AppSettings.Get("SellPriceList"))
                                  + orderFilter;
                            break;
                    }



                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempItem = GetItemModelFromRecordset(recordSet, true, false, false);
                            itemList.Add(tempItem);
                            recordSet.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    itemList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return itemList;
        }


        /// <summary>
        /// Gets part of the item list, upon the number of elements per page and the requested page
        /// </summary>
        /// <param name="oCompany">SAP connection</param>
        /// <param name="itemName">Filter Item Name, if any</param>
        /// <param name="specie">Filter by animal Specie, if any</param>
        /// <param name="principio1">Filter by principle, if any</param>
        /// <param name="principio2">Filter by principle, if any</param>
        /// <param name="principio3">Filter by principle, if any</param>
        /// <param name="itemGroup">Filter by Item Group, if any</param>
        /// <param name="itemCode">Filter by Item Code, if any</param>
        /// <param name="_itemPriceType">Set if price comes from SAP price list or last selled</param>
        /// <param name="page">Number of the page to load</param>
        /// <param name="cardCode">If any, adds item price discount for that customer</param>
        /// <returns></returns>
        public static List<CRMItemModel> GetPartialItemsList(SAPbobsCOM.Company oCompany, string itemName, string specie, string principio1, string principio2, string principio3,
                                                             string itemGroup, string itemCode, int _itemPriceType, int page, string cardCode, bool withSales)
        {
            List<CRMItemModel> itemList = new List<CRMItemModel>();
            SAPbobsCOM.Recordset recordSet;
            CRMItemModel tempItem;
            List<CRMBaseModel> salesList;

            string sql = "";
            string itemNameFilter = "";
            string principio1Filter = "";
            string principio2Filter = "";
            string principio3Filter = "";
            string specieFilter = "";
            string itemGroupFilter = "";
            string itemCodeFilter = "";
            string pageFilter = "";
            string orderFilter;
            ItemPriceType itemPriceType = (ItemPriceType)Enum.ToObject(typeof(ItemPriceType), _itemPriceType);
            string cardCodeFilter = "";

            int firstRow;
            int lastRow;
            int pageRows;

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    if (!string.IsNullOrWhiteSpace(itemName))
                        itemNameFilter = string.Format(SAPItemsQuerys.GetItemList_ItemNameFilter, itemName);
                    if (!string.IsNullOrWhiteSpace(specie))
                        specieFilter = string.Format(SAPItemsQuerys.GetItemList_SpecieFilter, specie);
                    if (!string.IsNullOrWhiteSpace(principio1))
                        principio1Filter = string.Format(SAPItemsQuerys.GetItemList_PrinAcFilter1, principio1);
                    if (!string.IsNullOrWhiteSpace(principio2))
                        principio2Filter = string.Format(SAPItemsQuerys.GetItemList_PrinAcFilter2, principio2);
                    if (!string.IsNullOrWhiteSpace(principio3))
                        principio3Filter = string.Format(SAPItemsQuerys.GetItemList_PrinAcFilter3, principio3);
                    if (!string.IsNullOrWhiteSpace(itemGroup))
                        itemGroupFilter = string.Format(SAPItemsQuerys.GetItemList_ItemGroupFilter, itemGroup);
                    if (!string.IsNullOrWhiteSpace(itemCode))
                        itemCodeFilter = string.Format(SAPItemsQuerys.GetItemList_ItemCodeFilter, itemCode);
                    if (page >= 0)
                    {
                        pageRows = int.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("PageRows"));
                        firstRow = (page * pageRows) + 1;
                        lastRow = firstRow + pageRows - 1;
                        pageFilter = string.Format(SAPItemsQuerys.GetPartialItemList_PageFilter, firstRow, lastRow);
                    }
                    orderFilter = SAPItemsQuerys.GetItemList_OrderFilter;
                    if (!string.IsNullOrWhiteSpace(cardCode))
                        cardCodeFilter = string.Format(SAPItemsQuerys.GetPartialItemList_CardCodeFilter, cardCode);


                    switch (itemPriceType)
                    {
                        case ItemPriceType.LastSellingPrice:
                            //sql = string.Format(SAPItemsQuerys.GetItemList_LastSellingPrice, cardCode) + itemNameFilter + orderFilter;
                            break;
                        default:
                            if (string.IsNullOrWhiteSpace(cardCode))
                            {
                                //sql = SAPItemsQuerys.GetItemsList + itemNameFilter + orderFilter;
                                /*sql = SAPItemsQuerys.GetItemList_Farmavet_old + itemNameFilter + specieFilter + 
                                                     principio1Filter + principio2Filter + principio3Filter + orderFilter;*/
                                sql = string.Format(SAPItemsQuerys.GetPartialItemList_Farmavet,
                                                    itemNameFilter + specieFilter + principio1Filter + principio2Filter + principio3Filter + itemGroupFilter + itemCodeFilter,
                                                    System.Configuration.ConfigurationManager.AppSettings.Get("SellPriceList"),
                                                    orderFilter)
                                      + pageFilter;
                            }
                            else
                            {
                                //Primero se obtiene el ID de la lista de precios del cliente
                                string bpPriceList = SAPBDGenerics.GetFirstValueFromTable(oCompany, "OCRD", "ListNum", string.Format("\"CardCode\" = '{0}' ", cardCode));
                                //Luego se añade a la sentencia parcial que selecciona entre la lista de precios en OSPP (siempre que no sea nula o 0) o la del cliente
                                bpPriceList = string.Format(SAPItemsQuerys.GetPartialItemList_CstmrDscnt_Farmavet_PriceList, bpPriceList);
                                //Y finalmente generamos la query completa
                                sql = string.Format(SAPItemsQuerys.GetPartialItemList_CstmrDscnt_Farmavet,
                                                    itemNameFilter + specieFilter + principio1Filter + principio2Filter + principio3Filter + itemGroupFilter + itemCodeFilter,
                                                    //string.Format(SAPItemsQuerys.GetPartialItemList_GetCardCodePriceList, cardCode),
                                                    bpPriceList,
                                                    cardCodeFilter,
                                                    orderFilter)
                                      + pageFilter;
                            }
                            break;
                    }




                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempItem = GetItemModelFromRecordset(recordSet, true, !string.IsNullOrWhiteSpace(cardCode), true);

                            if (withSales)
                            {
                                salesList = SAPSalesData.GetSummarizedSalesList(oCompany, tempItem.Id);
                                if (salesList != null)
                                    tempItem.nSales = salesList.Count;
                            }

                            itemList.Add(tempItem);
                            recordSet.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    itemList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return itemList;
        }




        public static CRMItemModel GetItemDetails(SAPbobsCOM.Company oCompany, string itemCode, string priceList, string userCode)
        {
            SAPbobsCOM.Recordset recordSet;
            CRMItemModel tempItem = null;

            string sql = "";
            
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);


                    //sql = string.Format(SAPItemsQuerys.GetItemDetails, itemCode, System.Configuration.ConfigurationManager.AppSettings.Get("SellPriceList"));
                    sql = string.Format(SAPItemsQuerys.GetItemDetails, itemCode, priceList);
                    ErrorHandler.WriteInLog("SAPItemsQuerys.GetItemDetails " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191115 WriteSQL log para intentar localizar APP CRASH

                    recordSet.DoQuery(sql);
                    if (!recordSet.EoF)
                    {
                        recordSet.MoveFirst();
                        tempItem = GetItemModelFromRecordset(recordSet, true, false, true);
                        tempItem.pricesList = GetItemPricesList(oCompany, itemCode, userCode);
                        tempItem.defaultPriceListId = System.Configuration.ConfigurationManager.AppSettings.Get("SellPriceList");

                        //AMM 20191114 OBSOLETO!!!
                        //if ((tempItem.UoMMultiplier != 1.0) && (tempItem.pricesList != null) && (tempItem.pricesList.Count > 0))
                        //{
                        //    for (int i = 0; i < tempItem.pricesList.Count; i++)
                        //        tempItem.pricesList[i].price = tempItem.pricesList[i].price * tempItem.UoMMultiplier;
                        //}
                    }
                }
                catch (Exception ex)
                {
                    tempItem = new CRMItemModel();
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            else
            {
                tempItem = new CRMItemModel();
            }

            return tempItem;
        }


        /// <summary>
        /// Get if an item is managed by batch
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="itemCode"></param>
        /// <returns></returns>
        public static bool CheckItemManagedByBatch (SAPbobsCOM.Company oCompany, string itemCode)
        {
            SAPbobsCOM.Recordset recordSet;
            bool useBatch = false;

            string sql = "";

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    sql = string.Format(SAPItemsQuerys.IsItemMangedByBatch, itemCode);

                    recordSet.DoQuery(sql);
                    if (!recordSet.EoF)
                    {
                        recordSet.MoveFirst();
                        if (recordSet.Fields.Item("ManBtchNum").Value.ToString() == "Y")
                            useBatch = true;
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            
            return useBatch;
        }


        #region Stock ***********************************************************************************************
        private static CRMStockModel GetItemStockFromRecordset (SAPbobsCOM.Recordset recordSet)
        {
            CRMStockModel tempModel = new CRMStockModel();

            tempModel.Id = recordSet.Fields.Item("ItemCode").Value.ToString();
            tempModel.Name = recordSet.Fields.Item("ItemName").Value;
            tempModel.WhsCode = recordSet.Fields.Item("WhsCode").Value;
            tempModel.WhsName = recordSet.Fields.Item("WhsName").Value;
            tempModel.Stock = recordSet.Fields.Item("OnHand").Value;
            tempModel.Reserved = recordSet.Fields.Item("IsCommited").Value;
            tempModel.IUoMEntry = recordSet.Fields.Item("IUoMEntry").Value;
            tempModel.IUoMName = recordSet.Fields.Item("IUoMCode").Value;

            return tempModel;
        }


        public static List<CRMStockModel> GetItemStock(SAPbobsCOM.Company oCompany, string itemCode)
        {
            List<CRMStockModel> stockList = new List<CRMStockModel>();
            SAPbobsCOM.Recordset recordSet;
            CRMStockModel tempStock;

            string sql = "";
            string itemCodeFilter = "";

            if ((oCompany != null) && oCompany.Connected && !string.IsNullOrWhiteSpace(itemCode))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    itemCodeFilter = string.Format(SAPItemsQuerys.GetItemStock_ItemCodeFilter, itemCode);

                    sql = SAPItemsQuerys.GetItemStock + itemCodeFilter;

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempStock = GetItemStockFromRecordset(recordSet);
                            stockList.Add(tempStock);
                            recordSet.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    stockList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return stockList;
        }

        #endregion **************************************************************************************************

        #region Item groups *****************************************************************************************
        public static List<CRMBaseModel> GetItemGroupsList (SAPbobsCOM.Company oCompany)
        {
            List<CRMBaseModel> itemGroupsList = new List<CRMBaseModel>();
            SAPbobsCOM.Recordset recordSet;
            CRMBaseModel tempGroup;

            string sql = "";
            
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    sql = SAPItemsQuerys.GetItemGroups;

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempGroup = new CRMBaseModel();
                            tempGroup.Id = recordSet.Fields.Item("ItmsGrpCod").Value.ToString();
                            tempGroup.Name = recordSet.Fields.Item("ItmsGrpNam").Value.ToString();

                            itemGroupsList.Add(tempGroup);
                            recordSet.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    itemGroupsList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return itemGroupsList;
        }
        #endregion **************************************************************************************************

        #region Item prices functions **********************************************************************************************************
        public static List<CRMItemListPrice> GetItemPricesList (SAPbobsCOM.Company oCompany, string itemCode, string userCode)
        {
            SAPbobsCOM.Recordset recordSet;
            List<CRMItemListPrice> pricesList = new List<CRMItemListPrice>();
            CRMItemListPrice tempPrice;

            string sql = "";
            string whiteList = "";
            string blackList = "";

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Getting filters data
                    SAPUserData.GetPricesLists(oCompany, userCode, out whiteList, out blackList);

                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    if (!string.IsNullOrEmpty(whiteList))
                    {
                        whiteList = string.Format(SAPItemsQuerys.GetPricesForItem_WhiteListFilter, whiteList);
                        blackList = "";
                    }
                    else
                    {
                        whiteList = "";

                        if (!string.IsNullOrEmpty(blackList))
                            blackList = string.Format(SAPItemsQuerys.GetPricesForItem_BlackListFilter, blackList);
                        else
                            blackList = "";
                    }

                    sql = string.Format(SAPItemsQuerys.GetPricesForItem + whiteList + blackList, itemCode);

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempPrice = new CRMItemListPrice();
                            tempPrice.priceList = recordSet.Fields.Item("PriceList").Value.ToString();
                            tempPrice.listName = recordSet.Fields.Item("ListName").Value;
                            tempPrice.price = recordSet.Fields.Item("Price").Value;

                            pricesList.Add(tempPrice);
                            recordSet.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return pricesList;
        }


        public static double GetItemPrice(SAPbobsCOM.Company oCompany, string cardCode, string itemCode, double quantity, string docDueDate)
        {
            SAPbobsCOM.SBObob oBob = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);
            SAPbobsCOM.Recordset recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            double itemPriceGet;
            DateTime tempDocDueDate = DateTime.ParseExact(docDueDate, WebApiConfig.AppDateFormat, System.Globalization.CultureInfo.InvariantCulture);

            recordSet = oBob.GetItemPrice(cardCode, itemCode, quantity, tempDocDueDate);
            itemPriceGet = recordSet.Fields.Item("Price").Value;

            return itemPriceGet;
        }
        #endregion *****************************************************************************************************************************


        #region Items discounts functions ******************************************************************************************************

        #region Models constructors from recordsets ***********************************************************************
        private static CRMItemDiscountModel GetDiscountModelFromRecordset(SAPbobsCOM.Recordset recordSet)
        {
            CRMItemDiscountModel tempModel = new CRMItemDiscountModel();

            tempModel.itemCode = recordSet.Fields.Item("ItemCode").Value;
            tempModel.itemName = recordSet.Fields.Item("ItemName").Value;
            tempModel.cardCode = recordSet.Fields.Item("CardCode").Value;
            tempModel.listNum = recordSet.Fields.Item("ListNum").Value;
            tempModel.listName = recordSet.Fields.Item("ListName").Value;
            tempModel.finalDisc = recordSet.Fields.Item("FinalDisc").Value;

            tempModel.dto = recordSet.Fields.Item("Dto").Value;
            tempModel.price = recordSet.Fields.Item("Price").Value;

            tempModel.fromDate = recordSet.Fields.Item("FromDate").Value;
            tempModel.toDate = recordSet.Fields.Item("ToDate").Value;
            tempModel.dtoP = recordSet.Fields.Item("DtoP").Value;
            tempModel.priceP = recordSet.Fields.Item("PriceP").Value;

            tempModel.amount = recordSet.Fields.Item("Amount").Value;
            tempModel.dtoPQ = recordSet.Fields.Item("DtoPQ").Value;
            tempModel.pricePQ = recordSet.Fields.Item("PricePQ").Value;

            return tempModel;
        }
        #endregion ********************************************************************************************************


        /// <summary>
        /// Devuelve una lista de todos los descuentos definidos para un artículo, según una lista de precios y, opcionalmente, el cliente, fecha y cantidad, dentro de la tabla de precios
        /// especiales (tabla de descuentos por periodo y cantidad)
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="priceList"></param>
        /// <param name="itemCode"></param>
        /// <param name="cardCode"></param>
        /// <param name="date"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static List<CRMItemDiscountModel> GetItemDiscountList (SAPbobsCOM.Company oCompany, int priceList, string itemCode, string cardCode = "", string date = "", double amount = 0)
        {
            SAPbobsCOM.Recordset recordSet;
            List<CRMItemDiscountModel> discountList = new List<CRMItemDiscountModel>();
            CRMItemDiscountModel tempDiscount;

            string sql = "";
            string priceListFilter = "";
            string itemCodeFilter = "";
            string cardCodeFilter = "";
            string dateFilter = "";
            string amountFilter = "";

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    priceListFilter = string.Format(SAPItemsQuerys.GetDiscountsForPeriodAmmount_PriceListFilter, priceList);
                    itemCodeFilter = string.Format(SAPItemsQuerys.GetDiscountsForPeriodAmmount_ItemCodeFilter, itemCode);
                    if (!string.IsNullOrWhiteSpace(cardCode))
                        cardCodeFilter = string.Format(SAPItemsQuerys.GetDiscountsForPeriodAmmount_CardCodeFilter, cardCode, priceList);
                    if (!string.IsNullOrWhiteSpace(date))
                        dateFilter = string.Format(SAPItemsQuerys.GetDiscountsForPeriodAmmount_DateFilter,
                            DateTime.ParseExact(date, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));

                    if (amount > 0)
                        amountFilter = string.Format(SAPItemsQuerys.GetDiscountsForPeriodAmmount_AmountFilter, amount.ToString());

                    //AMM 20191105 SOPSAP-421 Accidentalmente detectamos bug de la SQL de OSPP ... el S2.Amount en el WHERE no gusta... mejor como parte del LEFT JOIN SPP2
                    //sql = SAPItemsQuerys.GetDiscountsForPeriodAmmount + priceListFilter + itemCodeFilter + cardCodeFilter + dateFilter + amountFilter;
                    sql = string.Format( SAPItemsQuerys.GetDiscountsForPeriodAmmount + priceListFilter + itemCodeFilter + cardCodeFilter + dateFilter , amountFilter);

                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            tempDiscount = GetDiscountModelFromRecordset(recordSet);

                            discountList.Add(tempDiscount);
                            recordSet.MoveNext();
                        }
                    }
                  
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return discountList;
        }


        /// <summary>
        /// Intenta obtener un único valor porcentual del descuento que debe aplicarse a un artículo según una lista de precios, aportando opcionalmente, los datos de cliente, fecha y cantidad.
        /// En caso de que no encuentre ningún dato relevante en dicha tabla, calculará el descuento en función del precio unitario del artículo, y la función GetItemPrice.
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="priceList"></param>
        /// <param name="itemCode"></param>
        /// <param name="cardCode"></param>
        /// <param name="date"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static double GetItemDiscount (SAPbobsCOM.Company oCompany, int priceList, string itemCode, string cardCode = "", string date = "", double amount = 0)
        {
            List<CRMItemDiscountModel> discountList = GetItemDiscountList(oCompany, priceList, itemCode, cardCode, date, amount);
            double discount = 0;
            double itemPrice = 0;
            double itemDiscountPrice = 0;
            string tempPrice;
            CRMBaseModel itemDisc;

            //Primero intentamos obtener el descuento de la tabla de precios por (cliente, ) periodo y cantidad
            if ((discountList != null) && (discountList.Count > 0))
            {
                if (discountList.Count > 1)
                    discountList = discountList.OrderByDescending(x => x.amount).ToList();

                discount = discountList[0].finalDisc;
                
            }
            else
            {

                //AMM 20191104 SOPSAP-421 APP - Pedidos: valores de descuento de promociones inconsistentes
                //OJO! Si en OSPP no encontramos nada.. buscar en OEDG!!! (pprevio a la "maldita" funcion " sbobob.GetItemPrice que se llama cuando no encontramos descuentos via SQLs
                bool bHaveDiscountOEDG = false;
                double auxDiscount = 1; //primero vamos multiplicando las cascadas...
                double currentDiscount = 0;
                string fechaFormateada = DateTime.ParseExact(date, WebApiConfig.AppDateFormat,
                                        System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"));

                string sql = string.Format(SAPItemsQuerys.SOPSAP_421_GetDisccountPercent_002, itemCode, cardCode, fechaFormateada);
                SAPbobsCOM.Recordset recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                recordSet.DoQuery(sql);
                ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                if (recordSet.RecordCount > 0)
                {
                    bHaveDiscountOEDG = true;
                    recordSet.MoveFirst();
                    while (!recordSet.EoF)
                    {
                        currentDiscount = recordSet.Fields.Item("Discount").Value;
                        auxDiscount = auxDiscount *( 1 - currentDiscount / 100); //Formula matematica de dtos en cascada DRE = 1 - ((1-Dto1)*(1-Dto2)*...*(1-Dton)) 
                        recordSet.MoveNext();
                    }
                }

                auxDiscount = 1 - auxDiscount;
                //AMM 20191104 FIN SOPSAP-421 


                if (bHaveDiscountOEDG)
                {
                    discount = auxDiscount*100;//AMM 20191104 SOPSAP-421 ajusarlo para que llegue "bonito" a la APP
                }
                else
                {
                    //Si no funciona, intentamos calcular el descuento a través del precio del artículo
                    itemDiscountPrice = GetItemPrice(oCompany, cardCode, itemCode, amount, date);
                    if (itemDiscountPrice > 0)
                    {
                        tempPrice = SAPBDGenerics.GetFirstValueFromTable(oCompany, "ITM1", "Price", string.Format("\"itemCode\" = '{0}' AND \"PriceList\" = '{1}'", itemCode, priceList));
                        if (double.TryParse(tempPrice, out itemPrice))
                            discount = 100 - (itemDiscountPrice * 100 / itemPrice);
                        else
                            discount = 0;
                    }
                    else
                    {
                        itemDisc = new CRMBaseModel() { Id = itemCode, Name = "0" };
                        discount = 0;
                    }
                }
            }

            return discount;
        }


        /// <summary>
        /// Obtiene los descuentos, usando las funciones SQL generadas por Farmavet, para los diversos campos de descuento que ellos aplican
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="itemCode"></param>
        /// <param name="cardCode"></param>
        /// <param name="date"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static CRMDogertyItemDiscounts GetFarmavetItemDiscounts(SAPbobsCOM.Company oCompany, string cardCode, string itemCode, bool aplicaCampana, double amount, string date)
        {
            CRMDogertyItemDiscounts descuentos = new CRMDogertyItemDiscounts();
            string sqlAplicaCampana = aplicaCampana ? "Y" : "N";
            string fechaFormateada = DateTime.ParseExact(date, WebApiConfig.AppDateFormat,
                                                         System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"));
            string sql;
            SAPbobsCOM.Recordset recordset;

            try
            {
                //Primero intentamos obtener el descuento de campaña
                sql = string.Format(SAPItemsQuerys.Dogerty_GetDtoCamapana, cardCode, itemCode, sqlAplicaCampana, amount, fechaFormateada);
                recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                recordset.DoQuery(sql);
                recordset.MoveFirst();

                if (!recordset.EoF)
                    descuentos.dtoCampana = recordset.Fields.Item("dtoCampana").Value;

                //Si hay campaña, calculamos el descuento que falta
                if (Math.Round(descuentos.dtoCampana, 2) != 0)
                {
                    sql = string.Format(SAPItemsQuerys.Dogerty_GetDtosConCampana, cardCode, itemCode, fechaFormateada);
                    recordset.DoQuery(sql);
                    recordset.MoveFirst();
                    if (!recordset.EoF)
                        descuentos.dto4 = recordset.Fields.Item("dto4Dogerty").Value;
                }
                else
                {
                    sql = string.Format(SAPItemsQuerys.Dogerty_GetDtosSinCampana, cardCode, itemCode, fechaFormateada);
                    recordset.DoQuery(sql);
                    recordset.MoveFirst();
                    if (!recordset.EoF)
                    {
                        descuentos.dto1 = recordset.Fields.Item("dto1").Value;
                        descuentos.dto2 = recordset.Fields.Item("dto2").Value;
                        descuentos.dto3 = recordset.Fields.Item("dto3").Value;
                        descuentos.dto4 = recordset.Fields.Item("dto4Dogerty").Value;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return descuentos;
        }



        public static List<CRMBaseModel> GetFarmavetItemCampanas(SAPbobsCOM.Company oCompany, string cardCode, string itemCode, string date)
        {
            List<CRMBaseModel> campanas = new List<CRMBaseModel>();
            string fechaFormateada = DateTime.ParseExact(date, WebApiConfig.AppDateFormat,
                                                         System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"));
            string sql;
            SAPbobsCOM.Recordset recordset;

            try
            {
                //Primero intentamos obtener el descuento de campaña
                sql = string.Format(SAPItemsQuerys.Dogerty_GetCampanas, cardCode, itemCode, fechaFormateada);
                recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                recordset.DoQuery(sql);
                ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);

                if (recordset.RecordCount > 0)
                {
                    recordset.MoveFirst();
                    while (!recordset.EoF)
                    {
                        campanas.Add(new CRMBaseModel()
                        {
                            Id = recordset.Fields.Item("cant_min").Value.ToString(),
                            Name = recordset.Fields.Item("dto").Value.ToString()
                        });
                        recordset.MoveNext();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return campanas;
        }
        #endregion *****************************************************************************************************************************


        #region Recargo equivalencia (RE , R.E.)

        /// <summary>
        /// Para obtener el % de recargo de equivalencia. Si el IC tiene marcado "Recargo de equivalencia, habrá que ver su % RE
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="itemCode">Artículo (ver si Grupo Impositivo para ver si dicho grupo tiene RE vigente en la fecha señalada)</param>
        /// <param name="cardCode">Cliente (ver si tiene marcado RE)</param>
        /// <param name="date">Fecha (ver el posible %RE a aplicar en dicha fecha)</param>
        /// <returns>0: si el IC NO tiene marcado RE; En caso que IC tenga RE, se mirará el Grupo Impositivo del artículo y vemo que posible RE tiene en la fecha señalada</returns>
        public static double GetItemRE(SAPbobsCOM.Company oCompany,  string itemCode, string cardCode , string date )
        {
            //AMM 20191030 SOPSAP-452 Recargo equivalencia

            //AMM 20191030 OJO!! SOPSAP-541 DICE QUE EL %GRUPO IMPOSITIVO DEPENDE DEL IC TAMBIEN..CON LO QUE...llegado el momento...habrá que "corregir" la SQL llamada "GetItemRE"!!
            SAPbobsCOM.Recordset recordSet;
            string dateFilter = "" ;
            string sql = "";
            string IC_WithRE = "N"; //valores "Y"/"N"
            double PrcntRE = 0; //Por defectopartimo de que NO tiene Recargo de equivalencia
            try
            {
                //Initializing objects
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                dateFilter = DateTime.ParseExact(date, WebApiConfig.AppDateFormat,
                                            System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"));

               sql = string.Format(SAPItemsQuerys.GetItemRE, itemCode,cardCode, dateFilter);
             
 
                recordSet.DoQuery(sql);
                ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                if (recordSet.RecordCount > 0)
                {
                    recordSet.MoveFirst();
                    if (!recordSet.EoF)
                    {
                        IC_WithRE = recordSet.Fields.Item("Equ").Value; //Ver si tiene recargo de equivalencia
                        if (IC_WithRE == "Y")
                        {
                            PrcntRE = recordSet.Fields.Item("EquVatPr").Value;
                        }                   
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return PrcntRE;
        
        }
        #endregion


    }
}