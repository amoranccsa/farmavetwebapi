﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApiFarmavet.Services.SAP {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class SAPRefundsQuerys {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SAPRefundsQuerys() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WebApiFarmavet.Services.SAP.SAPRefundsQuerys", typeof(SAPRefundsQuerys).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetOrderRefundsWithBatchs_HANA_OK
        ///SELECT &quot;DLN1&quot;.&quot;BaseEntry&quot; AS &quot;OrderEntry&quot;, &quot;DLN1&quot;.&quot;DocEntry&quot; AS &quot;DeliveryEntry&quot;,
        ///       &quot;RDN1&quot;.&quot;DocEntry&quot; AS &quot;ReturnEntry&quot;, &quot;RDN1&quot;.&quot;ItemCode&quot;, &quot;RDN1&quot;.&quot;Quantity&quot; ,
        ///	   ABS(&quot;T3&quot;.&quot;Quantity&quot;) AS &quot;BatchQty&quot;,
        ///       &quot;OBTN&quot;.&quot;DistNumber&quot; AS &quot;BatchNum&quot;
        ///FROM &quot;DLN1&quot;
        ///RIGHT JOIN &quot;RDN1&quot; ON &quot;DLN1&quot;.&quot;DocEntry&quot; = &quot;RDN1&quot;.&quot;BaseEntry&quot; AND &quot;RDN1&quot;.&quot;BaseType&quot; = &apos;15&apos; and DLN1.&quot;LineNum&quot; = RDN1.&quot;BaseLine&quot;
        ///INNER JOIN (SELECT &quot;OITL&quot;.&quot;DocEntry&quot;, &quot;OITL&quot;.&quot;DocLine&quot;, &quot;ITL1&quot;.&quot;SysNumber&quot;, &quot;ITL1&quot;.&quot;Qua [resto de la cadena truncado]&quot;;.
        /// </summary>
        internal static string GetOrderRefundsWithBatchs_HANA_OK {
            get {
                return ResourceManager.GetString("GetOrderRefundsWithBatchs_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetRefundDocNum
        ///SELECT &quot;DocNum&quot; FROM &quot;ORDN&quot; WHERE &quot;DocEntry&quot; = &apos;{0}&apos;.
        /// </summary>
        internal static string GetRefundDocNum_HANA_OK {
            get {
                return ResourceManager.GetString("GetRefundDocNum_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetRefundDraftDocNum_HANA_OK
        ///SELECT &quot;DocNum&quot; FROM &quot;ODRF&quot; WHERE &quot;DocEntry&quot; = &apos;{0}&apos;.
        /// </summary>
        internal static string GetRefundDraftDocNum_HANA_OK {
            get {
                return ResourceManager.GetString("GetRefundDraftDocNum_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetRefundsDraftsList_HANA_OK
        ///SELECT &quot;DocEntry&quot;, &quot;DocNum&quot;, &quot;CardCode&quot;, &quot;CardName&quot;, &quot;DocDate&quot;, &quot;DocTotal&quot;, &quot;DocStatus&quot;,
        ///	&apos;Y&apos; AS &quot;Draft&quot; FROM &quot;ODRF&quot;
        ///WHERE &quot;CANCELED&quot; = &apos;N&apos; AND &quot;ObjType&quot; = &apos;16&apos;.
        /// </summary>
        internal static string GetRefundsDraftsList_HANA_OK {
            get {
                return ResourceManager.GetString("GetRefundsDraftsList_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetRefundsDraftsPaged_HANA_OK
        ///SELECT * FROM (
        ///	SELECT ROW_NUMBER() OVER(ORDER BY &quot;DocDate&quot; DESC, &quot;DocNum&quot; DESC) AS &quot;RowNum&quot;, &quot;OList&quot;.*
        ///	FROM({0} {1} {2}) AS &quot;OList&quot;
        ///) as &quot;sql&quot;
        ///WHERE &quot;sql&quot;.&quot;RowNum&quot; BETWEEN &apos;{3}&apos; AND &apos;{4}&apos;.
        /// </summary>
        internal static string GetRefundsDraftsPaged_HANA_OK {
            get {
                return ResourceManager.GetString("GetRefundsDraftsPaged_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a  AND &quot;CardName&quot; LIKE &apos;%{0}%&apos; .
        /// </summary>
        internal static string GetRefundsList_CustomerFilter_HANA_OK {
            get {
                return ResourceManager.GetString("GetRefundsList_CustomerFilter_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a --GetRefundsList_HANA_OK
        ///SELECT &quot;DocEntry&quot;, &quot;DocNum&quot;, &quot;CardCode&quot;, &quot;CardName&quot;, &quot;DocDate&quot;, &quot;DocTotal&quot;, &quot;DocStatus&quot;,
        ///	&apos;N&apos; AS &quot;Draft&quot; FROM &quot;ORDN&quot;
        ///WHERE &quot;CANCELED&quot; = &apos;N&apos;.
        /// </summary>
        internal static string GetRefundsList_HANA_OK {
            get {
                return ResourceManager.GetString("GetRefundsList_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a AND &quot;DocNum&quot; LIKE &apos;%{0}%&apos;.
        /// </summary>
        internal static string GetRefundsList_RefundFilter_HANA_OK {
            get {
                return ResourceManager.GetString("GetRefundsList_RefundFilter_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a AND &quot;SlpCode&quot; = &apos;{0}&apos;.
        /// </summary>
        internal static string GetRefundsList_SalesPersonFilter_HANA_OK {
            get {
                return ResourceManager.GetString("GetRefundsList_SalesPersonFilter_HANA_OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a SELECT &quot;Code&quot;, &quot;Name&quot; FROM &quot;@DOI_TDEV&quot;.
        /// </summary>
        internal static string NO_SE_USA_GetRefundReasonList {
            get {
                return ResourceManager.GetString("NO_SE_USA_GetRefundReasonList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a (&quot;DocDueDate&quot; &gt;= &apos;{0}&apos; AND &quot;DocDueDate&quot; &lt;= &apos;{1}&apos;).
        /// </summary>
        internal static string NO_SE_USA_GetRefunds_DatesFilter {
            get {
                return ResourceManager.GetString("NO_SE_USA_GetRefunds_DatesFilter", resourceCulture);
            }
        }
    }
}
