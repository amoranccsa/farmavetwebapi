﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPSalesData
    {

        #region Models constructors from recordsets ***********************************************************************
        private static CRMBaseModel GetSummarizedModelFromRecordset(SAPbobsCOM.Recordset recordset, bool v)
        {
            CRMBaseModel tempModel = new CRMBaseModel();

            tempModel.Id = recordset.Fields.Item("Code").Value.ToString();
            tempModel.Name = recordset.Fields.Item("Name").Value.ToString();

            return tempModel;
        }

        private static CRMSaleModel GetModelFromRecordset (SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset recordset)
        {
            CRMSaleModel saleInfo = new CRMSaleModel();
            string tempString;

            saleInfo.Id = recordset.Fields.Item("Code").Value.ToString();
            saleInfo.Name = recordset.Fields.Item("Name").Value.ToString();
            switch (recordset.Fields.Item("U_DOI_Rea").Value.ToString())
            {
                case "1":
                    saleInfo.maker = (int)CRMSaleModel.MakerEnum.Laboratorio;
                    break;
                default:
                    saleInfo.maker = (int)CRMSaleModel.MakerEnum.Farmavet;
                    break;
            }
            if (recordset.Fields.Item("U_DOI_Rep").Value.ToString() == "Y")
                saleInfo.repoProv = true;
            else
                saleInfo.repoProv = false;

            tempString = recordset.Fields.Item("U_TipoGD").Value.ToString();
            if(string.IsNullOrWhiteSpace(tempString) || (tempString == "1"))
            {
                //Oferta definida
                saleInfo.saleType = (int)CRMSaleModel.SaleTypeEnum.Definido;
                //Obteniendo artículo que dispara la oferta
                tempString = recordset.Fields.Item("U_TipoDA").Value.ToString();
                if (tempString == "2")
                {
                    //Origen es un grupo de artículos
                    tempString = recordset.Fields.Item("U_CGAD").Value.ToString();
                    saleInfo.saleOrigin = GetItemsFromGroup(oCompany, tempString);
                    saleInfo.saleOriginQty = recordset.Fields.Item("U_CanGAD").Value;
                }
                else
                {
                    //Origen es un artículo
                    saleInfo.saleOrigin = new List<CRMBaseModel>();
                    saleInfo.saleOrigin.Add(new CRMBaseModel()
                    {
                        Id = recordset.Fields.Item("U_CArtD").Value.ToString(),
                        Name = recordset.Fields.Item("U_NArtD").Value.ToString(),
                    });
                    saleInfo.saleOriginQty = recordset.Fields.Item("U_CanAD").Value;
                }

                //Obteniendo artículo de regalo
                saleInfo.saleDestiny = new List<CRMBaseModel>();
                saleInfo.saleDestiny.Add(new CRMBaseModel()
                {
                    Id = recordset.Fields.Item("U_CArtRD").Value.ToString(),
                    Name = recordset.Fields.Item("U_NArtRD").Value.ToString()
                });
                saleInfo.saleDestinyQty = recordset.Fields.Item("U_CanARD").Value;
            }
            else
            {
                //Oferta "el valor más bajo"
                saleInfo.saleType = (int)CRMSaleModel.SaleTypeEnum.MasBajo;
                //Obteniendo artículo que dispara la oferta
                tempString = recordset.Fields.Item("U_TipoVA").Value.ToString();
                if (tempString == "2")
                {
                    //Origen es un grupo de artículos
                    tempString = recordset.Fields.Item("U_CGAV1").Value.ToString();
                    saleInfo.saleOrigin = GetItemsFromGroup(oCompany, tempString);
                    saleInfo.saleOriginQty = recordset.Fields.Item("U_CanGAV1").Value;
                }
                else
                {
                    //Origen es un artículo
                    saleInfo.saleOrigin = new List<CRMBaseModel>();
                    saleInfo.saleOrigin.Add(new CRMBaseModel()
                    {
                        Id = recordset.Fields.Item("U_CArtV").Value.ToString(),
                        Name = recordset.Fields.Item("U_NArtV").Value.ToString(),
                    });
                    saleInfo.saleOriginQty = recordset.Fields.Item("U_CanAV").Value;
                }

                //Obteniendo artículo de regalo
                tempString = recordset.Fields.Item("U_CGAV2").Value.ToString();
                saleInfo.saleDestiny = GetItemsFromGroup(oCompany, tempString);
                saleInfo.saleDestinyQty = recordset.Fields.Item("U_CanGAV2").Value;
            }

            saleInfo.pricesList = GetPriceListForSale(oCompany, saleInfo.Id);

            return saleInfo;
        }
        #endregion ********************************************************************************************************

        #region sales list ************************************************************************************************
        public static List<CRMBaseModel> GetSummarizedSalesList(SAPbobsCOM.Company oCompany, string itemCode)
        {
            List<CRMBaseModel> salesList = new List<CRMBaseModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string customerFilter = "";
            CRMBaseModel tempSales;

            ErrorHandler.WriteInLog("Getting summarized Sales list: Item code (" + itemCode.ToString() + ")",
                                    ErrorHandler.ErrorVerboseLevel.Trace);

            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected && !string.IsNullOrWhiteSpace(itemCode))
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by customer name
                    /*if (!string.IsNullOrWhiteSpace(customer))
                        customerFilter = string.Format(" AND " + SAPSalesQuerys.Sales_List_CustomerPricesFilter, customer);*/

                    sql = string.Format(SAPSalesQuerys.SalesList_HANA_OK, SAPSalesQuerys.SalesList_SummarizedData_HANA_OK, customerFilter, itemCode);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempSales = GetSummarizedModelFromRecordset(recordset, false);
                            salesList.Add(tempSales);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    salesList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetOrdersList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return salesList;
        }
        #endregion ********************************************************************************************************


        #region Get sale data *********************************************************************************************
        private static List<CRMBaseModel> GetItemsFromGroup (SAPbobsCOM.Company oCompany, string itemGroup)
        {
            List<CRMBaseModel> itemsList = new List<CRMBaseModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            CRMBaseModel tempItem;

            ErrorHandler.WriteInLog("     Getting items from group: item group(" + itemGroup + ")",
                                    ErrorHandler.ErrorVerboseLevel.Trace);

            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected && (!string.IsNullOrWhiteSpace(itemGroup)))
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    
                    sql = string.Format(SAPSalesQuerys.Sales_GetItemsFromGroup_HANA_OK, itemGroup);
                    ErrorHandler.WriteInLog("SAPSalesQuerys.Sales_GetItemsFromGrou " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191115 

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempItem = new CRMBaseModel()
                            {
                                Id = recordset.Fields.Item("Code").Value.ToString(),
                                Name = recordset.Fields.Item("Name").Value.ToString()
                            };
                            itemsList.Add(tempItem);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    itemsList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetItemsFromGroup: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return itemsList;
        }


        private static List<CRMBaseModel> GetPriceListForSale(SAPbobsCOM.Company oCompany, string saleCode)
        {
            List<CRMBaseModel> pricesList = new List<CRMBaseModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            CRMBaseModel tempItem;


            ErrorHandler.WriteInLog("     Getting prices for sale code: (" + saleCode + ")",
                                    ErrorHandler.ErrorVerboseLevel.Trace);

            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected && (!string.IsNullOrWhiteSpace(saleCode)))
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    sql = string.Format(SAPSalesQuerys.Sales_GetPricesList_HANA_OK, saleCode);
                    ErrorHandler.WriteInLog("SAPSalesQuerys.Sales_GetPricesList " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191115 
                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempItem = new CRMBaseModel()
                            {
                                Id = recordset.Fields.Item("U_CTa").Value.ToString(),
                                Name = recordset.Fields.Item("U_NTa").Value.ToString()
                            };
                            pricesList.Add(tempItem);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    pricesList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetPriceListForSale: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return pricesList;
        }


        /// <summary>
        /// Gets the sales for an item, or the complete list if it's null or empty
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="itemCode"></param>
        /// <returns></returns>
        public static List<CRMSaleModel> GetSalesList (SAPbobsCOM.Company oCompany, string itemCode)
        {
            List<CRMSaleModel> saleInfoList = new List<CRMSaleModel>();
            CRMSaleModel saleInfo = new CRMSaleModel();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string customerFilter = "";

            ErrorHandler.WriteInLog("Getting sale info: itemCode(" + itemCode + ")", ErrorHandler.ErrorVerboseLevel.Trace);

            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    if (!string.IsNullOrWhiteSpace(itemCode))
                    {
                        sql = string.Format(SAPSalesQuerys.SalesList_HANA_OK, SAPSalesQuerys.SalesList_Data_HANA_OK, customerFilter, itemCode);
                        ErrorHandler.WriteInLog("SAPSalesQuerys.SalesList  " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191115 WriteSQL log para intentar localizar APP CRASH
                    }
                    else
                    {
                        sql = SAPSalesQuerys.SalesList_All_HANA_OK;
                        ErrorHandler.WriteInLog("SAPSalesQuerys.SalesList_All  " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191115 WriteSQL log para intentar localizar APP CRASH

                    }

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            saleInfo = GetModelFromRecordset(oCompany, recordset);
                            saleInfoList.Add(saleInfo);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    saleInfoList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetOrdersList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return saleInfoList;
        }
        #endregion ********************************************************************************************************


        #region Get Sale Details ***************************************************************************************
        public static CRMSaleModel GetSaleDetails (SAPbobsCOM.Company oCompany, string saleCode)
        {
            CRMSaleModel saleInfo = new CRMSaleModel();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string saleCodeFilter;
            
            ErrorHandler.WriteInLog("Getting sale info: saleCode(" + saleCode + ")", ErrorHandler.ErrorVerboseLevel.Trace);

            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected && !string.IsNullOrWhiteSpace(saleCode))
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    saleCodeFilter = string.Format(SAPSalesQuerys.NO_SE_USA_SaleCodeFilter, saleCode);
                    sql = SAPSalesQuerys.SalesList_All_HANA_OK;

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        if (!recordset.EoF)
                        {
                            saleInfo = GetModelFromRecordset(oCompany, recordset);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    saleInfo = null;
                    ErrorHandler.WriteInLog("EXCEPTION GetOrdersList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return saleInfo;
        }
        #endregion *****************************************************************************************************




        #region Get other info for Sale *******************************************************************************
        public static List<CRMItemModel> GetPricesAndAvaiable(SAPbobsCOM.Company oCompany, CRMSaleModel sale, string userCode)
        {
            List<CRMItemModel> itemList = new List<CRMItemModel>();
            int i;
            string priceList = null;

            ErrorHandler.WriteInLog("Getting sale info: saleCode(" + sale.Id + ")", ErrorHandler.ErrorVerboseLevel.Trace);

            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected && (sale != null))
            {
                try
                {
                    if ((sale.pricesList != null) && !string.IsNullOrWhiteSpace(sale.pricesList[0].Id))
                    {
                        priceList = sale.pricesList[0].Id;
                    }

                    if ((sale.saleOrigin != null) && (priceList != null))
                    {
                        for (i = 0; i < sale.saleOrigin.Count; i++)
                        {
                            itemList.Add(SAPItems.GetItemDetails(oCompany, sale.saleOrigin[i].Id, priceList, userCode));
                        }
                        for (i = 0; i < sale.saleDestiny.Count; i++)
                        {
                            itemList.Add(SAPItems.GetItemDetails(oCompany, sale.saleDestiny[i].Id, priceList, userCode));
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    itemList = null;
                    ErrorHandler.WriteInLog("EXCEPTION GetPricesAndAvaiable for Sale: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return itemList;
        }
        #endregion *****************************************************************************************************
    }
}