﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPPayments
    {
        #region Models constructors from recordset *************************************************************************
        private static CRMPendingPaymentModel GetPendingPaymentModelFromRecordset(SAPbobsCOM.Recordset recordset)
        {
            CRMPendingPaymentModel tempModel = new CRMPendingPaymentModel();

            tempModel.Id = recordset.Fields.Item("DocEntry").Value.ToString();
            tempModel.docNum = recordset.Fields.Item("DocNum").Value.ToString();
            tempModel.cardCode = recordset.Fields.Item("CardCode").Value;
            tempModel.cardName = recordset.Fields.Item("CardName").Value;
            tempModel.docTotal = recordset.Fields.Item("DocTotal").Value;
            tempModel.paidToDate = recordset.Fields.Item("PaidToDate").Value + recordset.Fields.Item("DraftPaid").Value;
            tempModel.docDate = recordset.Fields.Item("DocDate").Value;
            tempModel.docDueDate = recordset.Fields.Item("DocDueDate").Value;

            tempModel.payMethodId = recordset.Fields.Item("PeyMethod").Value;
            tempModel.payMethodName = recordset.Fields.Item("Descript").Value;

            return tempModel;
        }
        #endregion ********************************************************************************************************

        #region Get pending payment list *********************************************************************************
        /// <summary>
        /// Retunrs a list of pending payments to this sales person
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson"></param>
        /// <returns></returns>
        public static List<CRMPendingPaymentModel> GetPendingPaymentList(SAPbobsCOM.Company oCompany, int salesPerson, string cardCode, string paymentMethod)
        {
            List<CRMPendingPaymentModel> pendingPaymentsList = new List<CRMPendingPaymentModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string cardCodeFilter = "";
            string paymentMethodFilter = "";
            string orderFilter = "";
            CRMPendingPaymentModel tempPendingPayment;

            ErrorHandler.WriteInLog("Getting Pending Payment list: SalesPerson(" + salesPerson.ToString() + "), CardCode(" + cardCode + "), Payment Method(" + paymentMethod + ")",
                                    ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(SAPPaymentsQuerys.GetPayments_SLPFilter_HANA_OK, salesPerson.ToString());
                    if (!string.IsNullOrWhiteSpace(cardCode))
                        cardCodeFilter = string.Format(SAPPaymentsQuerys.GetPayments_CardCodeFilter_HANA_OK, cardCode);
                    if (!string.IsNullOrWhiteSpace(paymentMethod))
                        paymentMethodFilter = string.Format(SAPPaymentsQuerys.GetPayments_PaymentMethodFilter_HANA_OK, paymentMethod);
                    orderFilter = SAPPaymentsQuerys.GetPayments_OrderFilter_HANA_OK;

                    sql = SAPPaymentsQuerys.GetPayments_NO_HANA + salesPersonFilter + cardCodeFilter + paymentMethodFilter + orderFilter;

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempPendingPayment = GetPendingPaymentModelFromRecordset(recordset);
                            pendingPaymentsList.Add(tempPendingPayment);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    pendingPaymentsList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetPendingPaymentList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return pendingPaymentsList;
        }
        #endregion ********************************************************************************************************


        #region Add payment ***********************************************************************************************
        private static string GetPaymentDocNum(SAPbobsCOM.Company oCompany, bool asDraft, string docEntry)
        {
            SAPbobsCOM.Recordset recordset;
            string sql;
            string docNum = "";

            if (!string.IsNullOrWhiteSpace(docEntry) && (oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (!asDraft)
                        sql = string.Format(SAPPaymentsQuerys.GetPayments_GetDocNum_HANA_OK, docEntry);
                    else
                        sql = string.Format(SAPPaymentsQuerys.GetPayments_GetDraftDocNum_HANA_OK, docEntry);

                    recordset.DoQuery(sql);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        docNum = recordset.Fields.Item("DocNum").Value.ToString();
                        ErrorHandler.WriteInLog("DocNum found: " + docNum, ErrorHandler.ErrorVerboseLevel.Trace);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION GetPaymentDocNum: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return docNum;
        }


        public static string AddPayment(Company oCompany, CRMPaymentModel payment, bool asDraft)
        {
            SAPbobsCOM.Payments paymentSAP;
            string _error = "";
            int iError;
            string tempCashAccount;

            if (payment != null)
            {
                try
                {
                    if (!asDraft)
                        paymentSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments);
                    else
                    {
                        paymentSAP = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPaymentsDrafts);
                        paymentSAP.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_IncomingPayments;
                    }

                    paymentSAP.CardCode = payment.cardCode;
                    switch (payment.paymentMode)
                    {
                        case CRMPaymentModel.PaymentModeType.cash:
                            paymentSAP.CashSum = payment.docTotal;
                            if (payment.bankCode != "-1")
                                paymentSAP.CashAccount = payment.bankCode;
                            else
                            {
                                tempCashAccount = GetUserCashAccount(oCompany, payment.userName);
                                if (!string.IsNullOrWhiteSpace(tempCashAccount))
                                    paymentSAP.CashAccount = tempCashAccount;
                                else
                                    throw new Exception("-e001: No hay asignada una cuenta de caja de comercial al usuario " + payment.userName + ".");
                            }
                            break;
                        case CRMPaymentModel.PaymentModeType.check:

                            /* AMM 20191111 SOPSAP-428
                             * OJO!!! los corbor de TIPO EFECTO se crearán como borradores pero OJO!! de tipo efectivo... pues el tipo CHEQUE nos dará problemas con el nº talon alfanumerico
                            paymentSAP.CheckAccount = payment.bankCode;   //WARNING: is not assigning to CHECKS LINES
                            paymentSAP.Checks.CheckSum = payment.docTotal;
                            paymentSAP.Checks.CheckNumber = int.Parse(payment.checkNumber);
                            paymentSAP.Checks.DueDate = payment.checkDueDate;
                            paymentSAP.Checks.CheckAccount = payment.bankCode;  //Cuenta de mayor
                            CRMBankAccountModel bankData = SAPBankData.GetBankAccountData(oCompany, payment.bankCode);
                            if (bankData != null)
                            {
                                paymentSAP.Checks.BankCode = bankData.Id;
                                paymentSAP.Checks.Branch = bankData.Branch;
                                paymentSAP.Checks.AccounttNum = bankData.Account;
                            }
                            else
                                throw new Exception("-2028: Bank data not found.");
                            //paymentSAP.Checks.Add();
                            */
                            //AMM 20191111 SOPSAP-428 borrador ed de efecto se guardaran como de tipo efectivo alimentando 3 UDFs
                            paymentSAP.UserFields.Fields.Item("U_es_talon").Value = "Y";
                            paymentSAP.UserFields.Fields.Item("U_num_talon").Value = payment.checkNumber;
                            paymentSAP.UserFields.Fields.Item("U_venc_talon").Value = payment.checkDueDate;
                            paymentSAP.CashSum = payment.docTotal;
                            if (payment.bankCode != "-1")
                                paymentSAP.CashAccount = payment.bankCode;
                            else
                            {
                                tempCashAccount = GetUserCashAccount(oCompany, payment.userName);
                                if (!string.IsNullOrWhiteSpace(tempCashAccount))
                                    paymentSAP.CashAccount = tempCashAccount;
                                else
                                    throw new Exception("-e001: No hay asignada una cuenta de caja de comercial al usuario " + payment.userName + ".");
                            }
                            break;
                        /*case CRMPaymentModel.PaymentModeType.check:   //Pago mediante efectos
                            paymentSAP.BillOfExchangeAmount = payment.docTotal;
                            //paymentSAP.BoeAccount = payment.bankCode; //No, esto viene en el número del talón, no se debe coger de payment porque no se ha podido seleccionar, es solo para efectivo
                            paymentSAP.BillOfExchange.BillOfExchangeDueDate = payment.checkDueDate;
                            paymentSAP.BillOfExchange.ReferenceNo = payment.checkNumber;

                            CRMBPPaymentModel paymentInfo = SAPBPPaymentInfo.GetBPPaymentInfo(oCompany, payment.cardCode);
                            if (paymentInfo != null)
                            {
                                paymentSAP.BillOfExchange.PaymentMethodCode = paymentInfo.PaymentWayId;
                                //paymentSAP.BoeAccount = paymentInfo.CCCAccount;
                                //paymentSAP.PayToBankCode = paymentInfo.CCCBankCode;
                                //paymentSAP.PayToBankBranch = paymentInfo.CCCBranch;
                                //paymentSAP.PayToBankAccountNo = paymentInfo.CCCAccount;
                            }
                            break;*/
                    }

                    //In case of partial payment, there is only ONE invoice, so we apply the total ammount of the paymnt to it
                    if (payment.invoicesID.Count == 1)
                        paymentSAP.Invoices.SumApplied = payment.docTotal;
                    //Set the rest of values for any invoice
                    for (int i = 0; i < payment.invoicesID.Count; i++)
                    {
                        if (i > 0)
                            paymentSAP.Invoices.Add();

                        paymentSAP.Invoices.DocEntry = int.Parse(payment.invoicesID[i]);
                        paymentSAP.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                    }

                    paymentSAP.Remarks = payment.comments;


                    paymentSAP.Add();

                    oCompany.GetLastError(out iError, out _error);
                    if (iError != 0)
                    {
                        _error = iError.ToString() + ": " + _error + "******" + paymentSAP.BillOfExchange.PaymentMethodCode;
                    }
                    else
                    {
                        _error = oCompany.GetNewObjectKey();
                        _error = GetPaymentDocNum(oCompany, asDraft, _error);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION AddPayment: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                    _error = "-ex00|Se ha producido un error interno en el servidor. Consulte a su administrador del sistema.";
                }
            }
            else
            {
                _error = "-e005|El pago está vacío";
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Payment new error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }

        #endregion ********************************************************************************************************


        #region Business Partner payment info ***********************************************************************
        public static CRMBPCreditInfo GetCreditInfo (SAPbobsCOM.Company oCompany, string cardCode)
        {
            SAPbobsCOM.Recordset recordset;
            string sql;
            CRMBPCreditInfo creditInfo = new CRMBPCreditInfo();
            
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected && !string.IsNullOrWhiteSpace(cardCode))
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    sql = string.Format(SAPPaymentsQuerys.GetCreditInfo_HANA_OK, cardCode);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        if (!recordset.EoF)
                        {
                            creditInfo.CreditLine = recordset.Fields.Item("CreditLine").Value;
                            creditInfo.CurrentBalance = recordset.Fields.Item("Balance").Value;
                        }
                    }
                }
                catch (Exception ex)
                {
                    creditInfo = null;
                    ErrorHandler.WriteInLog("EXCEPTION GetCreditInfo: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            else
            {
                ErrorHandler.WriteInLog("-e001|CardCode no válido.", ErrorHandler.ErrorVerboseLevel.None);
            }


            return creditInfo;

        }
        #endregion **************************************************************************************************

        #region Otras funciones privadas ****************************************************************************
        private static string GetUserCashAccount(SAPbobsCOM.Company oCompany, string userName)
        {
            SAPbobsCOM.Recordset recordset;
            string sql;
            string cashAccount = "";

            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected && !string.IsNullOrWhiteSpace(userName))
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    sql = string.Format(SAPPaymentsQuerys.GetCashAccountFromUser_HANA_OK, userName);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        if (!recordset.EoF)
                        {
                            cashAccount = recordset.Fields.Item("CashAcct").Value;
                        }
                    }
                }
                catch (Exception ex)
                {
                    cashAccount = null;
                    ErrorHandler.WriteInLog("EXCEPTION GetUserCashAccount: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            else
            {
                ErrorHandler.WriteInLog("-e001|problema de conexión con SAP o userName no válido.", ErrorHandler.ErrorVerboseLevel.None);
            }


            return cashAccount;
        }
        #endregion **************************************************************************************************
    }
}