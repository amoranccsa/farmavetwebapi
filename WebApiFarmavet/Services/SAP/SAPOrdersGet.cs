﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPOrdersGet
    {
        #region Models constructors from recordsets ***********************************************************************
        private static CRMOrderModel GetOrderModelFromRecordset(SAPbobsCOM.Recordset recordset, bool getStatus)
        {
            CRMOrderModel tempModel = new CRMOrderModel();

            tempModel.Id = recordset.Fields.Item("DocEntry").Value.ToString();
            tempModel.docNum = recordset.Fields.Item("DocNum").Value.ToString();
            tempModel.docTotal = recordset.Fields.Item("DocTotal").Value;
            tempModel.cardCode = recordset.Fields.Item("CardCode").Value;
            tempModel.cardName = recordset.Fields.Item("CardName").Value;
            tempModel.docDate = recordset.Fields.Item("DocDate").Value;
            tempModel.docDueDate = recordset.Fields.Item("DocDueDate").Value;
            //Código FARMAVET *****************************************************
            if (recordset.Fields.Item("Draft").Value == "Y")
                tempModel.draft = true;
            else
                tempModel.draft = false;

            if (getStatus)
            {
                if (recordset.Fields.Item("DocStatus").Value == "O")
                    tempModel.open = true;
                else
                    tempModel.open = false;
            }
            //*********************************************************************

            return tempModel;
        }

        private static CRMOrderDetailModel GetOrderDetailModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset recordset)
        {
            CRMOrderDetailModel tempModel = new CRMOrderDetailModel();
            string addressID = "";

            tempModel.Id = recordset.Fields.Item("DocEntry").Value.ToString();
            tempModel.docNum = recordset.Fields.Item("DocNum").Value.ToString();
            tempModel.cardCode = recordset.Fields.Item("CardCode").Value;
            tempModel.cardName = recordset.Fields.Item("CardName").Value;
            tempModel.docTotal = recordset.Fields.Item("DocTotal").Value;
            tempModel.docDueDate = recordset.Fields.Item("DocDueDate").Value;
            tempModel.docDate = recordset.Fields.Item("DocDate").Value;
            tempModel.comments = recordset.Fields.Item("Comments").Value;
            tempModel.authComments = recordset.Fields.Item("U_DOI_APPAUTH").Value;//AMM 20191122

            addressID = recordset.Fields.Item("ShipToCode").Value;

            if (!string.IsNullOrWhiteSpace(addressID))
                tempModel.shipAddress = SAPBPAddress.GetAddressData(oCompany, tempModel.cardCode, SAPBPAddress.AddressType.Shipping, addressID);
            else
                tempModel.shipAddress = new CRMBPAddressModel();

            //Código FARMAVET *****************************************************
            if (recordset.Fields.Item("DocStatus").Value == "O")
                tempModel.open = true;
            else
                tempModel.open = false;

            tempModel.discPrcnt = recordset.Fields.Item("DiscPrcnt").Value;
            //"ODRF"."GroupNum", "OCTG"."PymntGroup", "PeyMethod", "OPYM"."Descript"
            tempModel.paymentTermsId = recordset.Fields.Item("GroupNum").Value;
            tempModel.paymentTermsName = recordset.Fields.Item("PymntGroup").Value;
            tempModel.paymentMethodId = recordset.Fields.Item("PeyMethod").Value;
            tempModel.paymentMethodName = recordset.Fields.Item("Descript").Value;
            //*********************************************************************

            tempModel.comments = tempModel.comments.Replace("\r", "\n");

            return tempModel;
        }

        private static CRMOrderLineModel GetOrderLineModelFromRecordset(SAPbobsCOM.Recordset recordset)
        {
            CRMOrderLineModel tempModel = new CRMOrderLineModel();
            //ItemCode, Dscription, Quantity, UomEntry, UomCode, LineTotal, text
            tempModel.Id = recordset.Fields.Item("LineNum").Value.ToString();
            tempModel.itemCode = recordset.Fields.Item("ItemCode").Value;
            tempModel.description = recordset.Fields.Item("Dscription").Value;
            tempModel.quantity = Convert.ToInt32(recordset.Fields.Item("Quantity").Value);
            tempModel.openQuantity = Convert.ToDouble(recordset.Fields.Item("OpenQty").Value);
            tempModel.servedQuantity = Convert.ToDouble(recordset.Fields.Item("ServedQty").Value);
            tempModel.salesUomEntry = recordset.Fields.Item("SalesUomEntry").Value;
            tempModel.salesUomCode = recordset.Fields.Item("SalesUomCode").Value;
            tempModel.invUomEntry = recordset.Fields.Item("InvUomEntry").Value;
            tempModel.invUomCode = recordset.Fields.Item("InvUomCode").Value;
            tempModel.unitPrice = recordset.Fields.Item("Price").Value;
            tempModel.vatPercent = recordset.Fields.Item("VatPrcnt").Value;
            tempModel.lineTotal = recordset.Fields.Item("LineTotal").Value;
            tempModel.text = recordset.Fields.Item("text").Value;

            //Código Farmavet ************************************
            tempModel.discPrcnt = recordset.Fields.Item("DiscPrcnt").Value;
            tempModel.whsCode = recordset.Fields.Item("WhsCode").Value.ToString();
            tempModel.whsName = recordset.Fields.Item("WhsName").Value;
            if (recordset.Fields.Item("LineStatus").Value == "C")
                tempModel.lineClosed = true;
            else
                tempModel.lineClosed = false;

            tempModel.saleCode = recordset.Fields.Item("U_DOI_CPro").Value;
            tempModel.saleName = recordset.Fields.Item("U_DOI_NPro").Value;

            tempModel.dogertyDiscounts = new CRMDogertyItemDiscounts()
            {
                dtoCampana = recordset.Fields.Item("U_l_dto_ca").Value,
                dto1 = recordset.Fields.Item("U_l_dto_1").Value,
                dto2 = recordset.Fields.Item("U_l_dto_2").Value,
                dto3 = recordset.Fields.Item("U_l_dto_3").Value,
                dtoExtra = recordset.Fields.Item("U_l_dto_xtra").Value,
                dto4 = recordset.Fields.Item("U_l_dto_4").Value
            };

            if (recordset.Fields.Item("U_c_camp").Value == "Y")
                tempModel.aplicarCampana = true;
            else
                tempModel.aplicarCampana = false;
            //****************************************************

            return tempModel;
        }
        #endregion *******************************************************************************************************

        #region Get orders list ******************************************************************************************
        /// <summary>
        /// Returns the list of opened orders for the sales person ID selected, or all if the code is -2
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="drafts">Set to true to return ONLY the draft orders</param>
        /// <param name="salesPerson">-2: Returns all the orders, -1: Isn't a sales person, returns an empty list, >=0: only his/her orders</param>
        /// <param name="order">The DocNUM start (not the DocENTRY)</param>
        /// <param name="customer">Customer CardName</param>
        /// <returns></returns>
        public static List<CRMOrderModel> GetOpenOrdersList(SAPbobsCOM.Company oCompany, bool drafts, int salesPerson, string order, string customer)
        {
            List<CRMOrderModel> ordersList = new List<CRMOrderModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string orderFilter = "";
            string customerFilter = "";
            CRMOrderModel tempOrder;

            ErrorHandler.WriteInLog("Getting Open Orders list: SalesPerson(" + salesPerson.ToString() + "); DocNum(" + order +
                                    "*); Customer(" + customer + "*)", ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_SLPFilter_HANA_OK, salesPerson);
                    //Filtering list by order id
                    if (!string.IsNullOrWhiteSpace(order))
                        orderFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_DocNumFilter_HANA_OK, order);
                    //Filtering list by customer name
                    if (!string.IsNullOrWhiteSpace(customer))
                        customerFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_CardNameFilter_HANA_OK, customer);

                    if (!drafts)
                        sql = SAPOrdersQuerys.GetOrdersOpened_HANA_OK + salesPersonFilter + orderFilter + customerFilter;
                    else
                        sql = SAPOrdersQuerys.GetDraftsOpened_HANA_OK + salesPersonFilter + orderFilter + customerFilter;

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempOrder = GetOrderModelFromRecordset(recordset, false);
                            tempOrder.draft = drafts;
                            ordersList.Add(tempOrder);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ordersList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetOrdersList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            //}

            return ordersList;
        }


        /// <summary>
        /// Returns the list of orders for the sales person ID selected, or all if the code is -2
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="drafts">Set to true to return ONLY the draft orders</param>
        /// <param name="salesPerson">-2: Returns all the orders, -1: Isn't a sales person, returns an empty list, >=0: only his/her orders</param>
        /// <param name="order">The DocNUM start (not the DocENTRY)</param>
        /// <param name="customer">Customer CardName</param>
        /// <returns></returns>
        public static List<CRMOrderModel> GetCompleteOrdersList(SAPbobsCOM.Company oCompany, bool drafts, int salesPerson, string order, string customer, string dateFrom, string dateTo)
        {
            List<CRMOrderModel> ordersList = new List<CRMOrderModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string orderFilter = "";
            string customerFilter = "";
            string dateFilter = "";
            CRMOrderModel tempOrder;

            ErrorHandler.WriteInLog("Getting Open Orders list: SalesPerson(" + salesPerson.ToString() + "); DocNum(" + order +
                                    "*); Customer(" + customer + "*)", ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_SLPFilter_HANA_OK, salesPerson);
                    //Filtering list by order id
                    if (!string.IsNullOrWhiteSpace(order))
                        orderFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_DocNumFilter_HANA_OK, order);
                    //Filtering list by customer name
                    if (!string.IsNullOrWhiteSpace(customer))
                        customerFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_CardNameFilter_HANA_OK, customer);
                    //Filtering list by dates
                    if (!string.IsNullOrWhiteSpace(dateFrom) && !string.IsNullOrWhiteSpace(dateTo))
                    {
                        /*dateFilter = " AND " + string.Format(SAPOrdersQuerys.GetOrders_DatesFilter,
                            DateTime.ParseExact(dateFrom, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture),
                            DateTime.ParseExact(dateTo, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture));*/
                        dateFilter = " AND " + string.Format(SAPOrdersQuerys.GetOrders_DatesFilter_NO_HANA,
                            DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")),
                            DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));
                    }

                    if (!drafts)
                        sql = SAPOrdersQuerys.GetAllOrders_HANA_OK + salesPersonFilter + orderFilter + customerFilter + dateFilter;
                    else
                        sql = SAPOrdersQuerys.GetAllDrafts_HANA_OK + salesPersonFilter + orderFilter + customerFilter + dateFilter;

                    //ErrorHandler.WriteInLog(sql, ErrorHandler.ErrorVerboseLevel.Trace);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempOrder = GetOrderModelFromRecordset(recordset, true);
                            tempOrder.draft = drafts;
                            ordersList.Add(tempOrder);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ordersList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetOrdersList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            //}

            return ordersList;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="drafts">Set to true to return the draft orders</param>
        /// <param name="documents">Set to true to return the draft documents</param>
        /// <param name="salesPerson">-2: Returns all the orders, -1: Isn't a sales person, returns an empty list, >=0: only his/her orders</param>
        /// <param name="order">The DocNUM start (not the DocENTRY)</param>
        /// <param name="customer">Customer CardName</param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static List<CRMOrderModel> GetCompletePagedOrdersList(SAPbobsCOM.Company oCompany, bool drafts, bool documents, int salesPerson, string order,
                                                                     string customer, string dateFrom, string dateTo, int page)
        {
            List<CRMOrderModel> ordersList = new List<CRMOrderModel>();
            SAPbobsCOM.Recordset recordset;
            string sqlDoc;
            string sqlDrf;
            string sql, sqlUnion;
            string salesPersonFilter = "";
            string orderFilter = "";
            string customerFilter = "";
            string dateFilter = "";
            CRMOrderModel tempOrder;

            int firstRow;
            int lastRow;
            int nElements;

            ErrorHandler.WriteInLog("Getting Complete Orders paged list: SalesPerson(" + salesPerson.ToString() + "); DocNum(" + order +
                                    "*); Customer(" + customer + "*)", ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    //Getting first and last row to fetch
                    if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("PageRows"), out nElements))
                        nElements = 10;

                    firstRow = (page * nElements) + 1; //first row is number 1
                    lastRow = (page * nElements) + nElements;

                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_SLPFilter_HANA_OK, salesPerson);
                    //Filtering list by order id
                    if (!string.IsNullOrWhiteSpace(order))
                        orderFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_DocNumFilter_HANA_OK, order);
                    //Filtering list by customer name
                    if (!string.IsNullOrWhiteSpace(customer))
                        customerFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_CardNameFilter_HANA_OK, customer);
                    //Filtering list by dates
                    if (!string.IsNullOrWhiteSpace(dateFrom) && !string.IsNullOrWhiteSpace(dateTo))
                    {
                        dateFilter = " AND " + string.Format(SAPOrdersQuerys.GetOrders_DatesFilter_NO_HANA,
                            DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")),
                            DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));
                    }


                    //Creando la SQL componiendo la SQL de documentos y borradores
                    if (documents)
                        sqlDoc = SAPOrdersQuerys.GetAllOrders_HANA_OK + salesPersonFilter + orderFilter + customerFilter + dateFilter;
                    else
                        sqlDoc = "";

                    if (drafts)
                        sqlDrf = SAPOrdersQuerys.GetAllDrafts_HANA_OK + salesPersonFilter + orderFilter + customerFilter + dateFilter + SAPOrdersQuerys.GetAllDrafts_NoClosedFilter_HANA_OK;
                    else
                        sqlDrf = "";

                    if (documents && drafts)
                        sqlUnion = " UNION ALL ";
                    else
                        sqlUnion = "";

                    sql = string.Format(SAPOrdersQuerys.GetAllOrdersDraftsPaged_HANA_OK, sqlDoc, sqlUnion, sqlDrf, firstRow, lastRow);

                    //ErrorHandler.WriteInLog(sql, ErrorHandler.ErrorVerboseLevel.Trace);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempOrder = GetOrderModelFromRecordset(recordset, true);
                            ordersList.Add(tempOrder);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ordersList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetOrdersList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            //}

            return ordersList;
        }


        /// <summary>
        /// Returns the list of orders with, at least, one delivery done,for the sales person ID selected,
        /// or all if the code is -2
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson">-2: Returns all the orders, -1: Isn't a sales person, returns an empty list, >=0: only his/her orders</param>
        /// <param name="order">The DocNUM start (not the DocENTRY)</param>
        /// <param name="customer">Customer CardName</param>
        /// <param name="dateFrom">minimum DocDueDate</param>
        /// <param name="dateTo">maximum DocDueDate</param>
        /// <returns></returns>
        public static List<CRMOrderModel> GetOrdersWithDeliveriesList(SAPbobsCOM.Company oCompany, int salesPerson, string order, string customer, string dateFrom, string dateTo)
        {
            List<CRMOrderModel> ordersList = new List<CRMOrderModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string orderFilter = "";
            string customerFilter = "";
            string dateFilter = "";
            CRMOrderModel tempOrder;

            ErrorHandler.WriteInLog("Getting Orders with deliveries list: SalesPerson(" + salesPerson.ToString() + "); DocNum(" + order +
                                    "*); Customer(" + customer + "*)", ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(" WHERE " + SAPOrdersQuerys.GetOrders_SLPFilter_HANA_OK, salesPerson);
                    //Filtering list by order id
                    if (!string.IsNullOrWhiteSpace(order))
                    {
                        if (string.IsNullOrWhiteSpace(salesPersonFilter))
                            orderFilter = string.Format(" WHERE " + SAPOrdersQuerys.GetOrders_DocNumFilter_HANA_OK, order);
                        else
                            orderFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_DocNumFilter_HANA_OK, order);
                    }
                    //Filtering list by customer name
                    if (!string.IsNullOrWhiteSpace(customer))
                        if (string.IsNullOrWhiteSpace(salesPersonFilter) && string.IsNullOrWhiteSpace(orderFilter))
                            customerFilter = string.Format(" WHERE " + SAPOrdersQuerys.GetOrders_CardNameFilter_HANA_OK, customer);
                        else
                            customerFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_CardNameFilter_HANA_OK, customer);
                    //Filtering list by dates
                    if (!string.IsNullOrWhiteSpace(dateFrom) && !string.IsNullOrWhiteSpace(dateTo))
                    {
                        /*dateFilter = " AND " + string.Format(SAPOrdersQuerys.GetOrders_DatesFilter,
                            DateTime.ParseExact(dateFrom, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture),
                            DateTime.ParseExact(dateTo, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture));*/
                        dateFilter = " AND " + string.Format(SAPOrdersQuerys.GetOrders_DatesFilter_NO_HANA,
                            DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")),
                            DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));
                    }

                    sql = SAPOrdersQuerys.GetOrdersWitDeliveries_HANA_OK + salesPersonFilter + orderFilter + customerFilter + dateFilter;

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempOrder = GetOrderModelFromRecordset(recordset, true);
                            ordersList.Add(tempOrder);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ordersList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetOrdersList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            //}

            return ordersList;
        }


        /// <summary>
        /// Returns the list of orders with, at least, one delivery done,for the sales person ID selected,
        /// or all if the code is -2, but only part of the list
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="salesPerson"></param>
        /// <param name="order"></param>
        /// <param name="customer"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public static List<CRMOrderModel> GetOrdersWithDeliveriesList(SAPbobsCOM.Company oCompany, int salesPerson, string order, string customer, string dateFrom, string dateTo, int page)
        {
            List<CRMOrderModel> ordersList = new List<CRMOrderModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            string salesPersonFilter = "";
            string orderFilter = "";
            string customerFilter = "";
            string dateFilter = "";
            CRMOrderModel tempOrder;

            int firstRow;
            int lastRow;
            int nElements;

            ErrorHandler.WriteInLog("Getting Orders with deliveries list: SalesPerson(" + salesPerson.ToString() + "); DocNum(" + order +
                                    "*); Customer(" + customer + "*); Page(" + page + ")", ErrorHandler.ErrorVerboseLevel.Trace);
            //If salesPerson is -1, it's not a salesPerson valid value, so it will return an empty list
            //if (salesPerson != -1)
            //{
            //Checking oCompany connection
            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    //Getting first and last row to fetch
                    if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("PageRows"), out nElements))
                        nElements = 10;

                    firstRow = (page * nElements) + 1; //first row is number 1
                    lastRow = (page * nElements) + nElements;

                    //Filtering list by sales person id
                    if (salesPerson > 0)
                        salesPersonFilter = string.Format(" WHERE " + SAPOrdersQuerys.GetOrders_SLPFilter_HANA_OK, salesPerson);
                    //Filtering list by order id
                    if (!string.IsNullOrWhiteSpace(order))
                    {
                        if (string.IsNullOrWhiteSpace(salesPersonFilter))
                            orderFilter = string.Format(" WHERE " + SAPOrdersQuerys.GetOrders_DocNumFilter_HANA_OK, order);
                        else
                            orderFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_DocNumFilter_HANA_OK, order);
                    }
                    //Filtering list by customer name
                    if (!string.IsNullOrWhiteSpace(customer))
                        if (string.IsNullOrWhiteSpace(salesPersonFilter) && string.IsNullOrWhiteSpace(orderFilter))
                            customerFilter = string.Format(" WHERE " + SAPOrdersQuerys.GetOrders_CardNameFilter_HANA_OK, customer);
                        else
                            customerFilter = string.Format(" AND " + SAPOrdersQuerys.GetOrders_CardNameFilter_HANA_OK, customer);
                    //Filtering list by dates
                    if (!string.IsNullOrWhiteSpace(dateFrom) && !string.IsNullOrWhiteSpace(dateTo))
                    {
                        if (string.IsNullOrWhiteSpace(salesPersonFilter) && string.IsNullOrWhiteSpace(orderFilter) && string.IsNullOrWhiteSpace(customerFilter))
                            dateFilter = " WHERE ";
                        else
                            dateFilter = " AND ";

                        /*dateFilter += string.Format(SAPOrdersQuerys.GetOrders_DatesFilter,
                            DateTime.ParseExact(dateFrom, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture),
                            DateTime.ParseExact(dateTo, System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha"),
                                                System.Globalization.CultureInfo.InvariantCulture));*/
                        dateFilter += string.Format(SAPOrdersQuerys.GetOrders_DatesFilter_NO_HANA,
                            DateTime.ParseExact(dateFrom, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")),
                            DateTime.ParseExact(dateTo, WebApiConfig.AppDateFormat,
                                                System.Globalization.CultureInfo.InvariantCulture).ToString(System.Configuration.ConfigurationManager.AppSettings.Get("SQLFormatoFecha")));
                    }

                    sql = string.Format(SAPOrdersQuerys.GetOrdersWithDeliveriesPaged_HANA_OK, salesPersonFilter + orderFilter + customerFilter + dateFilter, firstRow, lastRow);

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempOrder = GetOrderModelFromRecordset(recordset, true);
                            ordersList.Add(tempOrder);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ordersList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetOrdersList: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            //}

            return ordersList;
        }

        #endregion *******************************************************************************************************

        #region Get order details ****************************************************************************************

        public static List<CRMOrderLineModel> GetOrderLines(SAPbobsCOM.Company oCompany, string _order, bool isDraft)
        {
            List<CRMOrderLineModel> linesList = new List<CRMOrderLineModel>();
            SAPbobsCOM.Recordset recordset;
            string sql;
            CRMOrderLineModel tempLine;

            if (!string.IsNullOrWhiteSpace(_order) && (oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    /*if (!isDraft)
                        sql = string.Format(SAPOrdersQuerys.GetOrder_Lines, _order);
                    else
                        sql = string.Format(SAPOrdersQuerys.GetDraft_Lines, _order);*/

                    //Código Farmavet **************************************************************
                    if (!isDraft)
                        sql = string.Format(SAPOrdersQuerys.GetOrder_LinesFarmavet_HANA_OK, _order);
                    else
                        sql = string.Format(SAPOrdersQuerys.GetDraft_LinesFarmavet_HANA_OK, _order);
                    //******************************************************************************

                    recordset.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordset.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        while (!recordset.EoF)
                        {
                            tempLine = GetOrderLineModelFromRecordset(recordset);
                            if (isDraft)
                                tempLine.servedQuantity = 0;

                            linesList.Add(tempLine);
                            recordset.MoveNext();
                        }
                    }
                }
                catch (Exception ex)
                {
                    linesList.Clear();
                    ErrorHandler.WriteInLog("EXCEPTION GetOrderLines: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return linesList;
        }



        public static CRMOrderDetailModel GetOrderDetails(SAPbobsCOM.Company oCompany, string _order, bool isDraft)
        {
            CRMOrderDetailModel orderDetail = new CRMOrderDetailModel();
            SAPbobsCOM.Recordset recordset;
            string sql;

            if (!string.IsNullOrWhiteSpace(_order) && (oCompany != null) && oCompany.Connected)
            {
                try
                {
                    recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (!isDraft)
                    {
                        sql = string.Format(SAPOrdersQuerys.GetOrder_Details_HANA_OK, _order);
                        //AMM 20191127 Mail de Ale Muñoz al intentar visualizar un pedido en productivo da error  vamos a depurar la SQL .debug
                        ErrorHandler.WriteInLog("SAPOrdersQuerys.GetOrderDetails " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191115 

                    }
                    else
                    {
                        sql = string.Format(SAPOrdersQuerys.GetDraft_Details_HANA_OK, _order);
                        //AMM 20191127 Mail de Ale Muñoz al intentar visualizar un pedido en productivo da error  vamos a depurar la SQL .debug
                        ErrorHandler.WriteInLog("SAPOrdersQuerys.GetDraft_Details " + sql, ErrorHandler.ErrorVerboseLevel.Debug); //AMM 20191115 

                    }



                    recordset.DoQuery(sql);
                    if (recordset.RecordCount > 0)
                    {
                        recordset.MoveFirst();
                        orderDetail = GetOrderDetailModelFromRecordset(oCompany, recordset);
                        orderDetail.draft = isDraft;
                        orderDetail.lines = GetOrderLines(oCompany, _order, isDraft);
                    }
                }
                catch (Exception ex)
                {
                    orderDetail = null;
                    ErrorHandler.WriteInLog("EXCEPTION GetOrderDetails: " + ex, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return orderDetail;
        }


        #endregion *******************************************************************************************************

    }
}