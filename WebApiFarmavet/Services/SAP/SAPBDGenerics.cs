﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiFarmavet.Models;

namespace WebApiFarmavet.Services.SAP
{
    public static class SAPBDGenerics
    {
        /// <summary>
        /// SELECT field FROM teble WHERE whereClause
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="whereClause"></param>
        /// <returns></returns>
        public static string GetFirstValueFromTable(SAPbobsCOM.Company oCompany, string table, string field, string whereClause)
        {
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string whereFilter = "";
            string result = null;

            try
            {
                //Errors check
                if ((oCompany == null) || !oCompany.Connected)
                    throw new Exception("Instancia de objeto Company no válido.");
                if (string.IsNullOrWhiteSpace(table))
                    throw new Exception("Nombre de tabla vacía.");
                if (string.IsNullOrWhiteSpace(field))
                    throw new Exception("Nombre de campo vacío.");

                //Initializing objects
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (!string.IsNullOrWhiteSpace(whereClause))
                    whereFilter = " WHERE " + whereClause;
                sql = string.Format("SELECT \"{0}\" FROM \"{1}\" {2}", field, table, whereFilter);

                recordSet.DoQuery(sql);
                ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);

                if (recordSet.RecordCount > 0)
                {
                    recordSet.MoveFirst();
                    result = recordSet.Fields.Item(field).Value.ToString();
                }

                recordSet = null;

            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return result;
        }

        /// <summary>
        /// Return the entire list of registers of a table, with the Code and Name field only
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="table">Table to read</param>
        /// <param name="whereClause">Conditions to filter</param>
        /// <returns></returns>
        public static List<CRMBaseModel> GetIDNameListFromTable(SAPbobsCOM.Company oCompany, string table, string whereClause)
        {
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string whereFilter = "";
            CRMBaseModel tempData;
            List<CRMBaseModel> dataList = new List<CRMBaseModel>();

            try
            {
                //Errors check
                if ((oCompany == null) || !oCompany.Connected)
                    throw new Exception("Instancia de objeto Company no válido.");
                if (string.IsNullOrWhiteSpace(table))
                    throw new Exception("Nombre de tabla vacía.");

                //Initializing objects
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (!string.IsNullOrWhiteSpace(whereClause))
                    whereFilter = " WHERE " + whereClause;
                sql = string.Format("SELECT \"Code\", \"Name\" FROM \"{0}\" {1}", table, whereFilter);

                recordSet.DoQuery(sql);
                ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);

                if (recordSet.RecordCount > 0)
                {
                    recordSet.MoveFirst();
                    while (!recordSet.EoF)
                    {
                        tempData = new CRMBaseModel();
                        tempData.Id = recordSet.Fields.Item("Code").Value.ToString();
                        tempData.Name = recordSet.Fields.Item("Name").Value.ToString();
                        dataList.Add(tempData);
                        recordSet.MoveNext();
                    }
                }

                recordSet = null;

            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            ErrorHandler.WriteInLog("Found " + dataList.Count.ToString() + " registers.", ErrorHandler.ErrorVerboseLevel.Trace);

            return dataList;
        }



        /// <summary>
        /// Return the entire list of registers of a table, with the Code and Name field only, using nameField to get the name
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="table"></param>
        /// <param name="nameField"></param>
        /// <param name="whereClause"></param>
        /// <returns></returns>
        public static List<CRMBaseModel> GetIDCustomnameListFromTable(SAPbobsCOM.Company oCompany, string table, string nameField, string whereClause)
        {
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string whereFilter = "";
            CRMBaseModel tempData;
            List<CRMBaseModel> dataList = new List<CRMBaseModel>();

            try
            {
                //Errors check
                if ((oCompany == null) || !oCompany.Connected)
                    throw new Exception("Instancia de objeto Company no válido.");
                if (string.IsNullOrWhiteSpace(table))
                    throw new Exception("Nombre de tabla vacía.");

                //Initializing objects
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (!string.IsNullOrWhiteSpace(whereClause))
                    whereFilter = " WHERE " + whereClause;
                sql = string.Format("SELECT \"Code\", \"{0}\" FROM \"{1}\" {2}", nameField, table, whereFilter);

                recordSet.DoQuery(sql);
                ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);

                if (recordSet.RecordCount > 0)
                {
                    recordSet.MoveFirst();
                    while (!recordSet.EoF)
                    {
                        tempData = new CRMBaseModel();
                        tempData.Id = recordSet.Fields.Item("Code").Value.ToString();
                        tempData.Name = recordSet.Fields.Item(nameField).Value.ToString();
                        dataList.Add(tempData);
                        recordSet.MoveNext();
                    }
                }

                recordSet = null;

            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            ErrorHandler.WriteInLog("Found " + dataList.Count.ToString() + " registers.", ErrorHandler.ErrorVerboseLevel.Trace);

            return dataList;
        }

        /// <summary>
        /// Return the entire list of registers of a table, with the Code and Name field only, using codeField to get the Code and nameField to get the Name
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="table"></param>
        /// <param name="codeField"></param>
        /// <param name="nameField"></param>
        /// <param name="whereClause"></param>
        /// <returns></returns>
        public static List<CRMBaseModel> GetIDCustomAllListFromTable(SAPbobsCOM.Company oCompany, string table, string codeField, string nameField, string whereClause)
        {
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string whereFilter = "";
            CRMBaseModel tempData;
            List<CRMBaseModel> dataList = new List<CRMBaseModel>();

            try
            {
                //Errors check
                if ((oCompany == null) || !oCompany.Connected)
                    throw new Exception("Instancia de objeto Company no válido.");
                if (string.IsNullOrWhiteSpace(table))
                    throw new Exception("Nombre de tabla vacía.");

                //Initializing objects
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (!string.IsNullOrWhiteSpace(whereClause))
                    whereFilter = " WHERE " + whereClause;
                sql = string.Format("SELECT \"{0}\", \"{1}\" FROM \"{2}\" {3}", codeField, nameField, table, whereFilter);

                recordSet.DoQuery(sql);
                ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);

                if (recordSet.RecordCount > 0)
                {
                    recordSet.MoveFirst();
                    while (!recordSet.EoF)
                    {
                        tempData = new CRMBaseModel();
                        tempData.Id = recordSet.Fields.Item(codeField).Value.ToString();
                        tempData.Name = recordSet.Fields.Item(nameField).Value.ToString();
                        dataList.Add(tempData);
                        recordSet.MoveNext();
                    }
                }

                recordSet = null;

            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            ErrorHandler.WriteInLog("Found " + dataList.Count.ToString() + " registers.", ErrorHandler.ErrorVerboseLevel.Trace);

            return dataList;
        }
    }
}